<?php namespace Irradiate\View;

use Illuminate\Events\Dispatcher;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\View;
use Illuminate\View\ViewFinderInterface;

class Factory extends \Illuminate\View\Factory
{
	/**
	 * @var \Illuminate\View\View[]
	 */
	protected $viewstack = [];

	public function render($view, $data = [], $inherit = true)
	{
		$context = ($inherit && $this->viewstack) ? $this->viewstack[0]->getData() : [];

		$content = $this->make($view, $data, $context)->render();

		return $content;
	}

	public function master($view, $data = [])
	{
		$context = ($this->viewstack) ? $this->viewstack[0]->getData() : [];

		$content = $this->make($view, $data, $context)->render();

		echo $content;
	}

	public function open($section)
	{
		parent::startSection($section);
	}

	public function close($overwrite = false)
	{
		parent::stopSection($overwrite);
	}

	public function section($section, $default = '')
	{
		return isset($this->sections[$section]) ? $this->sections[$section] : $default;
	}

	public function parent()
	{
		return '@parent';
	}

	public function set($section, $content)
	{
		$this->sections[$section] = $content;
	}

	public function get($section, $default = '')
	{
		return isset($this->sections[$section]) ? $this->sections[$section] : $default;
	}

	/**
	 * @inheritdoc
	 */
	public function make($view, $data = [], $mergeData = [])
	{
		if (isset($this->aliases[$view])) {
            $view = $this->aliases[$view];
        }

        $view = $this->normalizeName($view);
        $path = $this->finder->find($view);

        $data = array_merge($mergeData, $this->parseData($data));

		$engine = $this->getEngineFromPath($path);
		$data['view'] = $engine;

		$view = new View($this, $engine, $view, $path, $data);

        $this->callCreator($view);

        return $view;
	}

	public function component($view, $data = [])
	{
		if (isset($this->aliases[$view])) {
			$view = $this->aliases[$view];
		}

		$path = $this->normalizeName($view);
		$path = $this->finder->find($path);

		$data = $this->parseData($data);

		$engine = $this->getEngineFromPath($path);
		$data['view'] = $engine;

		$content = $engine->get($path, $data);

		return $content;
	}

	public function callComposer(View $view)
	{
		parent::callComposer($view);

		array_unshift($this->viewstack, $view);
	}

	public function decrementRender()
	{
		array_shift($this->viewstack);

		parent::decrementRender();
	}
}
