<?php namespace Irradiate\View;

class ViewServiceProvider extends \Illuminate\View\ViewServiceProvider
{
	/**
	 * Register the view environment.
	 *
	 * @return void
	 */
	public function registerFactory()
	{
		$this->app->alias(Factory::class, 'view');

		$this->app->singleton(Factory::class, function($app) {

			$resolver = $app['view.engine.resolver'];
			$finder = $app['view.finder'];

			$env = new Factory($resolver, $finder, $app['events']);
			$env->setContainer($app);
			$env->share('app', $app);

			return $env;
		});
	}
}
