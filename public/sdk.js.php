<?php

    header('Content-Type: application/javascript');

?>
var ReSynctSDK = new (function(){

    <?php
        include 'settings.php';

        echo "var rs_domain = '".RESYNCT_BASE_URL."';";
    ?>

    var loaded_from_cache = false;

    var _log = function(msg) {
        if (console && console.log) {
            console.log(msg);
        }
    };

    var iframes = [];

    var _this = this;

    var _counter = 0;
    var getNextWid = function() {
        return ++_counter;
    };


    // =============================================  Generic helpers ===================================================== //
    // ==================================================================================================================== //

    var fireEvent = function(event)
    {
        if (document.createEvent){
            // dispatch for firefox + others
            var evt = document.createEvent("HTMLEvents");
            evt.initEvent(event, true, true ); // event type,bubbling,cancelable
            return ! this.dispatchEvent(evt);
        } else {
            // dispatch for IE
            var evt = document.createEventObject();
            return this.fireEvent('on'+event,evt)
        }
    };

    /**
     * Load all given attributes of the node.
     * @param node
     * @param mandatory
     * @param optional
     * @returns {*} or false if any of the mandatory attributes is missing
     */
    var getAttributes = function(node, mandatory, optional)
    {
        var attrs = {};
        var i, attr, value;
        for (i in mandatory) {
            attr = mandatory[i];
            value = node.getAttribute('data-' + attr);
            if (typeof value === 'undefined' || ! value) {
                _log('attribute data-' + attr + ' is missing');
                return false;
            }
            attrs[attr] = value;
        }
        if (typeof optional !== 'undefined') {
            for (i in optional) {
                attr = optional[i];
                value = node.getAttribute('data-' + attr);
                if (typeof value !== 'undefined' && value !== null) {
                    attrs[attr] = value;
                }
            }
        }

        return attrs;
    };


    var generateCollectWidgets = function(class_name)
    {
        var elements = document.getElementsByClassName(class_name);
        var i, div, attrs, iframe;
        for (i = 0; i < elements.length; i++) {
            div = elements[i];
            attrs = getAttributes(div, ['hotel_id', 'amount', 'currency', 'session_id']);
            if ( ! attrs) {
                continue;
            }

            iframe = generateIframe('collect', attrs, div);
        }
    };


    var generateLoginWidget = function(id)
    {
        var div = document.getElementById(id);
        if ( ! div || typeof div === 'undefined') {
            return;
        }
        var attrs = {};
        var iframe = generateIframe('login', attrs, div);
    };


    var generateConfirmWidget = function(class_name)
    {
        var elements = document.getElementsByClassName(class_name);
        var i, div, attrs, iframe;
        for (i = 0; i < elements.length; i++) {
            div = elements[i];

            var mandatory = ['display_currency', 'display_amount', 'display_taxes_amount'];
            attrs = getAttributes(div, mandatory, ['display_total_amount']);
            if (attrs === false) {
                _log('Missing attribute on resynct-redeem widget. Mandatory attributes: ');
                _log(mandatory);
                continue;
            }

            // reservation_id OR session_id must be provided
            var res_id = div.getAttribute('data-reservation_id');
            var sess_id = div.getAttribute('data-session_id');
            if (typeof (res_id) !== 'undefined' && res_id) {
                attrs.reservation_id = res_id;
            }
            else if (typeof (sess_id) !== 'undefined' && sess_id) {
                attrs.session_id = sess_id;
            }
            else {
                _log('Missing parameter. data-reservation_id OR data-session_id must be provided.');
                continue;
            }

            iframe = generateIframe('confirm', attrs, div);
            if ( typeof iframe === 'undefined') {
                continue;
            }

            // Override styles for this type of widget
            iframe.removeAttribute('width');
            iframe.removeAttribute('height');
            iframe.style.width = '100%';
            div.style.width = '100%';
            div.style.minWidth = '220px';
            iframe.style.height = '185px';
            iframe.style.minWidth = '220px';
            iframe.style.maxWidth = '1000px';
            iframe.style.position = 'static';
        }
    };

    var generateRedeemWidgets = function(class_name)
    {
        var elements = document.getElementsByClassName(class_name);
        var i, div, attrs, iframe;
        for (i = 0; i < elements.length; i++) {
            div = elements[i];
            var mandatory = ['hotel_id', 'room_code', 'amount', 'currency', 'display_amount', 'display_currency', 'session_id', 'check_in', 'check_out'];
            attrs = getAttributes(div, mandatory, ['rate_code']);

            if (attrs === false) {
                _log('Missing attribute on resynct-redeem widget. Mandatory attributes: ');
                _log(mandatory);
                continue;
            }
            iframe = generateIframe('redeem', attrs, div);
        }
    };

    var generateIframe = function(type, attrs, div) {
        if (div.getAttribute('data-parsed') == '1') {
            loaded_from_cache = true;
            return;
        }
        // Widget unique id
        var wid = getNextWid();
        var iframe = document.createElement('iframe');
        // @todo - decide the size based on the iframe type

        iframe.width = 131;
        iframe.height = 28;
        if (type == 'redeem' || type == 'collect') {
            iframe.width = 172;
        }
        if (type == 'login') {
            iframe.width = 112;
        }
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('allowtransparency', 'true');
        iframe.setAttribute('allowfullscreen', 'true');
        iframe.setAttribute('scrolling', 'no');
        iframe.style.position = 'absolute';
        iframe.style.left = '0';

        div.style.position = 'relative';
        div.style.width = iframe.width + 'px';
        //div.style.height = iframe.height + 'px';
        //attrs.origin = window.location.hostname;

        var src = rs_domain + '/api/widget/' + type + '?wid=' + wid + '&t=' + Date.now();
        for (var p in attrs) {
            if (attrs.hasOwnProperty(p)) {
                src += '&' + p + '=' + encodeURI(attrs[p]);
            }
        }
        // Set the src
        iframe.src = src;
        iframe.name = wid + '-' + Date.now();
        div.setAttribute('data-wid', wid);
        div.appendChild(iframe);
        div.setAttribute('data-parsed', '1');
        div.setAttribute('class', div.getAttribute('class') + ' ' + 'rs-iframe-widget');
        iframes.push({
            wid: wid,
            type: type,
            iframe: iframe,
            div: div
        });

        return iframe;
    };

    var reloadIframe = function(iframe)
    {
        // @todo - make sure this is compatible with all browsers
        iframe.src = iframe.src;
    };

    var hideIframe = function(wid)
    {
        _log('hideIframe called with wid: ' + wid);
        var i;
        for (i in iframes) {
            if (iframes[i].wid == wid) {
                iframes[i].iframe.width = 0;
                iframes[i].iframe.height = 0;
                iframes[i].iframe.style.height = 0;
                //iframes[i].div.style.width = 0; // This breaks positioning in BF
                //iframes[i].div.style.height = 0;
            }
        }
    };

    var resizeIframe = function(wid, width, height, resize_container)
    {
        var i;
        for (i in iframes) {
            if (iframes[i].wid == wid) {
                iframes[i].iframe.width = width;
                iframes[i].iframe.height = height;
                iframes[i].iframe.style.zIndex = 1;
                // update iframe's container div - not really, it breaks the outer layout
                if (resize_container === true) {
                    // Confirm widget wants to resize it's container too (to take place in parent website)
                    // but redeem dialog doesn't want this - it is pop-up dialog
                    iframes[i].div.style.height = height + 'px';
                    iframes[i].div.style.width = width + 'px';
                }


            } else {
                // @todo - move the zIndex logic into the separate method
                iframes[i].iframe.style.zIndex = null;
            }
        }
    };

    this.reloadAllIframes = function()
    {
        var i;
        for (i in iframes) {
            reloadIframe(iframes[i].iframe);
        }
    };

    /**
     * Hides all dialogs in all iframes
     */
    var hideAllDialogs = function()
    {

        // @todo - postMessage to all iframes to hide the dialogs
        //            - iframes will postMessage back asking for the resize

    };

    var selectionMade = function(wid)
    {
        _log('selectionMade called with wid: '+wid);
        _log(iframes);
        for (var i in iframes) {
            var ifr = iframes[i];
            if (ifr.type !== 'redeem') {
                continue;
            }
            if (ifr.wid == wid) {
                _log('div container found. calling fireEvent.');
                // This is the one we want to trigger the event on
                fireEvent.call(ifr.div, 'resynct:selectionMade');
                break;
            }

//            ifr.postMessage({action: 'close_dialog'}, '*');
        }
    };

    var openDialog = function(wid)
    {
        for (var i in iframes) {
            var ifr = iframes[i];
            if (ifr.type !== 'redeem') {
                continue;
            }

            if (ifr.wid == wid) {
                // This is the only one we want to keep open
                continue;
            }

//            ifr.postMessage({action: 'close_dialog'}, '*');
        }
    };


    // ========================================================================================= //
    // ==================================== Login PopUp ======================================== //

    var a, i, g, f; // we need to pass the window size to the login page as get params
    var getCenteredPos = function(width, height) {
        a = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft;
        i = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop;
        g = typeof window.outerWidth!='undefined' ? window.outerWidth : document.documentElement.clientWidth;
        f = typeof window.outerHeight != 'undefined' ? window.outerHeight: (document.documentElement.clientHeight - 22);
        var h = a; //(a < 0) ? window.screen.width + a : a;
        var left = parseInt(h + ((g - width) / 2), 10);
        var top = parseInt(i + ((f-height) / 2.5), 10);
        return {
            left: left,
            top: top
        };
    };
    function popup_params(width, height)
    {
        var pos = getCenteredPos(width, height);
        return 'width=' + width + ',height=' + height + ',left=' + pos.left + ',top=' + pos.top + ',scrollbars=1';
    }

    var getRegisterUrl = function(a, i, g, f)
    {
        return rs_domain + '/auth/register/popup?a='+a+'&i='+i+'&g='+g+'&f='+f;
    };

    function openRegisterPopup()
    {
        var width = 800, height = 580;
        var params = popup_params(width, height);
        var win = window.open(getRegisterUrl(a, i, g, f), "resynct-register", params);

        var timer = window.setInterval(function(){
            if (win.closed !== false) {
                window.clearInterval(timer);
                _this.reloadAllIframes();
            }
        }, 50);
    }

    // ==================================== Login PopUp END===================================== //
    // ========================================================================================= //

    this.initSdk = function() {

        window.addEventListener('message', function(event) {
            // @todo - event origin domain check - all messages should be coming from resynct domain only

            _log('message received: ' + event.data.action);

            switch (event.data.action) {
                case 'selection_made':
                    return selectionMade(event.data.params.wid);
                case 'reload_all':
                    return _this.reloadAllIframes();
                case 'hide':
                    return hideIframe(event.data.params.wid);
                case 'resize':
                    return resizeIframe(event.data.params.wid, event.data.params.width, event.data.params.height, event.data.params.resize_container);
                case 'dialog_open':
                    return openDialog(event.data.params.wid);
            }
        }, false);


        window.addEventListener('click', function(event) {
            hideAllDialogs();
        });

        window.addEventListener('click', function(e) {
            var elem, evt = e ? e:event;
            if (evt.srcElement)  elem = evt.srcElement;
            else if (evt.target) elem = evt.target;

            if (elem && elem.getAttribute('class') && elem.getAttribute('class').indexOf('resynct-register') > -1) {
                // Registration link
                openRegisterPopup();
            }
            return true;
        });

        this.parse();
    };

    this.parse = function() {
        generateCollectWidgets('resynct-collect');
        generateRedeemWidgets('resynct-redeem');
        generateConfirmWidget('resynct-confirm');
        generateLoginWidget('resynct-login');
    };

    if (loaded_from_cache) {
        alert('loaded from cache');
    }

})();

ReSynctSDK.initSdk();


