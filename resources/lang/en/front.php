<?php

return [
    'By clicking "Sign Up" I agree to reSynct' => 'By clicking "Sign Up" I agree to reSynct <a class="privacy nowrap" href=":privacy_statement" target="_blank">Privacy Statement</a>',
];
