<?php

return [
    'privacy_statement'    => route('front.pages.privacy'),

    'faq_members'          => route('front.pages.faq.member'),

    'facebook'  => '#',
    'instagram' => '#',
    'linkedin'  => '#',
];
