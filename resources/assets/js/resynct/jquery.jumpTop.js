// <script>

(function( $ ) {
    var plugin_name_space = 'jumpTop';
    var settings = {
        minScrollTop: '300',
    };

    var _enabled = false;

    var methods = {

        init: function( options ) {
            if ( options ) {
                $.extend( settings, options );
            }

            var $elem = $(this).find('#jumpTop');
            if ( ! $elem.length) {
                $(this).append($('<div id="jumpTop" class="hidden"><span class="glyphicon glyphicon-arrow-up"></span></div>'));
                var $elem = $(this).find('#jumpTop');
            }


            $elem.on('click', function () {
                $(window).scrollTop(0);
            });


            $(window).on('scroll', function() {
                if (($(window).scrollTop() >= settings.minScrollTop) && ( ! _enabled)) {
                    $elem.removeClass('hidden');
                    _enabled = true;
                }

                if (($(window).scrollTop() < settings.minScrollTop) && (_enabled)) {
                    $elem.addClass('hidden');
                    _enabled = false;
                }
            });

        },


        _log: function( text ) {
            if ( typeof console !== 'undefined') {
                console.log( text );
            }
        }

    };

    $.fn.jumpTop = function( method ) {
        if ( methods[ method ] ) {
            return methods[ method ].apply( Array.prototype.slice.call( arguments , 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' + method + ' does not exist on jQuery.jumpTop');
        }

        return this;
    };
})( jQuery );
