
var ReSynct = {};

ReSynct.options = {};

/**
 * Cross-iframe comunication API - Message Receiver - this method may be called from any Resynct iframe to pass the message here
 * without using postMessage API which is not necessary.
 * @param action
 * @param params
 */
var sendCustomMessage = function(action, params)
{
    // Let the iframe react only if it is interested in this kind of messages
    $(document).trigger('rs:'+action, params);
};

/**
 * Cross-iframe comunication API - Message Sender - this method is called in widget's JS and it calls the sendCustomMessage on sibling iframes.
 * @param action
 * @param params
 */
ReSynct.sendSiblingIframesMessage = function(msg, params)
{
    if (typeof params === 'undefined') {
        params = {};
    }
    params.wid = ReSynct.options.wid;
    var frames = window.parent.frames;
    for (var i = 0; i < frames.length; i++) {
        try {
            var f = frames[i];
            if (typeof f.sendCustomMessage !== 'undefined') {
                f.sendCustomMessage(msg, params);
            };
        }
        catch (exception) {
            ReSynct._log(exception);
        }
    }
};


ReSynct._log = function(msg) {
    if (console && console.log) {
        console.log(msg);
    }
};

ReSynct.init = function(options)
{
    this.options = options;

    $(function(){
        if ($('#rs_no_data').length) {
            ReSynct.sendMessage('hide');
        } else {
            if ($('body').attr('data-type') !== 'confirm') {
                ReSynct.askForResize();
            }
        }
    });

    $(document).trigger('rs:init');
};


/**
 * Runs crossDomain ajax call
 */
ReSynct.ajax = function(url, params, callback, error_callback)
{
    return $.ajax({
        url: url,
        type: "POST",
        data: params,
        xhrFields: {
            withCredentials: true
        },
        success: function (response) {
            if (typeof callback == 'function') {
                callback(response);
            }
        },
        error: function (xhr, status) {
            ReSynct._log("AJAX request faild: ");
            ReSynct._log(status);
            if (typeof error_callback !== 'undefined') {
                error_callback(status);
            }
        }
    });
};


ReSynct.askForResize = function(width, height, resize_container)
{
    if (typeof width === 'undefined') {
        width = $('body').attr('data-width');
    }
    if (typeof height === 'undefined') {
        height = $('body').attr('data-height');
    }
    var data = {
        width: width,
        height: height
    };
    if ($('body').attr('data-resize_container') === '1' || resize_container === true) {
        data.resize_container = true;
    }
    ReSynct.sendMessage('resize', data);
};

ReSynct.sendMessage = function(action, params)
{
    // @todo - this will send a message to the parent window
    // - asking for iframe resize or the reload of all iframes (maybe some other actions will be needed)

    if (typeof (params) !== 'object') {
        params = {};
    }

    params.wid = ReSynct.options.wid;

    var data = {
        action: action,
        params: params
    };

    //ReSynct._log('sending message - action: ' + action);
    //ReSynct._log(data);
    window.parent.postMessage(data, '*');
};


// Login pop-up handler - any element matching ".resynct-authenticate-window" selector will open a login pop-up on click
(function(jQuery){

    var a, i, g, f; // we need to pass the window size to the login page as get params
    var getCenteredPos = function(width, height) {
        a = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft;
        i = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop;
        g = typeof window.outerWidth!='undefined' ? window.outerWidth : document.documentElement.clientWidth
        f = typeof window.outerHeight != 'undefined' ? window.outerHeight: (document.documentElement.clientHeight - 22);
        var h = a; //(a < 0) ? window.screen.width + a : a;
        var left = parseInt(h + ((g - width) / 2), 10);
        var top = parseInt(i + ((f-height) / 2.5), 10);
        return {
            left: left,
            top: top
        };
    };
    function popup_params(width, height) {
        var pos = getCenteredPos(width, height);
        return 'width=' + width + ',height=' + height + ',left=' + pos.left + ',top=' + pos.top + ',scrollbars=1';
    }

    var getLoginUrl = function(a, i, g, f)
    {
        return ReSynct.options.login_url + '?a='+a+'&i='+i+'&g='+g+'&f='+f;
    };

    var getRegisterUrl = function(a, i, g, f)
    {
        return ReSynct.options.register_url + '?a='+a+'&i='+i+'&g='+g+'&f='+f;
    };

    var win = null;

    /**
     * Opens registration popup window - based on the class .resynct-authenticate-window
     */
    function openLoginPopup()
    {
        var width = 430, height = 560;
        var params = popup_params(width, height);
        win = window.open(getLoginUrl(a, i, g, f), "resynct-login", params);
        var timer = window.setInterval(function(){
            if (win.closed !== false) {
                window.clearInterval(timer);
                ReSynct.sendMessage('reload_all');
            }
        }, 20);
    }
    jQuery(document).on('click', '.resynct-authenticate-window', openLoginPopup);


    /**
     * Opens registration popup window - based on the class .resynct-register-window
     */
    function openRegistrationPopup()
    {
        var width = 800, height = 580;
        var params = popup_params(width, height);
        win = window.open(getRegisterUrl(a, i, g, f), "resynct-register", params);
        var timer = window.setInterval(function(){
            if (win.closed !== false) {
                window.clearInterval(timer);
                ReSynct.sendMessage('reload_all');
            }
        }, 20);
    }
    jQuery(document).on('click', '.resynct-register-window', openRegistrationPopup);




})(jQuery);

