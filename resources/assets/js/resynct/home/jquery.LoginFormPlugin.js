// <script>

(function( $ ) {
    var plugin_name_space = 'LoginFormPlugin';
    var settings = {
    };

    var methods = {

        init: function( options ) {
            if ( options ) {
                $.extend( settings, options );
            }

            return this.each(function() {
                var $this = $(this);
                var $form = $(this);

                //ulozim si aktualni nastaveni pluginu
                methods._setData( $this , {
                    settings: settings
                });

                var fields = {
                    'email'                 : $form.find('input[name="email"]'),
                    'password'              : $form.find('input[name="password"]'),
                    'remember'              : $form.find('input[name="remember"]'),
                };

                $form.on('submit', function (e) {
                    // Clear error messages
                    // @FIXME
                    $form.find('.form-group').each(function() { $(this).removeClass('has-error'); });
                    $form.find('.help-block').each(function() { $(this).html(''); });

                    var data_out = $form.serialize();

                    $.ajax({
                        method : $form.attr('method') || 'POST',
                        async  : true,
                        url    : $form.attr('action'),
                        data   : data_out
                    })
                    .done(function(data_in) {
                        if ($.type(data_in) == 'string') {
                            window.location.reload();
                        }
                        if (data_in.errors === null && data_in.target_url) {
                            window.location.href = data_in.target_url;
                        }

                        if (data_in.errors !== null) {
                            for (var i in data_in.errors) {
                                var error  = data_in.errors[i];

                                if ( ! error.length) { continue; }

                                // If there is no predefined field
                                if ( ! fields[i]) { continue; }

                                var $field       = fields[i];
                                var $form_group  = $field.parent('.form-group');
                                var $message_box = $form_group.find('.help-block');

                                $form_group.addClass('has-error');
                                $message_box.html(error.shift());
                            }
                        }
                    })
                    .fail(function(data_in) {
                        var errors = data_in.responseJSON;

                        for (var i in errors) {
                            var error  = errors[i];

                            if ( ! error.length) { continue; }

                            // If there is no predefined field
                            if ( ! fields[i]) { continue; }

                            var $field       = fields[i];
                            var $form_group  = $field.parent('.form-group');
                            var $message_box = $form_group.find('.help-block');

                            $form_group.addClass('has-error');
                            $message_box.html(error.shift());
                        }
                    });

                    e.preventDefault();
                    return false;
                });

            });
        },


        /**
         *
         */
        _setData: function( $this, key, value ) {
            if (typeof key === 'object' ) {
                var current_data = $this.data( plugin_name_space);

                if (typeof current_data === 'undefined') {
                    current_data = new Object();
                }

                //budu extendovat to co mam aktualne v datech ulozene
                $.extend( current_data , key);

                $this.data( plugin_name_space, current_data );

            } else {
                var current_data = $this.data( plugin_name_space );
                if (typeof current_data === 'undefined' ) {
                    current_data = {
                        key: value
                    };
                } else {
                    current_data[key] = value;
                }

                $this.data( plugin_name_space, current_data )
            }
        },

        /**
         *
         */
        _getData: function( $this, key ) {
            var current_data = $this.data( plugin_name_space );
            return current_data[ key ];

        },

        _log: function( text ) {
            if ( typeof console !== 'undefined') {
            //    console.log( text );
            }
        }

    };

    $.fn.LoginFormPlugin = function( method ) {
        if ( methods[ method ] ) {
            return methods[ method ].apply( Array.prototype.slice.call( arguments , 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' + method + ' does not exist on jQuery.LoginFormPlugin');
        }

        return this;
    };
})( jQuery );
