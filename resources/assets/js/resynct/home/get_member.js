$(document).ready(function()
{
    // Hide/Show modals
    $('a[data-modal-show="modal"]').on('click', function(e) {
        var modal_id = $(this).data('target');
        var $modal   = $(modal_id);

        $('.modal').each(function() {
            $(this).modal('hide');
        });

        $modal.modal('show');

        e.preventDefault();
        return false;
    });
    $('a[data-modal-hide="modal"]').on('click', function(e) {
        var modal_id = $(this).data('target');
        var $modal   = $(modal_id);

        if ($modal.length) {
            $modal.modal('hide');
        }
        else {
            $('.modal').each(function() { $(this).modal('hide'); });
        }

        e.preventDefault();
        return false;
    });


    // Resize #header function
    (function() {
        var _helper = function() {
            if ( $(document).scrollTop() > 80) {
                $('body').attr('data-header', 'lowered');
            }
            else {
                $('body').attr('data-header', '');
            }
        }

        _helper();
        $(document).on('scroll', _helper);
    })();


    // Links in header scroll to events
    $('a[data-scroll-to*="#"]').on('click', function(e) {
        var id = $(this).data('scroll-to');
        var position_y = $(id).offset().top;

        $('html, body').animate({scrollTop: position_y}, '500');

        e.preventDefault();
        return false;
    });


    // Sign up forms ajax
    $('.form-sign-up form').RegistrationFormPlugin();
});
