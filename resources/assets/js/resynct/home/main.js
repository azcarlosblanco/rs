// Set padding-right for scrollbar.width on modal show/hide events
// https://github.com/twbs/bootstrap/blob/master/js/modal.js
$(function () {
    var originalBodyPad = 0;

    $('body').on('show.bs.modal', function() {
        originalBodyPad = document.body.style.paddingRight || ''
    });
    $('body').on('shown.bs.modal', function() {
        $('[data-adjust-on-modal="true"]').each(function() {
            $(this).css('padding-right', document.body.style.paddingRight);
        });
    });
    $('body').on('hidden.bs.modal', function() {
        $('[data-adjust-on-modal="true"]').each(function() {
            $(this).css('padding-right', originalBodyPad);
        });
    });
});
