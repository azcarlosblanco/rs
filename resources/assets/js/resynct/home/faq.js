$(document).ready(function() {
    $('#content.faq .line h3').each(function() {
        $(this).on('click', function() {
            var $container = $(this).parent('.line');
            var $content   = $container.find('.content');

            if ($container.attr('data-expanded') && ($container.attr('data-expanded') == 'true')) {
                $container.attr('data-expanded', 'false');
            }
            else {
                $container.attr('data-expanded', 'true');
            }
        })
    });
});
