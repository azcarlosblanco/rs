/**
 * Created by Jira on 19.4.2016.
 */

$(document).ready(function(){
    $('.resynct-authenticated').hover(function(){
        var $t = $(this);
        $t.addClass('logout-displayed');

    }, function() {
        var $t = $(this);
        $t.removeClass('logout-displayed');
    });

    $(document).on('click', '.logout-displayed', function(){
        $.ajax({
            url: ReSynct.options.logout_url,
            type: 'get',
            success: function(){
                ReSynct.sendMessage('reload_all');
            },
            error: function(){
                $('.resynct-authenticated').removeClass('logout-displayed');
            }
        });
    })
});
