
$(document).on('rs:init', function() {

    if ( ! $('#rs_redeem_container').length) {
        // This is not a redeem iframe - nothing to init
        return;
    }

    var $this = $('#rs_redeem_container');

    var settings = {
        redeem_points_btn_class: 'resynct-use-points-btn',
        redeem_points_dialog_class: 'resynct-point-selection-dialog',
        save_point_selection_class: 'resynct-save-selection-btn',
        // @todo - move this to options so that it can be translated on the server
        redeem_points_btn_with_selection_label: 'Redeem :points:pts',
        redeem_points_btn_label: 'Redeem Points'
    };

    // ============================== External event handlers ================================= //
    // ======================================================================================== //

    // If a dialog is opened this is triggered on all resynct iframes
    $(document).on('rs:dialog_open', function(event, data) {
        if (data.wid != ReSynct.options.wid) {
            if ($this.hasClass('rs-open')) {
                closeDialog();
            }
        }
    });
    // If a selection is made this is triggered on all iframes
    $(document).on('rs:selection_made', function(event, data) {
        ReSynct._log('selection_made called on wid: ' + ReSynct.options.wid);
        ReSynct._log(data);
        if (data.wid*1 != ReSynct.options.wid*1) {
            if ($this.hasClass('resynct-selected')) {
                ReSynct._log('clearing UI');
                clearSelectionUI();
            }
        }
    });



    // =================================== Helper functions =================================== //
    // ======================================================================================== //

    var closeDialog = function()
    {
        $this.removeClass('rs-open');
        var w = 172;
        var h = 28;
        ReSynct.askForResize(w, h);
    };
    var openDialog = function($this)
    {
        $this.addClass('rs-open');
        var w = 172;
        var h = 215;
        ReSynct.sendSiblingIframesMessage('dialog_open');
        ReSynct.askForResize(w, h);
    };

    var clearSelectionUI = function()
    {
        $this.removeClass('resynct-selected');
        $this.find('.resynct-use-points-btn').find('span').html(settings.redeem_points_btn_label);
    };


    var pointsToCurrency = function(points)
    {
        return points / 100;
    };

    var getCollectiblePointsForAmount = function(usd_amount)
    {
        return usd_amount * ReSynct.options.usd_to_points_multiplier + ReSynct.options.flat_points;
    };

    // =================================== Internal handlers =================================== //
    // ========================================================================================= //

    // Redeem button clicked - open/close the dialog
    $('#redeem_button').on('click', function(){
        if ($this.hasClass('rs-open')) {
            closeDialog($this);
        } else {
            openDialog($this);
        }
    });


    // Clear selection button clicked
    $(document).on('click', '.resynct-clear-selection-btn', function(){
        // Clear the selection in the UI
        ReSynct.clearPointSelection($(this));
    });

    $(document).on('change', '#dropdown-selection', function() {
        sliderChanged.call($(this), null, {value: $(this).val()});
    });

    // Slider was changed by the user - handler
    var sliderChanged = function(event, data) {
        var $el = $(this);
        var $container = $el.parents('.resynct-point-selection-dialog:first');

        var usd_amount = 1 * ReSynct.options.usd_amount;
        var points_selected = data.value;
        // Corresponding money amount (discount in USD)
        var discount = pointsToCurrency(points_selected);
        var new_rate = usd_amount - discount;
        var earn_points = getCollectiblePointsForAmount(new_rate);

        discount = discount * ReSynct.options.display_currency_multiplier;
        new_rate = new_rate * ReSynct.options.display_currency_multiplier;

        // Update the numbers in the selection dialog
        $container.find('.resynct-selection-points').html(Math.round(points_selected));
        $container.find('.resynct-discount').html((Math.round(discount * 100) / 100) + ' ' + ReSynct.options.display_currency);
        $container.find('.resynct-final-rate').html((Math.round(new_rate * 100) / 100) + ' ' + ReSynct.options.display_currency);
        $container.find('.resynct-earn-points').html(Math.round(earn_points) + ' pts');
    };


    sliderChanged.call($('#dropdown-selection'), null, {value: $('#dropdown-selection').val()});
    // Call the handler to init the slider values
    //sliderChanged.call($('#resynct-slider-value'), null, {value: 500});



    var saveSelectionButtonClicked = function(event)
    {
        var selected_points = $('*[name=selection]').val();
        selectionMade(selected_points);
    };
    $(document).on('click', '.'+settings.save_point_selection_class, saveSelectionButtonClicked);


    /**
     * Clears the selection for given session_id.
     */
    ReSynct.clearPointSelection = function()
    {
        clearSelectionUI();
        this.ajax(ReSynct.options.clear_point_selection_url, {
            booking_engine_id: ReSynct.options.engine_id,
            session_id: ReSynct.options.session_id
        }, function(data){
            ReSynct._log(data);
        });
    };



    ReSynct.initSlider = function()
    {
        $('#resynct-slider-value').simpleSlider({
            highlight: true,
            range: [ReSynct.options.min_points, ReSynct.options.max_points],
            step: 100
        }).on('slider:changed', sliderChanged)
        ;
    };

    /**
     * Called after the selection is made (clicking on a "green" button).
     * @param selected_points - amount of points selected
     * @param wid - widget ID
     */
    var selectionMade = function(selected_points)
    {
        // Btn - to change the label
        var $btn = $this.find('.'+settings.redeem_points_btn_class);
        var new_label = settings.redeem_points_btn_with_selection_label.replace(':points:', selected_points);
        $btn.find('span').html(new_label);
        // Mark the widget as "selected" - it will display the remove button (cross)
        $this.addClass('resynct-selected');
        // Save a selection on ReSynct server
        ReSynct.savePointSelection(selected_points, function(){
            ReSynct.sendMessage('selection_made')
        });
        // Hide all selection dialogs
        closeDialog();
        // Tell other iframes that selection was made
        ReSynct.sendSiblingIframesMessage('selection_made');

    };


    ReSynct.savePointSelection = function(selected_points, success_callback)
    {
        ReSynct.ajax(ReSynct.options.save_point_selection_url, {
            room_code: ReSynct.options.room_code,
            rate_code: ReSynct.options.rate_code,
            hotel_id: ReSynct.options.hotel_id,
            currency: ReSynct.options.currency,
            amount: ReSynct.options.amount,
            points_amount: selected_points,
            booking_engine_id: ReSynct.options.engine_id,
            session_id: ReSynct.options.session_id,
            check_in: ReSynct.options.check_in,
            check_out: ReSynct.options.check_out
        }, function(r) {
                if (typeof success_callback !== 'undefined') {
                    success_callback(r);
                }
            },
        function(data){
            // Error callback
            ReSynct.clearPointSelection();
            ReSynct._log(data);
        });
    };

    ReSynct.initSlider();

});

