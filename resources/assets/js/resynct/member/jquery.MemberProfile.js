// <script>

(function( $ ) {
    var plugin_name_space = 'MemberProfile';
    var settings = {
        countryAutocompleteUrl: null,
    };

    var methods = {

        init: function( options ) {
            if ( options ) {
                $.extend( settings, options );
            }

            return this.each(function() {
                var $form = $(this).find('form[name="member_profile"]');

                $form.find('input[name="country_autocomplete"]').autocomplete({
                    minChars: 0,
                    serviceUrl: settings.countryAutocompleteUrl,
                    paramName: 'country',
                    onSelect: function (suggestion) {
                        $form.find('input[name="country_code"]').attr('value', suggestion.code);
                        $form.find('input[name="country_autocomplete"]').attr('value', suggestion.value);
                    }
                });
            });
        },


        _log: function( text ) {
            if ( typeof console !== 'undefined') {
                console.log( text );
            }
        }

    };

    $.fn.MemberProfile = function( method ) {
        if ( methods[ method ] ) {
            return methods[ method ].apply( Array.prototype.slice.call( arguments , 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' + method + ' does not exist on jQuery.MemberProfile');
        }

        return this;
    };
})( jQuery );
