// <script>

(function( $ ) {
    var plugin_name_space = 'MemberProfileAvatarUpdate';
    var settings = {
        slimElement   : null,
        avatarUrlGet  : null,
        avatarUrlPost : null,
    };

    var methods = {

        init: function( options ) {
            if ( options ) {
                $.extend( settings, options );
            }

            $(this).each(function() {

                var $this        = $(this);
                var $slimElement = settings.slimElement;

                // Real button //
                // Event: click on avatar-change (pencil button) => show avatar-change layer
                $(this).find('#avatar-change').on('click', function() {
                    $slimElement.trigger('slim.show');
                });


                // Events //

                // Event: slim.show : show avatar-change layer
                $slimElement.on('slim.show', function() {
                    $slimElement.css({opacity: 0});
                    $slimElement.removeClass('hidden');
                    $slimElement.css({opacity: 1});

                    $this.find('.avatar').addClass('hidden');
                });


                // Event: slim.close : hide avatar.change layer
                $slimElement.on('slim.close', function() {
                    $slimElement.css({opacity: 0});
                    $slimElement.addClass('hidden');

                    $this.find('.avatar').removeClass('hidden');
                });


                // Event: slim.upload_success : on success get new avatar.url, update it and close avatar-change layer
                $slimElement.on('slim.upload_success', function() {
                    $.ajax({url: settings.avatarUrlGet}).done(function(data) {
                        $this.find('.avatar .image img').prop('src', data.url);
                        $slimElement.trigger('slim.close');
                    });
                });


                // Event: slim.upload_error : just hide
                $slimElement.on('slim.upload_error', function() {
                    $slimElement.trigger('slim.close');
                });



                // Init slim //

                $slimElement.slim({
                    service           : settings.avatarUrlPost,
                    label             : "Drop your avatar here",
                    size              : {width: 300, height : 300},
                    ratio             : "1: 1",
                    buttonRemoveTitle : "Close",
                    buttonUploadTitle : "Change",
                    onRemove          : function() { $slimElement.trigger('slim.close'); },
                });

            });


        },

        _log: function( text ) {
            if ( typeof console !== 'undefined') {
                console.log( text );
            }
        }

    };

    $.fn.MemberProfileAvatarUpdate = function( method ) {
        if ( methods[ method ] ) {
            return methods[ method ].apply( Array.prototype.slice.call( arguments , 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' + method + ' does not exist on jQuery.MemberProfileAvatarUpdate');
        }

        return this;
    };
})( jQuery );
