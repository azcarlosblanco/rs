//<script>
(function( $ ){

    var methods = {
        init : function( options ) {

            return this.each(function() {

                var settings = $.extend( {
                    url: [location.protocol, '//', location.host, location.pathname].join(''),
                    sel_filter_form: '.filter_form',
                    sel_paginator: '.paginator',
                    page_size: 20
                }, options);

                var $this = $(this);

                // Paginator related vars
                var page_number = 1;
                var last_page = 1;
                var page_size = settings.page_size;
                var total_results_count = settings.total_results_count;

                // Sorting related vars
                var sort_by = null;
                var sort_dir = 'ASC'; // default sort dir

                // Filter related vars
                var filter_params = [];

                var updatePaginators = function()
                {
                    var paginator = '', active = '', disabled = '';
                    last_page = (total_results_count % page_size) ? parseInt(total_results_count / page_size) + 1 : parseInt(total_results_count / page_size);
                    paginator += '<ul class="pagination">';
                    if (page_number <= 1) disabled = 'disabled';
                    paginator += '<li class="'+disabled+'">\
                        <a href="javascript: ;" aria-label="Previous" data-page="prev">\
                        <span aria-hidden="true">&laquo;</span>\
                        </a>\
                    </li>';
                    disabled = '';
                    for (var page=1; page<=last_page; page++) {
                        active = (page == page_number) ? 'active' : '';
                        paginator += '<li class="'+disabled+' '+active+'">\
                            <a href="javascript: ;" data-page="'+page+'">'+page+'</a>\
                        </li>';
                    }
                    if (page_number >= last_page) disabled = 'disabled';
                    paginator += '<li class="'+disabled+'">\
                        <a href="javascript: ;" aria-label="Previous" data-page="next">\
                        <span aria-hidden="true">&raquo;</span>\
                        </a>\
                    </li>';
                    paginator += '</ul>';
                    $this.find(settings.sel_paginator).html(paginator);
                };

                var updateSorting = function()
                {
                    $this.find('[data-sortable]').each(function(){
                        var $el = $(this);
                        var col = $el.attr('data-sortable');
                        if ( ! $el.hasClass('sortable')) {
                            $el.addClass('sortable');
                        }
                        if (col == sort_by) {
                            $el.addClass('sorted_' + sort_dir.toLowerCase());
                        }
                    });
                }

                /**
                 * Update UI received from server with settings (sorting, page, ...) stored within the plugin
                 */
                var updateUI = function()
                {
                    updatePaginators();
                    updateSorting();
                };

                var gatherFilterParams = function()
                {
                    filter_params = {};

                    $this.find(settings.sel_filter_form).find("input[type='text'], input[type='hidden'], input[type='password'], select, textarea, :checkbox:checked, :radio:checked").each(function(){
                        var $el = $(this);
                        var name = $el.attr('name');
                        var value = $el.val();

                        console.log(name + ' : ' + value);
                        // Prazdne name ma treba typ exportu, ktery tady sbirat nechceme
                        if (typeof name !== 'string' || name == '') {
                            return;
                        }

                        if (filter_params[name]) {
                            if (typeof filter_params[name] === 'string') {
                                filter_params[name] = [filter_params[name]];
                            }
                            filter_params[name].push(value);
                        } else {
                            filter_params[name] = value;
                        }
                    });

                    console.log(filter_params);
                };


                var reload = function()
                {
                    var request = $.extend({}, filter_params);
                    request.pagination = {
                        page_number: page_number
                    };
                    if (sort_by) {
                        request.sorting = {};
                        request.sorting[sort_by] = sort_dir;
                    }

                    console.log(request);
                    $.ajax({
                        url: settings.url,
                        data: request,
                        method: 'get',
                        success: function(response) {
                            if (typeof response.total_count !== 'undefined') {
                                total_results_count = response.total_count;
                            }
                            $this.html(response.content);
                            updateUI();
                        }
                    });
                };

                updateUI();

                // Submit button click event
                $this.on('click', '.submit_filter', function(event){
                    gatherFilterParams();
                    page_number = 1;
                    reload();
                    event.preventDefault();
                    return false;
                });
                // Reset button click event
                $this.on('click', '.reset_filter', function(event){
                    filter_params = {};
                    page_number = 1;
                    reload();
                    event.preventDefault();
                    return false;
                });

                // Sorting click event
                $this.on('click', '.sortable', function(){
                    var col = $(this).attr('data-sortable');
                    if (col) {
                        if (sort_by == col) {
                            // sort oposite direction
                            sort_dir = (sort_dir == 'ASC') ? 'DESC' : 'ASC';
                        } else {
                            // sort by this col ASC
                            sort_by = col;
                            sort_dir = 'ASC';
                        }
                    }
                    reload(); // UI will be updated after reload
                });

                // Paginator click event
                $this.on('click', settings.sel_paginator + ' a', function(){
                    var $a = $(this);
                    var clicked = $a.attr('data-page');
                    if (clicked == 'prev' && page_number > 1) {
                        page_number--;
                    } else if (clicked == 'next' && page_number < last_page) {
                        page_number++;
                    } else if (parseInt(clicked)) {
                        page_number = clicked;
                    } else {
                        return; // no change no need to reload
                    }
                    reload();
                });


            }); // each function
     } // init method
  }; // methods list

  $.fn.DataTable = function( method ) {
    if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on DataTable plugin' );
    }    
  
  };

})( jQuery );

