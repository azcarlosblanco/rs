

var App = {};

App.singleSelectize = function(selector, params)
{
    $(selector).selectize({
        valueField: 'id',
        labelField: 'preview',
        searchField: 'preview',
        openOnFocus: true,
        preload: 'focus',
        create: false,
        maxItems: 1,
        load: function(query, callback) {
            $.ajax({
                url: params.url,
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                    limit: 10
                },
                error: function() {
                    callback();
                },
                success: function(res) {
                    callback(res);
                }
            });
        }
    });
};




$(document).on('click', 'a.popupform', function(e) {

    var $this = $(this);

    var $modal = $('#genericModal');

    $modal.find('#myModalLabel').html($this.attr('title'));
    $modal.find('.modal-body').html('Loading...');

    $.ajax({
        url: $this.attr('href'),
        type: 'post',
        headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
        success: function(r) {
            if (r.success) {
                $modal.find('.modal-body').html(r.content);
                $(function () {
                    $modal.find('[data-toggle="tooltip"]').tooltip()
                });

            } else {
                $modal.find('.modal-body').html('Reservation not found.');
            }
        },
        error: function(r) {
            $modal.find('.modal-body').html('Reservation not found.');
        }
    });

    $modal.modal();

    e.preventDefault();
});


// https://github.com/christianbach/tablesorter
$(document).ready(function() {
    $("#upcoming-reservations, #past-reservations").each(function() {
        $(this).tablesorter({headers: {6: {sorter: false}}});
    });
});
