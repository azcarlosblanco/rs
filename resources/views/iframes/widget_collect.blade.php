@extends('iframes.layout')

@section('content')

    @if ($user)
        <div class="resynct-font resynct-text">Collect up to {{ $points }} rePoints</div>
    @else
        <div class="resynct-points resynct-register-window">
            <div class="resynct-coin resynct-widget resynct-radius">
                <div class="resynct-font resynct-text resynct-white">Sign In &amp; Collect rePoints</div>
            </div>
        </div>
    @endif

@endsection

