@extends('iframes.layout')

@section('content')

        <div id="rs_redeem_container">
            <div class="resynct-use-points-btn-outer">
                <div class="resynct-coin resynct-widget resynct-radius" id="redeem_button">
                    <div class="resynct-font resynct-text resynct-white resynct-use-points-btn">
                        <span>Redeem Points</span>
                        <div class="resynct-caret"></div>
                    </div>
                </div>
                <div class="resynct-white resynct-font resynct-text resynct-widget resynct-radius-right resynct-clear-selection-btn">
                    <div class="resynct-cross"></div>
                </div>
                <div class="resynct-clearfix"></div>
            </div>

            <div class="resynct-point-selection-dialog resynct-font">

                    <?php if (true or in_array($_SERVER['REMOTE_ADDR'], ['90.177.30.211', '127.0.0.1', '::1'])): ?>

                        <div class="resynct-slider-container">
                            <label>Redeem:</label>
                            <select name="selection" id="dropdown-selection">
                                <?php foreach ($select_options as $key => $val): ?>
                                <option value="<?= $key ?>"><?= $val ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="resynct-values-container-sm">
                            <!--
                            <div class="resynct-line">
                                <div class="resynct-value"><span class="resynct-room-rate"><?= $options['usd_amount'] ?> USD</span></div>
                                <div class="resynct-label">Original room rate</div>
                            </div>
                            -->
                            <div class="resynct-line">
                                <div class="resynct-value"><span class="resynct-discount"></span></div>
                                <div class="resynct-label">To be paid with rePoints</div>
                            </div>
                            <div class="resynct-line">
                                <div class="resynct-value"><span class="resynct-final-rate"></span></div>
                                <div class="resynct-label">To be paid to hotel</div>
                            </div>
                            <!--
                            <div class="resynct-line">
                                <div class="resynct-value"><span class="resynct-earn-points">350</span></div>
                                <div class="resynct-label">rePoints you will earn</div>
                            </div>
                            -->

                            <div class="resynct-clearfix"></div>
                            <div class="resynct-widget resynct-radius resynct-save-selection-btn resynct-block"><div class="resynct-font resynct-text resynct-white resynct-bold">Apply & Book Now</div></div>
                        </div>

                    <?php else: ?>
                        <div class="resynct-slider-container">
                            <div class="resynct-icon"><div class="resynct-top-label">Redeem:</div></div>
                            <div class="slider" id="rs-slider-value-slider"></div>
                            <input type="text" id="resynct-slider-value" name="selection" value="500" style="display: none;">
                        </div>
                        <div class="resynct-values-container">
                            <div class="resynct-line"><div class="resynct-label">Original room rate</div><div class="resynct-value"><span class="resynct-room-rate"><?= $options['usd_amount'] ?> USD</span></div></div>
                            <div class="resynct-line"><div class="resynct-label">To be paid with rePoints</div><div class="resynct-value"><span class="resynct-discount">520 USD</span></div></div>
                            <div class="resynct-line"><div class="resynct-label">To be paid to hotel</div><div class="resynct-value"><span class="resynct-final-rate">520 USD</span></div></div>
                            <div class="resynct-line"><div class="resynct-label">rePoints you will earn</div><div class="resynct-value"><span class="resynct-earn-points">350</span></div></div>

                            <div class="resynct-clearfix"></div>
                            <div class="resynct-widget resynct-radius resynct-save-selection-btn resynct-block"><div class="resynct-font resynct-text resynct-white resynct-bold">Apply & Book Now</div></div>
                        </div>
                    <?php endif; ?>

                <div class="resynct-clearfix"></div>
            </div>
        </div>


        <div class="resynct-clearfix"></div>

@endsection

