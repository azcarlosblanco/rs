@extends('iframes.layout')

@section('content')

<div class="resynct-confirm-widget resynct-font" id="rs_confirm_container">
    <div class="resynct-ifr-left">
        <div class="resynct-logo-lg"></div>
    </div>
    <div class="resynct-ifr-content">
        <div class="resynct-text-block resynct-col-1">
            <div>
                <span class="resynct-value">{{ $data['earned_points'] }}</span>
                <span class="resynct-label"><span>rePoints to be earned</span></span>
            </div>
            <div>
                <span class="resynct-value">{{ $data['spent_points'] }}</span>
                <span class="resynct-label"><span>rePoints to be redeemed</span></span>
            </div>
        </div>
        <div class="resynct-text-block resynct-col-2">
            <div>
                <span class="resynct-value">{{ Format::usMoneyvalue($data['room_rate'], $data['currency']) }}</span>
                <span class="resynct-label"><span>Original room price</span></span>
            </div>
            <div>
                <span class="resynct-value">{{ Format::usMoneyvalue($data['discount'], $data['currency']) }}</span>
                <span class="resynct-label"><span>To be paid with rePoints</span></span>
            </div>
        </div>
        <div class="resynct-text-block resynct-col-3">
            <div class="resynct-center resynct-highlighted-value">{{ Format::usMoneyvalue($data['final_rate'], $data['currency']) }}</div>
            <div class="resynct-center resynct-label resynct-total-label">To be paid to hotel<br><span><small><i>Including taxes on original price</i></small></span></div>
        </div>
    </div>
    <div class="clearfix"></div>

</div>

@endsection