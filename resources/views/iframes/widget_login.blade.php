@extends('iframes.layout')

@section('content')

    @if ($user)

        <div class="resynct-authenticated">
            <div class="resynct-coin resynct-widget resynct-radius">
                <div class="resynct-font resynct-white resynct-text">
                    {{--<a href="{{ $profile_url }}" target="_blank">--}}
                        <span class="user-info">{{ $user->current_points_amount }} pts</span>
                    {{--</a>--}}
                        <span class="logout-link"><a href="javascript: ;">Logout</a></span>
                </div>
            </div>
        </div>

    @else

        <div id="resynct-login" class="resynct-register-window">
            <div class="resynct-coin resynct-widget resynct-radius">
                <div class="resynct-font resynct-text resynct-white">Sign In</div>
            </div>
        </div>

    @endif

@endsection

