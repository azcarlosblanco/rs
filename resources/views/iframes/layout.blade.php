<!doctype html>
<html lang="en" style="margin: 0; padding: 0;">
<head>
    <meta charset="UTF-8">
    <title>reSynct</title>

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,400italic,600italic,700,700italic|Raleway:400,500,400italic,500italic,600,600italic,700,700italic,300,300italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ url('css/widgets_new.css') }}">


    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    {{--<script type="text/javascript" src="http://localhost/ReSynct/public/js/jquery-1.11.3.min.js"></script>--}}
    <script type="text/javascript" src=" {{ elixir_uri('js/widgets.js') }} "></script>

    <script type="text/javascript">
        @if (isset($options))
        $(document).ready(function(){
            ReSynct.init({!! json_encode($options) !!});
        });
        @endif
    </script>

</head>
<body style="margin: 0; padding: 0;" data-type="{{ isset($type) ? $type : 'unknown' }}" data-width="{{ isset($width) ? $width : 0 }}" data-height="{{ isset($height) ? $height : 0 }}" data-resize_container="{{ (isset($resize_container) && $resize_container) ? '1' : '0' }}">

    @yield('content')

</body>
</html>
