
@extends('back')

@section('content')

<h2>Contact us with any questions</h2>

{!! Form::open() !!}

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('category', 'Category:') !!}
                    {!! Form::select('category', $categories, null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('text', 'Body:') !!}
                    {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>


<div class="row">
    <div class="col-md-2">
        {!! Form::submit('Send', ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>

{!! Form::close() !!}

@endsection