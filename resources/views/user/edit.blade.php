@extends('back')
@section('content')

    <h2>Edit User {{ $user->getFullName() }}</h2>

    {!! Form::model($user, ['method' => 'PUT', 'url' => route('users.update', ['users' => $user->id])]) !!}

        <div class="row">
            <div class="col-md-3">
                @include('form.select', ['name' => 'role', 'options' => $roles, 'label' => 'Role:'])
            </div>
            <div class="col-md-3 role_only role_member" style="display: none">
                @include('form.selectize', ['name' => 'member_id', 'label' => 'Member:', 'selected' => $user->member])
            </div>
            <div class="col-md-3 role_only role_hotel" style="display: none">
                @include('form.selectize', ['name' => 'hotel_id', 'label' => 'Hotel:', 'selected' => $user->hotel])
            </div>
            <div class="col-md-3">
                @include('form.checkbox', ['name' => 'active', 'label' => 'Active'])
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                @include('form.string', ['name' => 'firstname', 'label' => 'Name:'])
            </div>
            <div class="col-md-3">
                @include('form.string', ['name' => 'lastname', 'label' => 'Surname:'])
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                @include('form.string', ['name' => 'email', 'label' => 'E-mail:'])
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                @include('form.password', ['name' => 'password', 'label' => 'Password:'])
                @include('form.password', ['name' => 'password_confirmation', 'label' => 'Confirm password:'])
            </div>
        </div>

    <div class="row">
        <div class="col-md-3">
            {!! Form::submit('Save changes', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>

    {!! Form::close() !!}


@endsection

@section('footer_js_ready')
    // <script>
        App.singleSelectize('#member_id', {url: '{{ route('members.cb') }}' });
        App.singleSelectize('#hotel_id', {url: '{{ route('hotels.cb') }}' });
        var roleChanged = function(){
            $('.role_only').hide();
            var role = $('#role').val();
            $('.role_'+role).show();
        };
        $('#role').on('change', roleChanged);
        roleChanged();
@parent
@endsection
