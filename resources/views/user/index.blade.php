
<h2>Users</h2>

    <form class="filter_form container pull-left" style="padding: 0">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('filter[role]', 'Role:') !!}
                    {!! Form::select('filter[role]', $roles, array_get($filter, 'role'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('filter[fulltext]', 'Fulltext:') !!}
                    {!! Form::text('filter[fulltext]', array_get($filter, 'fulltext'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                @include('form.selectize', ['name' => 'filter[member_id]', 'id' => 'member_id', 'label' => 'Member:', 'selected' => $selected_member])
            </div>
            <div class="col-md-3">
                @include('form.selectize', ['name' => 'filter[hotel_id]', 'id' => 'hotel_id', 'label' => 'Hotel:', 'selected' => $selected_hotel])
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                {!! Form::submit('Search', ['class' => 'btn btn-primary form-control submit_filter']) !!}
            </div>
            <div class="col-md-2">
                {!! Form::submit('Reset filters', ['class' => 'btn btn-default form-control reset_filter']) !!}
            </div>
        </div>
    </form>
<div class="clearfix"></div>


@if (count($results))

    <div class="paginator pull-right"></div>

    <table class="table">
        <thead>
            <tr>
                <th>Active</th>
                <th>Role</th>
                <th>Name</th>
                <th data-sortable="email">E-mail</th>
                <th data-sortable="created_at">Created</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>

        @foreach ($results as $user)
            <tr>
                <td>{{ ($user->confirmed && $user->active) ? 'Yes' : 'No' }}</td>
                <td>{{ ucfirst($user->role) }}</td>
                <td>{{ $user->getFullName() }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->created_at }}</td>
                <td><a href="{{ route('users.edit', ['users' => $user->id]) }}">Detail</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="paginator pull-right"></div>

@else

    <div class="alert alert-info" style="margin: 20px 0;">
        No results found.
    </div>

@endif

<script type="text/javascript">
    $(document).ready(function(){
        App.singleSelectize('#member_id', {url: '{{ route('members.cb') }}' });
        App.singleSelectize('#hotel_id', {url: '{{ route('hotels.cb') }}' });
    });

</script>
