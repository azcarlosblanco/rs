@extends('emails/layout')

@section('content')
    <!-- Content -->
    <tr>
        <td width="40"></td>
        <td>
            <p><font size="2">Hi <strong><?= $member->firstname ?></strong>,</font></p>
            <p><font size="2"></font></p>
            <p><font size="2">
                    Guess who’s in?<br>
                    <br>
                    You are now on your way to earning with reSynct, the fastest growing hotel loyalty program designed with you in mind!<br>
<br>
                    Please follow <a href="{{ route('AccountConfirmation', ['id' => $member->id, 'token' => $member->confirmation_token]) }}">this link</a> to activate your account.

                    <ol>
                        <li>Activate your account by clicking above link and confirming your e-mail address.</li>
                        <li>Sign in to your reSynct member account via the "Sign In" icon on any partner hotel website.</li>
                        <li>Checkout and collect your rePoints* once your stay is confirmed by the hotel.</li>
                        <li>Redeem rePoints at any partner hotel towards room costs for future travel.</li>
                    </ol>
                    <br>
                    Thanks again for joining the most rewarding loyalty network ever!
                </font></p>
        </td>
        <td width="40"></td>
    </tr>


@endsection