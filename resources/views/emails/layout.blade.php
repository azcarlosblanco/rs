<?php
    $heading = isset($heading) ? $heading : 'reSynct';
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?=$heading?></title>
</head>
<body style="font-family: sans-serif, Verdana, Arial; font-size: 12px; background-color: #fafafa; width: 100%; border-collapse: collapse; color: #424242;" bgcolor="#f5f5f5" link="#1791cd" vlink="#6996b2" alink="#1791cd" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">

<!-- 100%x100% container -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr><td height="50"></td></tr>
<tr><td align="center">
    <!-- Internal table -->
    <table width="646" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border-radius: 4px;">

        <!-- Margin top -->
        <tr><td colspan="3" height="40"></td></tr>
        <!-- /Margin top -->


        <!-- Header -->
        <tr>
            <td width="40"></td>
            <td colspan="2"><!-- Remove padding-right -->
                <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="middle" width="50%">
                        <a href="https://www.resynct.com/" target="_blank"><img src="https://s3.amazonaws.com/resynct/widgets/logo-new.png" height="50" title="reSynct" alt="reSynct"></a>
                    </td>
                    <td align="right" valign="middle" width="50%">
                        <?php if (isset($head_info) && $head_info): ?>
                        <font color="#FFFFF;" size="2">
                            <span style="padding: 8px 20px; background: #396dff;">
                                <strong><?= $head_info ?></strong>
                            </span>
                        </font>
                        <?php else: ?>
                            &nbsp;
                        <?php endif; ?>

                    </td>
                </tr>
                </table>
            </td>
        </tr>
        <!-- /Header -->

        <!-- Margin between Header and Content -->
        <tr><td height="30" colspan="3"></td><td>
        <!-- /Margin between Header and Content -->


        @yield('content')


        <tr><td height="30" colspan="3"></td><td>
        <tr>
            <td width="40"></td>
            <td><font size="2" color="#616161">Have any questions? Just shoot us an email at <a href="mailto:info@resynct.com" target="_blank">info@resynct.com</a>. or follow, like, Tweet or chat with us on social media if that’s more your thing.</font></td>
            <td width="40"></td>
        </tr>


        <tr><td height="20" colspan="3"></td><td>
        <tr>
            <td width="40"></td>
            <td><font size="2" color="#616161">Loyally,<br>The reSynct Team</font></td>
            <td width="40"></td>
        </tr>



        <tr><td height="40" colspan="3"></td><td>
        <!-- /Content -->
    </table>


    <!-- Footer -->
    <table width="646" border="0" cellpadding="0" cellspacing="0">
    <tr><td align="center" colspan="3">

        <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr><td height="20" colspan="2"></td></tr>

        <tr>
            <td width="50%" height="50" align="left" valign="top">
                <font color="#bcbcbc" size="2">Copyright &#169; <?= date('Y') ?> reSynct.com. All rights reserved.</font>
            </td>
            <td width="50%" height="50" align="right" valign="top">
                <a href="http://www.google.com/" target="_blank"><img height="32" width="32" src="https://s3.amazonaws.com/resynct/widgets/email/facebook-icon.png" title="reSynct Facebook" alt="reSynct Facebook"></a>
                &nbsp; &nbsp;
                <a href="http://www.google.com/" target="_blank"><img height="32" width="32" src="https://s3.amazonaws.com/resynct/widgets/email/twitter-icon.png" title="reSynct Twitter" alt="reSynct Twitter"></a>
                &nbsp; &nbsp;
                <a href="http://www.google.com/" target="_blank"><img height="32" width="32" src="https://s3.amazonaws.com/resynct/widgets/email/linkedin-icon.png" title="reSynct LinkedIn" alt="reSynct LinkedIn"></a>
            </td>
        </tr>
        <tr><td height="30" colspan="2"></td></tr>
        </table>

    </td></tr>
    </table>
    <!-- /Footer -->

</table><!-- Internal table -->
</table><!-- 100%x100% container -->

</body>
</html>
