@extends('emails/layout')

@section('content')
    <!-- Content -->
    <tr>
        <td width="40"></td>
        <td>
            <p><font size="2">Hi <strong><?= $user->firstname ?></strong>,</font></p>
            <p><font size="2"></font></p>
            <p><font size="2">
                    Looks like you need a new password. If you did not request a reset, please disregard this email. If this is correct, simply <a href="{{ url('password/reset/'.$token) }}">click here</a> and follow the instructions to reset your credentials.

                </font></p>
        </td>
        <td width="40"></td>
    </tr>

@endsection
