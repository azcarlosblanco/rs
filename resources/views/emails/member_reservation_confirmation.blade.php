<?php
/*
function url ($value)   { return "http://resynct.local{$value}"; }
function route ($value) { return url("/{$value}"); }

$res                   = (object)[];
$res->hotel            = (object)[];
$res->hotel->real_name = 'Greenwich Hotel';
$res->code             = 'BFC-60736-1171XWX';
$res->total_amount     = '200';
$res->points           = '500';
$res->points_spent     = '300';
$res->points_earned    = '350';
$res->discount_amount  = '40';
*/
?>

@extends('emails/layout')

@section('content')
<!-- Content -->
<tr>
    <td width="40"></td>
    <td>
        <p><font size="2">Hello <strong><?= $res->member->firstname ?></strong>,</font></p>
        <p><font size="2"></font></p>
        <p><font size="2">Congrats on your recent booking with our valued partner the <?= $res->hotel->real_name ?>. You didn’t just get a great hotel deal, you also supported the growth of the reSynct community!
<br>
                <br>
                Below are the important details of your trip.
                </font></p>
    </td>
    <td width="40"></td>
</tr>

<!-- Margin between Greeting and First table -->
<tr><td height="30" colspan="3"></td><td>
        <!-- /Margin between Greeting and First table -->

        <!-- First table: Resynct Details -->
<tr>
    <td width="40"></td>
    <td>
        <table width="100%" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th align="left" colspan="3" style="border-bottom: 2px solid #396dff;" height="26" valign="top">
                    <font size="3" color="#000000"><strong>reSynct</strong></font>
                </th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td              height="44" width="130" style="border-bottom: 1px solid #f5f5f5;"></td>
                <td align="left" height="40" width="160" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong>*rePoints Earned</strong></font></td>
                <td align="left" height="40" width="" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong><?=$res->points_earned?> pts.</strong></font></td>
            </tr>
            <tr>
                <td              height="44" width="130" style="border-bottom: 1px solid #f5f5f5;"></td>
                <td align="left" height="40" width="160" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong>rePoints Redeemed</strong></font></td>
                <td align="left" height="40" width="" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong><?=$res->points_spent?> pts.</strong></font></td>
            </tr>
            <tr>
                <td              height="44" width="130" style="border-bottom: 1px solid #f5f5f5;"></td>
                <td align="left" height="40" width="160" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong>Paid with rePoints</strong></font></td>
                <td align="left" height="40" width="" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong><?= Format::usMoneyValue($res->discount_amount * $res->currency_multiplier, $res->currency) ?></strong></font></td>
            </tr>
            <tr>
                <td              height="44" width="130" style="border-bottom: 1px solid #f5f5f5;"></td>
                <td align="left" height="40" width="160" style="border-bottom: 1px solid #f5f5f5;"><font size="2">**Your rePoints Balance</font></td>
                <td align="left" height="40" width="" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong><?= (int)$res->member->current_points_amount ?> pts.</strong></font></td>
            </tr>
            </tbody>
        </table>
    </td>
    <td width="40"></td>
</tr>
<!-- /First table: Resynct Details -->


<tr>
    <td width="40"></td>
    <td><font size="2" color="#616161">
            * rePoints are earned once the hotel has confirmed your stay and are not available for redemption until confirmed.<br>
            ** Account balance reflects only stays confirmed by the hotel.
        </font></td>
    <td width="40"></td>
</tr>


<!-- Margin between First table and Second table -->
<tr><td height="30" colspan="3"></td><td>
        <!-- /Margin between First table and Second table -->

        <!-- Second table: Reservation Details -->
<tr>
    <td width="40"></td>
    <td>
        <table width="100%" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th align="left" colspan="3" style="border-bottom: 2px solid #396dff;" height="26" valign="top">
                    <font size="3" color="#000000"><strong>Reservation Details</strong></font>
                </th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td              height="44" width="130" style="border-bottom: 1px solid #f5f5f5;"></td>
                <td align="left" height="40" width="160" style="border-bottom: 1px solid #f5f5f5;"><font size="2">Reservation Code</font></td>
                <td align="left" height="40" width="" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong><?= $res->code ?></strong></font></td>
            </tr>
            <tr>
                <td              height="44" width="130" style="border-bottom: 1px solid #f5f5f5;"></td>
                <td align="left" height="40" width="160" style="border-bottom: 1px solid #f5f5f5;"><font size="2">Partner Hotel</font></td>
                <td align="left" height="40" width="" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong><?= $res->hotel->getWebsiteLink() ?></strong></font></td>
            </tr>
            <tr>
                <td              height="44" width="130" style="border-bottom: 1px solid #f5f5f5;"></td>
                <td align="left" height="40" width="160" style="border-bottom: 1px solid #f5f5f5;"><font size="2">Check-in</font></td>
                <td align="left" height="40" width="" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong><?= DateFormat::getUserDate($res->check_in_date) ?></strong></font></td>
            </tr>
            <tr>
                <td              height="44" width="130" style="border-bottom: 1px solid #f5f5f5;"></td>
                <td align="left" height="40" width="160" style="border-bottom: 1px solid #f5f5f5;"><font size="2">Check-out</font></td>
                <td align="left" height="40" width="" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong><?= DateFormat::getUserDate($res->check_out_date) ?></strong></font></td>
            </tr>
            <tr>
                <td              height="44" width="130" style="border-bottom: 1px solid #f5f5f5;"></td>
                <td align="left" height="40" width="160" style="border-bottom: 1px solid #f5f5f5;"><font size="2">Standard Rate</font></td>
                <td align="left" height="40" width="" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong><?= Format::usMoneyValue($res->total_amount_in_currency, $res->currency) ?></strong></font></td>
            </tr>
            <tr>
                <td              height="44" width="130" style="border-bottom: 1px solid #f5f5f5;"></td>
                <td align="left" height="40" width="160" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong>Your Rate</strong></font></td>
                <td align="left" height="40" width="" style="border-bottom: 1px solid #f5f5f5;"><font size="2"><strong><?= Format::usMoneyValue($res->paid_amount * $res->currency_multiplier, $res->currency) ?></strong></font></td>
            </tr>
            </tbody>
        </table>
    </td>
    <td width="40"></td>
</tr>
<!-- /Second table: Reservation Details -->

<tr><td height="20" colspan="3"></td><td>
<tr>
    <td width="40"></td>
    <td><font size="2" color="#616161">Enjoy your upcoming stay with the Greenwich Hotel! Afterall, you’ve earned it.</font></td>
    <td width="40"></td>
</tr>


@endsection