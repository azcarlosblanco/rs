<?php

$body_background = isset($body_background) ? $body_background : null;

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>reSynct</title>

    <script type="text/javascript" src="{{ elixir_uri('js/all.js') }}"></script>

    <script type="text/javascript" src="{{ elixir_uri('js/front.js') }}"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="{{ elixir_uri('css/front_merged.css') }}">

    @yield('head')

    <style type="text/css">
    </style>
</head>

<body data-background="{{$body_background}}">
<?php /*
<div id="error-container"   class="hidden"></div>
<div id="message-container" class="hidden"></div>
*/ ?>

@if ( ! isset($without_modals) || ! $without_modals)
    @include('auth/login_modal')
    @include('auth/password_modal')
    @include('auth/reset_modal')
@endif

@if ( ! isset($without_header) || ! $without_header)
<!-- #header -->
<div id="header" data-adjust-on-modal="true">
    <div class="container">
        <div class="row">
            <div class="logo col-xs-6 col-lg-2 text-left">
                <a href="{{ route('home.getMember') }}"><img src="{{ url('https://s3.amazonaws.com/resynct/widgets/resynct-logo.png') }}"></a>
            </div>

            <?php /* stupied bootstrap's column ordering: http://getbootstrap.com/css/#grid-column-ordering
                    it renders as [logo, menu, signin] in screen-lg and as [logo, signin, menu]
                    and because in -md- and less screen logo + signin takes 12cols, menu wrapped to new line */?>
            <div class="signinup col-xs-6 col-md-6 col-lg-3 col-lg-push-7 text-right">
                @if (Auth::user())
                    <a href="{{route('MemberReservationTable')}}" class="sign-up btn btn-round">My Profile</a>
                @else
                    <a class="sign-in btn btn-round" data-modal-show="modal" data-target="#modal-login">LOG IN</a>
                    <a href="{{ route('home.getMember') }}#sign-up" data-scroll-to="#sign-up" class="sign-up btn btn-round">SIGN UP</a>
                @endif
            </div>

            <div class="menu col-xs-12 col-lg-7 col-lg-pull-3 text-center">
                <ul class="list-inline">
                    <li><a href="{{route('home.getMember')}}#about" data-scroll-to="#about">ABOUT RESYNCT</a></li>
                    <li><a href="{{route('home.getMember')}}#how-it-works" data-scroll-to="#how-it-works">HOW IT WORKS</a></li>
                    <li><a href="{{route('home.getMember')}}#benefits" data-scroll-to="#benefits">BENEFITS</a></li>
                    <li><a href="{{trans('link.faq_members')}}">FAQ</a></li>
                </ul>
            </div>

        </div>
    </div>
</div>
<!-- #header -->
@endif



@yield('content')



@if ( ! isset($without_footer) || ! $without_footer)
<!-- #footer -->
<div id="footer">
    <div class="container">
        <div class="row">
            <div class="label col-xs-12 col-md-3">
                reSynct, 30 Church Street, NY NY 10007
            </div>

            <div class="social col-xs-12 col-md-6">
                <ul class="list-inline">
                    <li><a href="{{trans('link.facebook')}}" target="_blank"><div class="sprite-icons si-facebook"></div></a></li>
                    <li><a href="{{trans('link.instagram')}}" target="_blank"><div class="sprite-icons si-twitter"></div></a></li>
                    <li><a href="{{trans('link.linkedin')}}" target="_blank"><div class="sprite-icons si-linkedin"></div></a></li>
                </ul>
            </div>

            <div class="links col-xs-12 col-md-3">
                <ul class="list-inline">
                    <li><a href="{{trans('link.privacy_statement')}}" target="_blank">Privacy Policy</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- #footer -->
@endif



<script type="text/javascript">
    // @todo - move to external file
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    setTimeout(function(){
        $('.flash_msg').remove();
    }, 10000);


    @yield('footer_js')

    $(document).ready(function(){
        @yield('footer_js_ready')
    });
</script>
</body>
</html>
