<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>reSynct</title>

    <script type="text/javascript" src="{{ elixir_uri('js/all.js') }}"></script>
    <script type="text/javascript" src="{{ elixir_uri('js/front.js') }}"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="{{ elixir_uri('css/front_merged.css') }}">

    @yield('head')

    <style type="text/css">
    </style>
</head>

<body>

<div class="popup" data-mode="{{(isset($data_mode) ? $data_mode : '')}}">
    @yield('content')
</div>


<script type="text/javascript">
    @yield('footer_js')

    $(document).ready(function(){
        @yield('footer_js_ready')
    });
</script>
</body>
</html>
