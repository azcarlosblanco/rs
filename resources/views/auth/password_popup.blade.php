<!-- resources/views/auth/register.blade.php -->
@extends('front_popup', ['data_mode' => 'password'])

@section('content')
<div class="container-fluid padding">


<form id="login_form" method="POST" action="{{ url('password/email') }}">
{!! csrf_field() !!}

<div class="row logo">
    <div class="col-xs-12"><img src="{{ url('https://s3.amazonaws.com/resynct/widgets/resynct-logo.png') }}"></div>
</div>

@if (session('status'))
    <div class="row footer">
        <div class="col-xs-12 text-center">
            <div id="success_top">
                {{ session('status') }}
            </div>

            <br>
            <br>
            <a href="{{ url('auth/login/popup') . '?' . http_build_query($_GET) }}">Continue to login page.</a>
        </div>
    </div>

@else

    <div class="row message">
        <div class="col-xs-12"><h3>Enter your e-mail address and we'll get you back on track.</h3></div>
    </div>


    @if ($errors->any())
    <div class="row">
        <div class="col-xs-12 error_top">
            {{ $errors->first() }}
        </div>
    </div>
    @endif


    <div class="row input">
        <div class="col-xs-12">
            @include('form.string', ['name' => 'email', 'label' => 'Your E-mail', 'value' => old('email'), 'show_error' => false])
        </div>
    </div>

    <div class="row submit">
        <div class="col-xs-12">
            <button type="submit" id="submit_btn" class="btn btn-round btn-blue">Send Password Reset Link</button>
        </div>
    </div>

    <div class="row footer">
        <div class="col-xs-12 text-center nowrap">
            <a href="{{ url('auth/login/popup') . '?' . http_build_query($_GET) }}">Back to login page</a>
        </div>
    </div>
@endif
</form>


</div>
@include('auth.popup_resize', ['width' => 430, 'height' => 560])


@endsection
