
<script type="text/javascript">
    window.onload = function(){
        var width = {{ $width }};
        var height = {{ $height }};

        window.resizeTo(width, height);

        var a = <?= array_get($_GET, 'a', 'null') ?>;
        var i = <?= array_get($_GET, 'i', 'null') ?>;
        var g = <?= array_get($_GET, 'g', 'null') ?>;
        var f = <?= array_get($_GET, 'f', 'null') ?>;

        if (a === null || i === null || g === null || f === null) {
            return;
        }
        var left = parseInt(a + ((g - width) / 2), 10);
        var top = parseInt(i + ((f-height) / 2.5), 10);
        window.moveTo(left, top);
    };
</script>