@extends('front', ['body_background' => 'traveler-wide'])

@section('content')
<!-- resources/views/auth/reset.blade.php -->

<div class="pseudo-modal">
<div class="container-fluid">
    <form method="POST" action="{{ url('password/reset') }}">
    @include('auth/reset_partial', ['mode' => 'standalone'])
    </form>
</div>
</div>

<script>
    $('#reset-form').ResetFormPlugin();
</script>
@endsection

<?php unset($is_email_requeried, $is_password_required); ?>
