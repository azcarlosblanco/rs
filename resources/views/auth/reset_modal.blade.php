<?php
$params = [
    'modal_id'    => 'modal-reset',
    'form_id'     => 'modal-form-reset',
    'form_name'   => 'modal-form-reset',
    'form_action' => url('password/reset'),
];
$params += [
    'section_name_body'   => "{$params['modal_id']}-body",
    'section_name_js'     => "{$params['modal_id']}-js",
];
?>
@extends('partials/modal', $params)

@section($params['section_name_body'])
    <!-- resources/views/auth/reset_modal.blade.php -->
    @include('auth/reset_partial', ['mode' => 'modal'])
@endsection


@section($params['section_name_js'])
<script>
    $('#{{$params["form_id"]}}').ResetFormPlugin();
</script>
@endsection
