<!-- resources/views/auth/login.blade.php -->
@extends('front_popup')

@section('content')
<div class="container-fluid padding">


<form id="login_form" method="POST" action="{{ url('auth/login/popup') }}?popup=1">
{!! csrf_field() !!}


<div class="row logo">
    <div class="col-xs-12"><img src="{{ url('https://s3.amazonaws.com/resynct/widgets/resynct-logo.png') }}"></div>
</div>


<div class="row message">
    <div class="col-xs-12"><h3>Sign in & collect your rewards.</h3></div>
</div>


@if ($errors->any())
<div class="row">
    <div class="col-xs-12 error_top">
        {{ $errors->first() }}
    </div>
</div>
@endif


<div class="row input">
    <div class="col-xs-12">
        @include('form.string', ['name' => 'email', 'label' => 'Your E-mail', 'value' => old('email'), 'show_errors' => false])
    </div>
</div>


<div class="row input">
    <div class="col-xs-12">
        @include('form.password', ['name' => 'password', 'label' => 'Password', 'show_errors' => false])
    </div>
</div>


<div class="row input">
    <div class="col-xs-12">
        @include('form.checkbox_custom', ['name' => 'remember', 'label' => 'Remember me'])
    </div>
</div>


<div class="row submit">
    <div class="col-xs-6">
        <button type="submit" class="btn btn-round btn-blue">Sign in</button>
    </div>
    <div class="col-xs-6 text-right nowrap">
        <a class="forgot_password" href="{{ url('password/email/1') . '?' . http_build_query($_GET) }}">Forgot password?</a>
    </div>
</div>



<!-- <div class="row footer"> -->
<!--     <div class="col&#45;xs&#45;12 text&#45;center"> -->
<!--         Not a member? <a href="{{ url('auth/register/popup') . '?' . http_build_query($_GET) }}">Sign Up Now!</a> -->
<!--     </div> -->
<!-- </div> -->
</form>

<div class="row under_the_line">
    <div class="col-xs-12 text-left">
        Not a member? <a href="{{ url('auth/register/popup') . '?' . http_build_query($_GET) }}">Sign Up Now!</a>
    </div>
</div>


</div>
@include('auth.popup_resize', ['width' => 430, 'height' => 560])


@endsection
