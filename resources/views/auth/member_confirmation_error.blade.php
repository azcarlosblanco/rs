
@extends('front')


@section('content')

    <h1>Congratulations {{ $user->name }}!<h1>
    <h2>Your account was created successfully.</h1>
    <p>Please follow the instructions sent to {{ $user->email }} in order to activate your account.<p>

@endsection
