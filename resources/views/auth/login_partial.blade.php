<?php /*
@if ($errors->any())
<div id="error-container">
    <div class="error"><i class="material-icons md-24">&#xE001;</i> {{ $errors->first() }}</div>
</div>
@endif

@if (session('status'))
<div id="message-container">
    <div class="message"><i class="material-icons md-24">&#xE876;</i> {{ session('status') }}</div>
</div>
@endif
<div class="error-container"></div>
<div class="message-container"></div>
@if ($errors->any())
<script>renderError($('#{{$params["modal_id"]}}'), ["{{ $errors->first() }}"])</script>
@endif
@if (session('status'))
<script>renderMessage($('#{{$params["modal_id"]}}'), ["{{ session('status') }}"])</script>
@endif
*/ ?>



{!! csrf_field() !!}

<div class="row logo">
    <div class="col-xs-12"><img src="{{ url('https://s3.amazonaws.com/resynct/widgets/resynct-logo.png') }}"></div>
</div>

<div class="row message">
    <div class="col-xs-12"><h3>Sign in & collect your rewards.</h3></div>
</div>

<div class="row input">
    <div class="col-xs-12">
        @include('form.string', ['name' => 'email', 'label' => 'Your E-mail', 'value' => old('email')])
    </div>
</div>

<div class="row input">
    <div class="col-xs-12">
        @include('form.password', ['name' => 'password', 'label' => 'Password'])
    </div>
</div>

<div class="row input">
    <div class="col-xs-6">
        @include('form.checkbox_custom', ['name' => 'remember', 'label' => 'Remember me'])
    </div>
    <div class="col-xs-6 forgot-password text-right">
        @if ($mode == 'modal')
            <a data-modal-show="modal" data-target="#modal-password" href="{{ url('password/email') }}">Forgot password?</a>
        @else
            <a href="{{ url('password/email') }}">Forgot password?</a>
        @endif
    </div>
</div>

<div class="row submit">
    <div class="col-xs-12">
        <button type="submit" class="btn btn-round btn-blue">Sign in</button>
    </div>
</div>

<div class="row footer">
    <div class="col-xs-12 text-center">
        @if ($mode == 'modal')
            Not a member? <a data-modal-hide="modal" data-scroll-to="#sign-up">Sign Up Now!</a>
        @else
            Not a member? <a href="{{ url('auth/register') . '?' . http_build_query($_GET) }}">Sign Up Now!</a>
        @endif
    </div>
</div>
