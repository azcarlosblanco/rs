<!-- resources/views/auth/register.blade.php -->
@extends('front_popup', ['data_mode' => 'password'])

@section('content')
<div class="middle-container">
<div class="container-fluid padding middle">


<div class="row logo">
    <div class="col-xs-12"><img src="{{ url('https://s3.amazonaws.com/resynct/widgets/resynct-logo.png') }}"></div>
</div>


<div class="row message">
    <div class="col-xs-12"><h3>Thank you!</h3></div>
</div>


<div class="row">
    <div class="col-xs-12">
        @if ($user->current_points_amount >= \App\Hotel::MIN_REDEEMABLE_POINT_AMOUNT)
            You have just earned {{ $user->current_points_amount }} rePoints which you can start using now to get a discount on your next booking.
        @elseif ($user->current_points_amount > 0)
            You have just earned {{ $user->current_points_amount }} rePoints. Start booking to get more rePoints and get future discounts.
        @else
            You have been registered successfully and you can start earning rePoints right now!
        @endif
    </div>
</div>


<div class="row submit">
    <div class="col-xs-12">
        <br>
        <button type="submit" id="submit_btn" class="btn btn-round btn-blue" onclick="window.close()">Close and start Earning rePoints</button>
    </div>
</div>


</div>
</div>
@include('auth.popup_resize', ['width' => 500, 'height' => 340])

@endsection
