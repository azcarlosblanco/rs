<!-- resources/views/auth/login.blade.php -->
@extends('front_popup', ['data_mode' => 'registration'])

@section('content')

<div class="benefits">
<div class="middle-container">
    <div class="middle">
        <div class="benefit">
            <div class="benefit-icon bi-1"></div>
            <div class="text">
                <h2>Free Registration!</h2>
                <p>Create your account to join reSynct and track your RePoints. Book this  hotel and gain access to exclusive member only promotions within the reSynct community and on websites you see this (RS-ICON).</p>
            </div>
        </div>

        <div class="benefit">
            <div class="benefit-icon bi-2"></div>
            <div class="text">
                <h2>Book & Earn</h2>
                <p>Book on this reSynct partner hotel website and earn Repoints for your next stay. The more you stay, the more you earn. Look for the logo, log in and book.</p>
            </div>
        </div>

        <div class="benefit">
            <div class="benefit-icon bi-3"></div>
            <div class="text">
                <h2>Redeem RePoints</h2>
                <p>Upon checkin RePoints will be applied as a currency toward your stay.  Easy, cost effective and access to a VIP network where you’re the star of the show!</p>
            </div>
        </div>
    </div>
</div>
</div>



<div class="form">
<div class="container-fluid padding">

    <form id="reg_form" method="POST" action="{{Request::fullUrl()}}" autocomplete="false">
    {!! csrf_field() !!}

    <div class="row logo">
        <div class="col-xs-12"><img src="{{ url('https://s3.amazonaws.com/resynct/widgets/resynct-logo.png') }}"></div>
    </div>


    <div class="row message">
        <div class="col-xs-12"><h3>Sign Up & Start Earning Today!</h3></div>
    </div>


    @if ($errors->any())
    <div class="row">
        <div class="col-xs-12 error_top">
            {{ $errors->first() }}
        </div>
    </div>
    @endif


    <div class="row input">
        <div class="col-xs-12">
            @include('form.string', ['name' => 'full_name', 'label' => 'Full name', 'value' => old('full_name'), 'show_errors' => false])
        </div>
    </div>


    <div class="row input">
        <div class="col-xs-12">
            @include('form.string', ['name' => 'email', 'label' => 'Your E-mail', 'value' => old('email'), 'show_errors' => false])
        </div>
    </div>


    <div class="row input">
        <div class="col-xs-12">
            @include('form.password', ['name' => 'password', 'label' => 'Password', 'show_errors' => false])
        </div>
    </div>

    <div class="row input">
        <div class="col-xs-12">
            @include('form.password', ['name' => 'password_confirmation', 'label' => 'Confirm password', 'show_errors' => false])
        </div>
    </div>

    <div class="row input">
        <div class="col-xs-12">
            <?=trans('front.By clicking "Sign Up" I agree to reSynct', [
                'privacy_statement'    => trans('link.privacy_statement')
            ])?>
        </div>
    </div>


    <div class="row submit">
        <div class="col-xs-6">
            <button type="submit" id="submit_btn" class="btn btn-round btn-blue">Sign Up Now</button>
        </div>
        <div class="col-xs-6 text-right nowrap">
            <a class="already_a_member" href="{{ url('auth/login/popup') . '?' . http_build_query($_GET) }}">Already a member?</a>
        </div>
    </div>

    </form>

</div>
</div>


<div id="privacy" class="privacy hidden">
    <div class="container-fluid">
    <span data-mode="close" class="close glyphicon glyphicon-remove-circle"></span>

    <div class="row">
        <div class="col-xs-12">
            @include('front.pages.privacy_content')
        </div><!-- .col-xs-12 -->
    </div><!-- .row -->
    <div class="row footer">
        <div class="col-xs-12 col-sm-offset-4 col-sm-4 text-center">
            <button data-mode="close" class="btn btn-blue">Close</button>
        </div><!-- .col-xs-12 -->
    </div><!-- .row -->
    </div><!-- .container-fluid -->
</div><!-- #privacy -->


@section('footer_js_ready')
@parent
//<script>
(function() {
    var $parent   = $('.popup[data-mode="registration"]');
    var $privacy  = $parent.find('#privacy');
    var $benefits = $parent.find('.benefits');
    var $form     = $parent.find('.form');

    // Events show/hide registering
    $privacy.on('show', function() {
        $benefits.addClass('hidden');
        $form.addClass('hidden');

        $privacy.removeClass('hidden');

        $('body').scrollTop(0);
    });
    $privacy.on('hide', function() {
        $privacy.addClass('hidden');

        $benefits.removeClass('hidden');
        $form.removeClass('hidden');
    });

    // Bind buttons to trigger show/hide
    $parent.find('form a.privacy').on('click', function(e) {
        $privacy.trigger('show');
        e.preventDefault();
        return false;
    });
    $privacy.find('[data-mode="close"]').on('click', function() { $privacy.trigger('hide'); });
}());
@endsection



@include('auth.popup_resize', ['width' => 800, 'height' => 640])

@endsection
