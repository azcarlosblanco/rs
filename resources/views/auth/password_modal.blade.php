<?php
$params = [
    'modal_id'    => 'modal-password',
    'form_id'     => 'modal-form-password',
    'form_name'   => 'modal-form-password',
    'form_action' => url('password/email'),
];
$params += [
    'section_name_body'   => "{$params['modal_id']}-body",
    'section_name_js'     => "{$params['modal_id']}-js",
];
?>
@extends('partials/modal', $params)

@section($params['section_name_body'])
    <!-- resources/views/auth/register.blade.php -->
    @include('auth/password_partial', ['mode' => 'modal'])
@endsection


@section($params['section_name_js'])
<script>
    $('#{{$params["form_id"]}}').PasswordFormPlugin();
</script>
@endsection
