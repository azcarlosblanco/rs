<!-- resources/views/auth/login.blade.php -->
@extends('front', ['body_background' => 'traveler-wide'])

<?php /*
@if ($errors->any())
<div id="error-container">
    <div class="error"><i class="material-icons md-24">&#xE001;</i> {{ $errors->first() }}</div>
</div>
@endif

@if (session('status'))
<div id="message-container">
    <div class="message"><i class="material-icons md-24">&#xE876;</i> {{ session('status') }}</div>
</div>
@endif
*/ ?>

@section('content')

<div class="pseudo-modal">
<div class="container-fluid">
    <form id="login-form" method="POST" action="{{ url('auth/login') }}">
    @include('auth/login_partial', ['mode' => 'standalone'])
    </form>
</div>
</div>

<script>
    $('#login-form').LoginFormPlugin();
</script>
@endsection
