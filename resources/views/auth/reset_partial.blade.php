{!! csrf_field() !!}

<input type="hidden" name="token" value="{{ (isset($token) ? $token : '') }}">

<div class="row logo">
    <div class="col-xs-12"><img src="{{ url('https://s3.amazonaws.com/resynct/widgets/resynct-logo.png') }}"></div>
</div>

<div class="row message">
    <div class="col-xs-12"><h3>Please enter your new password</h3></div>
</div>

<div class="row input">
    <div class="col-xs-12">
        @include('form.string', ['name' => 'email', 'label' => 'E-mail'])
    </div>
</div>

<div class="row input">
    <div class="col-xs-12">
        @include('form.password', ['name' => 'password', 'label' => 'Password'])
    </div>
</div>

<div class="row input">
    <div class="col-xs-12">
        @include('form.password', ['name' => 'password_confirmation', 'label' => 'Confirm password'])
    </div>
</div>

<div class="row submit">
    <div class="col-xs-12">
        <button type="submit" class="btn btn-round btn-blue">Reset Password</button>
    </div>
</div>

<div class="row footer">
    <div class="col-xs-12 text-center">
        @if ($mode == 'modal')
            <a data-modal-show="modal" data-target="#modal-login">Back to Login</a>
        @else
            <a href="{{ url('auth/login') . '?' . http_build_query($_GET) }}">Back to Login</a>
        @endif
    </div>
</div>
