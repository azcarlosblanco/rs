<?php
$params = [
    'modal_id'    => 'modal-login',
    'form_id'     => 'modal-form-login',
    'form_name'   => 'modal-form-login',
    'form_action' => url('auth/login'),
];
$params += [
    'section_name_body'   => "{$params['modal_id']}-body",
    'section_name_footer' => "{$params['modal_id']}-footer",
    'section_name_js'     => "{$params['modal_id']}-js",
];
?>

@extends('partials/modal', $params)

@section($params['section_name_body'])
    <!-- resources/views/auth/login_modal.blade.php -->
    @include('auth/login_partial', ['mode' => 'modal'])
@endsection


@section($params['section_name_js'])
<script>
    $('#{{$params["form_id"]}}').LoginFormPlugin();
</script>
@endsection
