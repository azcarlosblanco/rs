<!-- resources/views/auth/register.blade.php -->
@extends('front', ['body_background' => 'traveler-wide'])

<?php /*
<?php
// $is_email_requeried   = ($errors->has('email') && (stripos($errors->first('email'), 'required') !== false));
?>
@section('content')
<?php // Hide only message with 'required field' because we need to display it in form // ?>
@if ($errors->any() && ! $is_email_requeried)
<div id="error-container">
    <div class="error"><i class="material-icons md-24">&#xE001;</i> {{ $errors->first() }}</div>
</div>
@endif

@if (session('status'))
<div id="message-container">
    <div class="message"><i class="material-icons md-24">&#xE876;</i>{{ session('status') }}</div>
</div>
@endif
<?php unset($is_email_requeried); ?>
*/ ?>


@section('content')

<div class="pseudo-modal">
<div class="container-fluid">
    <form id="password-form" method="POST" action="{{ url('password/email') }}">
    @include('auth/password_partial', ['mode' => 'standalone'])
    </form>
</div>
</div>

<script>
    $('#password-form').PasswordFormPlugin();
</script>
@endsection
