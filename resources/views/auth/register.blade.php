<!-- resources/views/auth/register.blade.php -->
@extends('front', ['body_background' => 'traveler-wide'])

@section('content')
<form method="POST" action="{{ url('auth/register') }}">

{!! csrf_field() !!}
<div class="pseudo-modal">
<div class="container-fluid">

    <div class="row logo">
        <div class="col-xs-12"><img src="{{ url('https://s3.amazonaws.com/resynct/widgets/resynct-logo.png') }}"></div>
    </div>

    <div class="row message">
        <div class="col-xs-12"><h3>Become a Member!</h3></div>
    </div>

<?php /*
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
*/ ?>


    <div class="row input">
        <div class="col-xs-12">
            @include('form.string', ['name' => 'full_name', 'label' => 'Full Name:'])
        </div>
    </div>
    <div class="row input">
        <div class="col-xs-12">
            @include('form.string', ['name' => 'email', 'label' => 'E-mail:'])
        </div>
    </div>
    <div class="row input">
        <div class="col-xs-12">
            @include('form.password', ['name' => 'password', 'label' => 'Password:'])
        </div>
    </div>
    <div class="row input">
        <div class="col-xs-12">
            @include('form.password', ['name' => 'password_confirmation', 'label' => 'Confirm password:'])
        </div>
    </div>

    <div class="row input">
        <div class="col-xs-12">
            <?=trans('front.By clicking "Sign Up" I agree to reSynct', [
                'privacy_statement'    => trans('link.privacy_statement')
            ])?>
        </div>
    </div>

    <div class="row submit">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-round btn-blue">Create my account</button>
        </div>
    </div>

    <div class="row footer">
        <div class="col-xs-12 text-center">
            <a href="{{ url('auth/login') . '?' . http_build_query($_GET) }}">Back to Login</a>
        </div>
    </div>


</div>
</div>
</form>



@endsection
