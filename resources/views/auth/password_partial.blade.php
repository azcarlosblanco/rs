<?php /*
<?php // Hide only message with 'required field' because we need to display it in form // ?>
@if ($errors->any() && ! $is_email_requeried)
<div id="error-container">
    <div class="error"><i class="material-icons md-24">&#xE001;</i> {{ $errors->first() }}</div>
</div>
@endif

@if (session('status'))
<div id="message-container">
    <div class="message"><i class="material-icons md-24">&#xE876;</i>{{ session('status') }}</div>
</div>
@endif



*/ ?>
<?php
// $is_email_requeried   = ($errors->has('email') && (stripos($errors->first('email'), 'required') !== false));
// unset($is_email_requeried);
?>




{!! csrf_field() !!}

<div class="row logo">
    <div class="col-xs-12"><img src="{{ url('https://s3.amazonaws.com/resynct/widgets/resynct-logo.png') }}"></div>
</div>

@if (session('status'))
    <div class="row footer">
        <div class="col-xs-12 text-center">
            <div id="success_top">
                {{ session('status') }}
            </div>

            <a href="{{ url('auth/login') . '?' . http_build_query($_GET) }}">Back to login page.</a>
        </div>
    </div>

@else

    <div class="row message">
        <div class="col-xs-12"><h3>Enter your e-mail address and we'll get you back on track.</h3></div>
    </div>

    <div class="row input">
        <div class="col-xs-12 form-group">
            @include('form.string', ['name' => 'email', 'label' => 'E-mail'])
        </div>
    </div>

    <div class="row submit">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-round btn-blue">Send Password Reset Link</button>
        </div>
    </div>

    <div class="row footer">
        <div class="col-xs-12 text-center">
        @if ($mode == 'modal')
            <a data-modal-show="modal" data-target="#modal-login">Back to Login</a>
        @else
            <a href="{{ url('auth/login') . '?' . http_build_query($_GET) }}">Back to Login</a>
        @endif
        </div>
    </div>
@endif
