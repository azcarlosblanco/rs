<?php
    $preview = 'long_preview';
    if ($selected) {
        $selected = [
            ['id' => $selected->id, 'preview' => $selected->{$preview}]
        ];
    } else {
        $selected = [];
    }

    if ( ! isset($id)) {
        $id = $name;
    }
?>
<div class="form-group @if ($errors->has($name)) has-error @endif ">
    {!! Form::label($name, $label) !!}
    {!! Form::text($name, null, ['class' => 'form-control', 'id' => $id, 'data-data' => htmlentities(json_encode($selected))]) !!}

    @if ($errors->has($name))
        <span class="help-block">{{ $errors->first($name) }}</span>
    @endif
</div>