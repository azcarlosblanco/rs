<?php
$class       = 'form-control ' . (isset($class) ? $class : '');
$placeholder = isset($placeholder) ? $placeholder : '';
$show_errors = isset($show_errors) ? $show_errors : true;
?>

<div class="form-group @if ($errors->has($name)) has-error @endif ">
    <?php if (isset($label)) : ?>
    {!! Form::label($name, $label) !!}
    <?php endif; ?>
    {!! Form::password($name, ['class' => $class, 'placeholder' => $placeholder]) !!}

    <span class="help-block">
    @if ($show_errors && $errors->has($name))
        {{ $errors->first($name) }}
    @endif
    </span>
</div>

<?php unset($class, $placeholder); ?>
