
<div class="form-group @if ($errors->has($name)) has-error @endif ">
    {!! Form::label($name, $label) !!}
    {!! Form::select($name, $options, null, ['class' => 'form-control']) !!}

    @if ($errors->has($name))
        <span class="help-block">{{ $errors->first($name) }}</span>
    @endif
</div>