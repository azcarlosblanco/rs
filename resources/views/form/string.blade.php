<?php
$class = 'form-control ' . (isset($class) ? $class : null);
$placeholder = isset($placeholder) ? $placeholder : null;
$show_errors = isset($show_errors) ? $show_errors : true;
$value       = isset($value)       ? $value       : null;
?>

<div class="form-group @if ($errors->has($name)) has-error @endif ">
    @if (isset($label))
    {!! Form::label($name, $label) !!}
    @endif

    {!! Form::text($name, $value, ['class' => $class, 'placeholder' => $placeholder]) !!}

    <span class="help-block">
    @if ($show_errors && $errors->has($name))
        {{ $errors->first($name) }}
    @endif
    </span>
</div>

<?php unset($class, $show_errors, $value, $placeholder); ?>
