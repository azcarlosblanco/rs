<?php
$class       = 'form-control custom' . (isset($class) ? $class : '');
$show_errors = isset($show_errors) ? $show_errors : true;
$label       = isset($label) ? $label : 'active';
$id          = isset($id) ? $id : md5($name . microtime());
?>

<div class="checkbox @if ($errors->has($name)) has-error @endif ">
    <input type="hidden" name="{{ $name }}" value="0" />
    {!! Form::checkbox($name, 1, null, ['class' => $class, 'id' => $id]) !!}

    <label class="checkbox" for="{{ $id }}">
    <?=$label?>
    </label>

    <span class="help-block">
    @if ($show_errors && $errors->has($name))
        {{ $errors->first($name) }}
    @endif
    </span>
</div>

<?php unset($class); ?>
