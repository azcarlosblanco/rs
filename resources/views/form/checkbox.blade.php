
<div class="checkbox @if ($errors->has($name)) has-error @endif " style="margin-top: 30px">
        <label class="checkbox">
            <input type="hidden" name="{{ $name }}" value="0" />
            {!! Form::checkbox($name, 1, ['class' => 'form-control']) !!}
            Active
        </label>
    @if ($errors->has($name))
        <span class="help-block">{{ $errors->first($name) }}</span>
    @endif
</div>

