
<h2>Reservations</h2>

    <form class="filter_form container pull-left" style="padding: 0">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('filter[fulltext]', 'Fulltext:') !!}
                    {!! Form::text('filter[fulltext]', array_get($filter, 'fulltext'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                {!! Form::submit('Search', ['class' => 'btn btn-primary form-control submit_filter']) !!}
            </div>
            <div class="col-md-2">
                {!! Form::submit('Reset filters', ['class' => 'btn btn-default form-control reset_filter']) !!}
            </div>
        </div>
    </form>
<div class="clearfix"></div>

@if (count($results))

    <div class="paginator pull-right"></div>

    <table class="table">
        <thead>
        <tr>
            <th data-sortable="status">Status</th>
            <th data-sortable="member.name">Customer Name</th>
            <th data-sortable="hotel.website_name">Hotel Name</th>
            <th data-sortable="check_in_date">Check-in</th>
            <th data-sortable="check_out_date">Check-out</th>
            <th data-sortable="total_amount">Total price</th>
            <th data-sortable="paid_amount">Paid price</th>
            <th data-sortable="points_spent">Points spent</th>
            <th data-sortable="points_earned">Points earned</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($results as $res)
            <tr>
                <td>{{ $res->getStatusPreview() }}</td>
                <td>{{ $res->member->getFullName() }}</td>
                <td>{{ $res->hotel->website_name }}</td>
                <td>{{ $res->check_in_date }}</td>
                <td>{{ $res->check_out_date }}</td>
                <td>{{ $res->total_amount }}</td>
                <td>{{ $res->paid_amount }}</td>
                <td>{{ $res->points_spent }}</td>
                <td>{{ $res->points_earned }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="paginator pull-right"></div>

@else

    <div class="alert alert-info" style="margin: 20px 0;">
        No results found.
    </div>

@endif




