<?php
 // We only decide what menu to display here
?>

@if (Auth::user())

    @if (Auth::user()->isMember())
        @include('partials.menu_member')
    @endif

@endif


