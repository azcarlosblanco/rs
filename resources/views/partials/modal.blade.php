<?php
// If ! isset($form_name) then $form_name = $form_id;
$form_name         = (isset($form_name)         && $form_name)         ? $form_name         : $form_id;
$form_autocomplete = (isset($form_autocomplete) && $form_autocomplete) ? $form_autocomplete : 'on';
$form_method       = (isset($form_method)       && $form_method)       ? $form_method       : 'POST';

// Show by default
$with_close_btn    = isset($with_close_btn) ? $with_close_btn : true;
?>
<form id="{{$form_id}}" name="{{$form_name}}" action="{{$form_action}}" method="{{$form_method}}" autocomplete="{{$form_autocomplete}}">
    <div id="{{$modal_id}}" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body container-fluid" data-close="true">
                    @if ($with_close_btn)
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                    @endif

                    @yield($section_name_body)
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog  -->
    </div><!-- .#{{$modal_id}} -->
</form><!-- #{{$form_id}} -->

<!-- #{{$modal_id}} javascript -->
@yield($section_name_js)
<!-- #{{$modal_id}} javascript -->
</div><!-- #{{$modal_id}} -->

<?php unset($form_name, $form_autocomplete, $form_method, $with_close_btn); ?>
