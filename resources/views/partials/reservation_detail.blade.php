
<div class="reservation-detail container-fluid">
    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">Hotel:</div>
        <div class="col-md-8 col-xs-8 col-sm-8 dialog-value">{!! $reservation->hotel->getWebsiteLink() !!}</div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">Reservation #:</div>
        <div class="col-md-8 col-xs-8 col-sm-8 dialog-value">{{ $reservation->code }}</div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">Room Code:</div>
        <div class="col-md-8 col-xs-8 col-sm-8 dialog-value">{{ $reservation->room_code }}</div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">Check-in:</div>
        <div class="col-md-8 col-xs-8 col-sm-8 dialog-value">{{ DateFormat::getUserDate($reservation->check_in_date) }}</div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">Check-out:</div>
        <div class="col-md-8 col-xs-8 col-sm-8 dialog-value">{{ DateFormat::getUserDate($reservation->check_out_date) }}</div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">Nights:</div>
        <div class="col-md-8 col-xs-8 col-sm-8 dialog-value">{{ $reservation->getNightsCount() }}</div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">Regular rate:</div>
        <div class="col-md-3 col-xs-3 col-sm-3 dialog-value dialog-number">{{ Format::usMoneyValue($reservation->total_amount) }}</div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">Discount:</div>
        <div class="col-md-3 col-xs-3 col-sm-3 dialog-value dialog-number">{{ Format::usMoneyValue($reservation->discount_amount) }}</div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">Your rate:</div>
        <div class="col-md-3 col-xs-3 col-sm-3 dialog-value dialog-number">{{ Format::usMoneyValue($reservation->paid_amount) }}</div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">rePoints redeemed:</div>
        <div class="col-md-3 col-xs-3 col-sm-3 dialog-value dialog-number">{!! Format::points(-$reservation->getRealSpentPoints(), $reservation->is_valid) !!} pts</div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">rePoints earned:</div>
        <div class="col-md-3 col-xs-3 col-sm-3 dialog-value dialog-number">{!! Format::points($reservation->getRealEarnedPoints(), $reservation->is_valid) !!} pts</div>
        <div class="col-md-5 col-xs-5 col-sm-5 dialog-value"><i class="material-icons md-18 cursor-pointer" data-toggle="tooltip" data-placement="top" title="{{ $reservation->getEarnedPointsTooltip() }}">info</i></div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">Booking date:</div>
        <div class="col-md-3 col-xs-3 col-sm-3 dialog-value">{{ DateFormat::getUserDate($reservation->created_at) }}</div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4 col-sm-4 dialog-label">Status:</div>
        <div class="col-md-3 col-xs-3 col-sm-3 dialog-value">{!! $reservation->getStatusPreview() !!}</div>
    </div>
</div>
