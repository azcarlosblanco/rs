<ul class="nav nav-pills" id="menu">
    <li @if(Route::current()->getName() == 'MemberBalance') class="active" @endif>
        <a href="{{ route('MemberBalance') }}"> Dashboard</a>
    </li>
    <li @if(Route::current()->getName() == 'MemberReservationTable') class="active" @endif>
        <a href="{{ route('MemberReservationTable') }}">My Reservations</a>
    </li>
    <li @if(Route::current()->getName() == 'MemberShowProfile') class="active" @endif>
        <a href="{{ route('MemberShowProfile') }}"> My Profile</a>
    </li>
    <li @if(Route::current()->getName() == 'GetContactForm') class="active" @endif>
        <a href="{{ route('GetContactForm') }}">Contact Us</a>
    </li>
</ul>
