<div id="profile">
    <div class="row no-gutter"><?php // .no-gutter remove padding in child (dirty hack) ?>
    <div class="avatar-container">
        <div class="avatar">
            <div class="image">
                <span id="avatar-change" class="glyphicon glyphicon-pencil"></span>
                @if ($avatar = Auth::user()->avatar)
                    <img src="{{$avatar->url()}}">
                @else
                    <img src="{{url('images/avatar-placeholder.png')}}">
                @endif
                <?php unset($avatar); ?>
            </div>
            <div class="fullname">{{ Auth::user()->getFullName() }}</div>
            <div class="points">You have <strong>{{ Auth::user()->current_points_amount }}</strong> rePoints</div>
            <div class="edit-profile">
                <a class="btn btn-darkblue btn-round" href="{{ route('MemberUpdateProfile') }}">Edit Profile</a>
            </div>
        </div>

        <div id="avatar-uploader" class="avatar-uploader slim hidden">
            <input type="file" name="slim[]"/>
        </div>
    </div>

    <div class="summary">
        <ul class="list-group">
            <li class="list-group-item">
                Total earned rePoints
                <div class="value">{{ Auth::user()->getTotalEarnedPoints() }} pts</div>
            </li>
            <li class="list-group-item">
                Total spent rePoints
                <div class="value">{{ Auth::user()->getTotalSpentPoints() }}  pts</div>
            </li>
            <li class="list-group-item">
                On hold (waiting hotel confirmation)
                <div class="value">{{ Auth::user()->getOnHoldPoints() }} pts</div>
            </li>
        </ul>
    </div>
    </div><?php // .no-gutter ?>
</div>


@section('footer_js_ready')
    // <script>
    $('#profile').MemberProfileAvatarUpdate({
        slimElement    : $('#profile #avatar-uploader.slim'),
        avatarUrlGet   : "{{route('GetMemberAvatar')}}",
        avatarUrlPost  : "{{route('PostMemberAvatar')}}?_token={{ csrf_token() }}",
    });
@parent
@endsection
