@extends('back')

@section('content')

    <h2>Hotel detail - {{ $hotel->real_name }}</h2>

    @include('hotel.overview_header')

    {!! Form::model($hotel, ['method' => 'PUT', 'url' => route('hotels.update', ['hotels' => $hotel->id])]) !!}

        <div class="row">
            <div class="col-md-3">
                @include('form.string', ['name' => 'real_name', 'label' => 'Hotel Legal Name:'])
            </div>
            <div class="col-md-3">
                @include('form.string', ['name' => 'website_name', 'label' => 'Hotel Public Name:'])
            </div>
            <div class="col-md-3">
                @include('form.checkbox', ['name' => 'active', 'label' => 'Active'])
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                @include('form.string', ['name' => 'email', 'label' => 'E-mail:'])
            </div>
            <div class="col-md-3">
                @include('form.string', ['name' => 'phone', 'label' => 'Phone:'])
            </div>
            <div class="col-md-6">
                @include('form.string', ['name' => 'website', 'label' => 'Website:'])
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                @include('form.select', ['name' => 'cb_country_id', 'options' => $countries, 'label' => 'Country:'])
            </div>
            <div class="col-md-3">
                @include('form.string', ['name' => 'state', 'label' => 'State:'])
            </div>
            <div class="col-md-3">
                @include('form.string', ['name' => 'city', 'label' => 'City:'])
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                @include('form.string', ['name' => 'zip', 'label' => 'ZIP:'])
            </div>
            <div class="col-md-3">
                @include('form.string', ['name' => 'address', 'label' => 'Address:'])
            </div>
            <div class="col-md-3">
                @include('form.string', ['name' => 'neighborhood', 'label' => 'Neighborhood:'])
            </div>
        </div>


<div class="row">
    <div class="col-md-3">
        <br>
        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>


    {!! Form::close() !!}

@endsection