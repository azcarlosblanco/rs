@extends('back')



@section('content')

    <h2>Hotel detail - {{ $hotel->real_name }}</h2>

    @include('hotel.overview_header')


    <h4>Put this code into the {{ '<head>' }} section on every page:</h4>
    <pre>{{'<link rel="stylesheet" type="text/css" href="'.$css_url.'">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="'.$js_url.'"></script>
<script type="text/javascript">
    window.onload = function() { RS.resynctRenderLoginWidget(\'rs-login-widget\') };
</script>'
    }}</pre>

    <h4>Put this placeholder to the place where you want the button to be displayed:</h4>
    <pre>{{ '<div id="rs-login-widget"></div>' }}</pre>

@endsection