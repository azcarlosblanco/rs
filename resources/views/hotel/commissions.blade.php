@extends('back')

@section('content')

    <h2>Hotel detail - {{ $hotel->real_name }}</h2>

    @include('hotel.overview_header')

    {!! Form::model($hotel) !!}

        <div class="container pull-left">
            <div class="row">
                <div class="col-md-3">
                    @include('form.string', ['name' => 'commission_relative', 'label' => 'Relative commission [%]:'])
                </div>
                <div class="col-md-3">
                    @include('form.string', ['name' => 'commission_flat', 'label' => 'Flat commission [$]:'])
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <br>
                    {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
                </div>
            </div>
        </div>


            <div class="clearfix"></div>

    {!! Form::close() !!}

@endsection