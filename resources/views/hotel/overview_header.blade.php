<ul class="nav nav-pills" style="margin-bottom: 10px;">
    <li @if(Route::current()->getName() == 'hotels.edit') class="active" @endif>
        <a href="{{ route('hotels.edit', ['hotels' => Route::current()->getParameter('hotels')]) }}">Basic information</a>
    </li>
    <li @if(Route::current()->getName() == 'hotels.commissions') class="active" @endif>
        <a href="{{ route('hotels.commissions', ['hotels' => Route::current()->getParameter('hotels')]) }}">Commissions</a>
    </li>
    <li @if(Route::current()->getName() == 'hotels.images') class="active" @endif>
        <a href="{{ route('hotels.images', ['hotels' => Route::current()->getParameter('hotels')]) }}">Images</a>
    </li>
    <li @if(Route::current()->getName() == 'hotels.widget') class="active" @endif>
        <a href="{{ route('hotels.widget', ['hotels' => Route::current()->getParameter('hotels')]) }}">Widget Code</a>
    </li>
</ul>