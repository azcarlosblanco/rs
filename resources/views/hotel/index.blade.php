
<h2>Hotels</h2>


    <form class="filter_form container pull-left" style="padding: 0">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('filter[cb_country_id]', 'Country:') !!}
                    {!! Form::select('filter[cb_country_id]', $countries, array_get($filter, 'cb_country_id'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('filter[fulltext]', 'Fulltext:') !!}
                    {!! Form::text('filter[fulltext]', array_get($filter, 'fulltext'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                {!! Form::submit('Search', ['class' => 'btn btn-primary form-control submit_filter']) !!}
            </div>
            <div class="col-md-2">
                {!! Form::submit('Reset filters', ['class' => 'btn btn-default form-control reset_filter']) !!}
            </div>
        </div>
    </form>
<div class="clearfix"></div>


@if (count($results))

    <div class="paginator pull-right"></div>

    <table class="table">
        <thead>
        <tr>
            <th data-sortable="status">Status</th>
            <th data-sortable="name">Hotel Name</th>
            <th data-sortable="email">E-mail</th>
            <th data-sortable="city">City</th>
            <th data-sortable="state">State</th>
            <th data-sortable="country.value">Country</th>
            <th data-sortable="current_points_amount">Point balance</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($results as $hotel)
            <tr>
                <td>{{ $hotel->active ? 'Active' : 'Inactive' }}</td>
                <td>{{ $hotel->real_name }}</td>
                <td>{{ $hotel->email }}</td>
                <td>{{ $hotel->city }}</td>
                <td>{{ $hotel->state }}</td>
                <td>{{ $hotel->country->value }}</td>
                <td>{{ $hotel->current_points_amount }}</td>
                <td><a href="{{ route('hotels.edit', ['hotels' => $hotel->id]) }}">Detail</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="paginator pull-right"></div>

@else

    <div class="alert alert-info" style="margin: 20px 0;">
        No results found.
    </div>

@endif




