@extends('back')


@section('head')
    <script type="text/javascript" src="{{ url('js/dropzone.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ url('css/dropzone.css') }}">
@endsection


@section('content')

    <h2>Hotel detail - {{ $hotel->real_name }}</h2>

    @include('hotel.overview_header')

    {!! Form::open(['class' => 'dropzone', 'id' => 'my_awesome_dropzone', 'url' => route('hotels.upload_images', ['hotels' => $hotel->id])]) !!}

    {!! Form::close() !!}


    <script type="text/javascript">
        $(document).ready(function(){
            Dropzone.options.myAwesomeDropzone = {
                parallelUploads: 20,
                addRemoveLinks: true,
                init: function() {
                    var files = {!! json_encode($images) !!};
                    var _this = this;
                    $.each(files, function(key, file){
                        _this.emit("addedfile", file);
                        _this.emit("thumbnail", file, file.url);
                        _this.emit('complete', file);
                    });

                    _this.on('success', function(file, response){
                        console.log('success triggered');
                        console.log(file);
                        console.log(response);
                        file.id = response.id;

                        return file;
                    });

                    _this.on('removedfile', function(file) {
                        $.ajax({
                            url: '{{ route('hotels.remove_image', ['hotels' => $hotel->id]) }}',
                            method: 'post',
                            data: {
                                file_id: file.id
                            },
                            success: function(){
                                console.log('file.removed');
                            }
                        })
                    });
                }

            }
        });
    </script>

@endsection