<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>reSynct</title>

    <script type="text/javascript" src="{{ elixir_uri('js/all.js') }}"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

    <link rel="stylesheet" href="{{ elixir_uri('css/back_merged.css') }}">

    <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = '2d4ac2b2bab15142e083d763ec557dae5088f227';
        window.smartsupp||(function(d) {
            var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
            s=d.getElementsByTagName('script')[0];c=d.createElement('script');
            c.type='text/javascript';c.charset='utf-8';c.async=true;
            c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
    </script>

    @yield('head')

</head>

<?php $hide_profile = ! ((Route::current()->getName() == 'MemberShowProfile') || (Route::current()->getName() == 'MemberUpdateProfile')); ?>
<body data-role="{{(Auth::guest() ? 'guest' : 'member')}}" data-hide-profile="{{($hide_profile ? 'true' : 'false')}}">
<?php unset($hide_profile); ?>

@if ($msg = Session::get('flash_msg'))
    <div class="flash_msg"><div>{!! $msg !!}</div></div>
@endif

<?php // #top-bar ?>
<div id="top-bar">
<div class="container-fluid">
    <div class="row">
        <div class="left col-xs-12 col-sm-8">
            <div class="logo">
            @if (Auth::guest())
                <a href="{{ url('/') }}"><img src="{{ url('https://s3.amazonaws.com/resynct/widgets/resynct-logo.png') }}" width="112"></a>
            @else
                <a href="{{route('MemberShowProfile')}}"><img src="{{ url('https://s3.amazonaws.com/resynct/widgets/resynct-logo.png') }}" width="112"></a>
            @endif
            </div>

            <?php // menu in the middle ?>
            @if ( ! Auth::guest())
                <div class="menu">
                    <ul class="list-inline">
                        <li @if(Route::current()->getName() == 'MemberShowProfile') class="active" @endif>
                            <a href="{{ route('MemberShowProfile') }}"> My Profile</a>
                        </li>
                        <li @if(Route::current()->getName() == 'MemberReservationTable') class="active" @endif>
                            <a href="{{ route('MemberReservationTable') }}">Reservation History</a>
                        </li>
                    </ul>
                </div>
            @endif
        </div>

        <?php // log in/out block ?>
        <div class="loginout-block col-xs-12 col-sm-4 nowrap">
            <ul class="list-inline">
            @if (Auth::guest())
                <li><a data-modal-toggle="modal" data-target="#modal-login">Log In</a></li>
        <li><a data-modal-toggle="modal" data-target="#modal-password">Forgot password?</a></li>
        <li><a data-modal-toggle="modal" data-target="#modal-reset">Reset Password?</a></li>
                <li><a href="{{ url('/auth/login') }}">Sign In</a></li>
                <li><a href="{{ url('/auth/register') }}">Become a member</a></li>
            @else
                <li><a class="logout" href="{{ url('/auth/logout') }}">Logout <img src="{{ url('images/icon-logout.png') }}"></a></li>
            @endif
            </ul>
        </div>
    </div>
</div>
</div>
<?php // #top-bar ?>



<div id="page-content" class="container-fluid">
<div class="row">
    @if (Auth::guest())
        <div id="content-container" class="col-xs-12">
            @yield('content')
        </div>
    @else
        <div id="profile-container">
            @include('partials/profile')
        </div>

        <?php // Content of subpage // ?>
        <div id="content-container">

            <?php if ( ! Auth::getUser()->confirmed): ?>
                <div class="alert alert-danger">
                    You can not redeem your RePoints yet because your e-mail address is not confirmed. Please follow the instructions in the confirmation e-mail which was sent to {{ Auth::getUser()->email }} on {{ DateFormat::getUserDate(Auth::getUser()->created_at) }}.
                </div>
            <?php endif; ?>

            @yield('content')
        </div>
    @endif
</div>
</div>



<!-- #footer -->
<div id="footer" class="hidden-print">
    <div class="social">
        <ul class="list-inline">
            <li class="facebook"><a  href="{{trans('link.facebook')}}" target="_blank"><span></span></a></li>
            <li class="instagram"><a href="{{trans('link.instagram')}}" target="_blank"><span></span></a></li>
            <li class="linkedin"><a  href="{{trans('link.linkedin')}}" target="_blank"><span></span></a></li>
        </ul>
    </div>
    <div class="copyright clearfix">© {{date('Y')}} Resynct. All Rights Reserved</div>
</div>
<!-- #footer -->



    <div class="modal fade" id="genericModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body" style="padding: 60px 10px;">

                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="pull-left btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


<script type="text/javascript">
    // @todo - move to external file
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    setTimeout(function(){
        $('.flash_msg').remove();
    }, 10000);


    @yield('footer_js')

    $(document).ready(function(){
        @yield('footer_js_ready')
    });
</script>

@yield('footer')

</body>
</html>
