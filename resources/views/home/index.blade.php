@extends('front', ['without_modals' => true, 'without_header' => true, 'without_footer' => true])


@section('content')
<div id="wrapper-choose-one">

<div class="member">
    <div class="choose-one">
        <div class="header"><h1>For <span class="hightlight">Members</span></h1></div>
        <div class="message">Become a member of the fastest growing loyalty network in the world, tailored to your unique travel preferences.</div>
        <div class="explore-btn"><a href="{{route('home.getMember')}}" class="btn btn-round">Explore <i class="material-icons md-18">&#xE8E4;</i></a></div>
    </div>
</div>
<div class="hotel">
    <div class="choose-one">
        <div class="header"><h1>For <span class="hightlight">Hotels</span></h1></div>
        <div class="message">This is your loyalty program, managed your <br> way for today's guest.</div>
        <div class="explore-btn"><a href="{{url('/hotel')}}" class="btn btn-round">Explore <i class="material-icons md-18">&#xE8E4;</i></a></div>
    </div>
</div>

</div>
@endsection
