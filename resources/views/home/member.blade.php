@extends('front')


@section('content')

<div class="get_member">
    <div id="sign-up" class="sign-up slide1">
        <div class="container">
            <div class="row">
                <div class="title {{((bool)Auth::user() ? 'centered' : '')}}"><h1>reSynct is a network of hotels that reward guests with points for staying at their unique properties.</h1></div>

                @if ( ! (bool)Auth::user())
                    <div class="message">Sign Up For Free!</div>
                    @include('home/sign_up_form')
                @endif
            </div>
        </div>
    </div>

    <div id="about" class="about slide2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 message-container">
                    <div class="title line-bottom-left">About reSynct</div>
                    <div class="message">
                      reSynct is the first guest loyalty program actually designed with the right person in mind…you!  We have developed not just another generic points program, but rather a highly adaptive system that learns from your engagement and customizes the rewards earned to your specific tastes.  Simply sign up through our site or your favorite independent or boutique hotel and instantly receive 500 rePoints for registration.  These points can be instantly redeemed at any one of our partner hotels worldwide with no blackout dates!  Easy to use and totally free, reSynct is the last loyalty program you’ll ever want, so sign up today!
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- #how-it-works -->
    <div id="how-it-works" class="how-it-works slide3">
    </div>
    <!-- #how-it-works -->

    <!-- #benefits -->
    <div id="benefits" class="benefits slide4" data-target="#benefits-carousel" role="button" data-slide="next">
        <div class="container">
            <div id="benefits-carousel" class="carousel slide" data-ride="carousel">

                <div class="title line-bottom-center"><h1>Benefits</h1></div>

                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#benefits-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#benefits-carousel" data-slide-to="1"></li>
                    <li data-target="#benefits-carousel" data-slide-to="2"></li>
                    <li data-target="#benefits-carousel" data-slide-to="3"></li>
                    <li data-target="#benefits-carousel" data-slide-to="4"></li>
                    <li data-target="#benefits-carousel" data-slide-to="5"></li>
                </ol>


                <?php
                $carousel_images = [
                  'slide1' => asset('images/front/get_member/benefits/1.png'),
                  'slide2' => asset('images/front/get_member/benefits/2.png'),
                  'slide3' => asset('images/front/get_member/benefits/3.png'),
                  'slide4' => asset('images/front/get_member/benefits/4.png'),
                  'slide5' => asset('images/front/get_member/benefits/5.png'),
                  'slide6' => asset('images/front/get_member/benefits/6.png'),
                ];?>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="text"><div class="message">Freedom of choice. reSynct members Are not bound to any 1 brand.</div></div>
                        <div class="image"><img src="{{$carousel_images['slide1']}}"></div>
                    </div>

                    <div class="item">
                        <div class="text"><div class="message">Personalized incentives and perks.</div></div>
                        <div class="image"><img src="{{$carousel_images['slide2']}}"></div>
                    </div>

                    <div class="item">
                        <div class="text"><div class="message">RePoints guarantee the best deal, always.</div></div>
                        <div class="image"><img src="{{$carousel_images['slide3']}}"></div>
                    </div>

                    <div class="item">
                        <div class="text"><div class="message">Top notch service as a preferred reSynct guest.</div></div>
                        <div class="image"><img src="{{$carousel_images['slide4']}}"></div>
                    </div>

                    <div class="item">
                        <div class="text"><div class="message">RePoints are flexible & can be redeemed when you need them.</div></div>
                        <div class="image"><img src="{{$carousel_images['slide5']}}"></div>
                    </div>

                    <div class="item">
                        <div class="text"><div class="message">Save $$. The more you stay the more RePoints you earn.</div></div>
                        <div class="image"><img src="{{$carousel_images['slide6']}}"></div>
                    </div>
                </div>
                <?php unset($carousel_images); ?>
            </div><!-- carousel -->

        </div><!-- .container -->
    </div>
    <!-- #benefits -->


    @if ( ! (bool)Auth::user())
    <!-- #earn-now -->
    <div id="earn-now" class="earn-now slide5">
        <div class="container">
            <div class="row">
                <div class="title line-bottom-center"><h1>Start Earning Now!</h1></div>
                <div class="message">Sign Up For Free!</div>

                @include('home/sign_up_form')

            </div>
        </div>
    </div>
    <!-- #earn-now -->
    @endif
</div>
@endsection
