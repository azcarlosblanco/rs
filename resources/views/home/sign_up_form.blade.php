<div class="form-sign-up container-fluid">
    <form method="POST" action="{{ url('auth/register') }}">
        {!! csrf_field() !!}

        <div class="row">
            <div class="col-xs-12 col-sm-6 text-left">@include('form.string', ['name' => 'full_name', 'label' => 'Full Name'])</div>
            <div class="col-xs-12 col-sm-6 text-left">@include('form.string', ['name' => 'email', 'label' => 'E-mail'])</div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 text-left">@include('form.password', ['name' => 'password', 'label' => 'Password'])</div>
            <div class="col-xs-12 col-sm-6 text-left">@include('form.password', ['name' => 'password_confirmation', 'label' => 'Confirm password'])</div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-9 terms terms-top">
                <?=trans('front.By clicking "Sign Up" I agree to reSynct', [
                    'privacy_statement'    => trans('link.privacy_statement')
                ])?>
            </div>

            <div class="col-xs-12 col-sm-3 submit">
                <button type="submit" class="btn btn-round btn-blue">Sign Up</button>
            </div>

            <div class="col-xs-12 col-sm-9 terms terms-bottom">
                <?=trans('front.By clicking "Sign Up" I agree to reSynct', [
                    'privacy_statement'    => trans('link.privacy_statement')
                ])?>
            </div>
        </div>
    </form>
</div><!-- div.form-sign-up.container-fluid -->
