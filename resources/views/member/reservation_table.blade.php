@extends('back')

@section('content')


<div id="member-reservation">

    @if ( ! $upcoming_reservations->count() and ! $other_reservations->count())
    <div class="row">
        <div class="alert alert-info">
            You have no reservation history yet.
        </div>
    </div>
    @endif


    <div class="row">

    @if ($upcoming_reservations->count())
    <h4>Upcoming Reservations</h4>
    <table id="upcoming-reservations" class="table table-striped">
        <thead id="table-thead-upcoming-reservations">
            <tr>
                <th>#</th>
                <th>Hotel</th>
                <th>Check-out</th>
                <th style="width: 100px">rePoints redeemed</th>
                <th style="width: 100px">rePoints earned</th>
                <th class="text-center">Status</th>
                <th>&nbsp;</th>
            </tr>
        </thead>

        <tbody id="table-tbody-upcoming-reservations">
        @foreach ($upcoming_reservations as $res)
            <tr>
                <td>{{ $res->code }}</td>
                <td>{!! $res->hotel->getWebsiteLink() !!}</td>
                <td>{{ DateFormat::getUserDate($res->check_out_date) }}</td>
                <td class="number">{!! Format::points(-$res->getRealSpentPoints()) !!}</td>
                <td class="number">{!! Format::points($res->getRealEarnedPoints()) !!}</td>
                <td class="text-center">{!! $res->getStatusPreview() !!}</td>
                <td class="text-center"><a href="{{ url('/member/reservations', [$res->id]) }}" class="btn btn-xs btn-primary popupform" title="Reservation details">Details</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif



    @if ($other_reservations->count())

    @if ($upcoming_reservations->count())<br><br>@endif
    <h4>Past Reservations</h4>

    <table id="past-reservations" class="table table-striped">
        <thead id="table-thead-past-reservations">
            <tr>
                <th>#</th>
                <th>Hotel</th>
                <th>Check-out</th>
                <th style="width: 100px">rePoints redeemed</th>
                <th style="width: 100px">rePoints earned</th>
                <th class="text-center">Status</th>
                <th>&nbsp;</th>
            </tr>
        </thead>

        <tbody id="table-tbody-past-reservations">
        @foreach ($other_reservations as $res)
            <tr>
                <td>{{ $res->code }}</td>
                <td>{!! $res->hotel->getWebsiteLink() !!}</td>
                <td>{{ DateFormat::getUserDate($res->check_out_date) }}</td>
                <td class="number">{!! Format::points(-$res->getRealSpentPoints(), $res->is_valid) !!}</td>
                <td class="number">{!! Format::points($res->getRealEarnedPoints(), $res->is_valid) !!}</td>
                <td class="text-center">{!! $res->getStatusPreview() !!}</td>
                <td class="text-center"><a href="{{ url('/member/reservations', [$res->id]) }}" class="btn btn-xs btn-primary popupform" title="Reservation details">Details</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

    </div><?php // .row ?>

</div><?php // #member-reservation ?>
@endsection



@section('footer_js_ready')
    // <script>
    $('body').jumpTop();
@parent
@endsection
