@extends('back')
@section('content')

    <h2>Dashboard</h2>

    <h3>Your account balance</h3>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-4">

                <div class="panel panel-success">
                    <div class="panel-heading text-center">Available points</div>
                    <div class="panel-body text-center">
                        <span style="font-size: 200%">XXX</span>
                    </div>
                </div>

            </div>
            <div class="col-md-6 col-lg-4">

                <div class="panel panel-info">
                    <div class="panel-heading">Statistics</div>
                    <div class="panel-body">

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-8">Total earned points:</div>
                                <div class="col-md-4"><?= $member->total_earned_points ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">Total spent points:</div>
                                <div class="col-md-4"><?= $member->total_spent_points ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">On hold points:</div>
                                <div class="col-md-4"><?= $member->getOnHoldPoints() ?></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection