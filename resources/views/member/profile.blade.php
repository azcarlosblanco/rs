@extends('back')
@section('content')

    <div class="row">
        <div class="col-xs-12 col-sm-offset-3 col-sm-6 col-lg-offset-0 col-lg-6">
            <h2>Your profile</h2>

            {!! Form::model($member, ['name' => 'member_profile', 'method' => 'PUT', 'url' => route('MemberUpdateProfile', [$member->id]), 'files' => true]) !!}
            <div class="row">
                <div class="col-xs-12">
                    @include('form.string', ['name' => 'firstname', 'label' => 'Name:'])
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    @include('form.string', ['name' => 'lastname', 'label' => 'Surname:'])
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    @include('form.hidden', ['name' => 'country_code',         'value' => ($member->country ? $member->country->code : '')])
                    @include('form.string', ['name' => 'country_autocomplete', 'value' => ($member->country ? $member->country->value : ''), 'label' => 'Country:'])
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        {!! Form::label('email', 'E-mail:') !!}
                        {!! Form::text('email', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    @include('form.string', ['name' => 'phone', 'label' => 'Phone:'])
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <br>
                    {!! Form::submit('Save changes', ['class' => 'btn btn-primary form-control']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection


@section('footer_js_ready')
    // <script>
    $('#content-container').MemberProfile({countryAutocompleteUrl: '{{route("AutocompleteCountry")}}'});
@parent
@endsection
