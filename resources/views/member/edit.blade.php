@extends('back')
@section('content')

    <h2>Edit member</h2>

    {!! Form::model($member, ['method' => 'PUT', 'url' => route('members.update', [$member->id]), 'files' => true]) !!}

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    @include('form.string', ['name' => 'firstname', 'label' => 'Name:'])
                </div>
                <div class="col-md-6">
                    @include('form.string', ['name' => 'lastname', 'label' => 'Surname:'])
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('email', 'E-mail:') !!}
                        {!! Form::text('email', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    @include('form.string', ['name' => 'phone', 'label' => 'Phone:'])
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            {!! Form::submit('Save changes', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>

    {!! Form::close() !!}


@endsection