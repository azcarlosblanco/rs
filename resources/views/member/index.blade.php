
<h2>Members</h2>

    <form class="filter_form container pull-left" style="padding: 0">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('filter[fulltext]', 'Fulltext:') !!}
                    {!! Form::text('filter[fulltext]', array_get($filter, 'fulltext'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                {!! Form::submit('Search', ['class' => 'btn btn-primary form-control submit_filter']) !!}
            </div>
            <div class="col-md-2">
                {!! Form::submit('Reset filters', ['class' => 'btn btn-default form-control reset_filter']) !!}
            </div>
        </div>
    </form>
<div class="clearfix"></div>


@if (count($results))

    <div class="paginator pull-right"></div>

    <table class="table">
        <thead>
        <tr>
            <th>Confirmed</th>
            <th>Name</th>
            <th data-sortable="email">E-mail</th>
            <th data-sortable="current_points_amount">Point balance</th>
            <th data-sortable="created_at">Registration date</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($results as $member)
            <tr>
                <td>{{ $member->user->confirmed ? 'Yes' : 'No' }}</td>
                <td>{{ $member->getFullName() }}</td>
                <td>{{ $member->email }}</td>
                <td>{{ $member->current_points_amount }}</td>
                <td>{{ $member->created_at }}</td>
                <td>
                    <a href="{{ route('members.edit', ['members' => $member->id]) }}">Profile</a>&nbsp; &nbsp;
                    <a href="{{ route('users.index', ['filter[member_id]' => $member->id]) }}">User accounts</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="paginator pull-right"></div>

@else

    <div class="alert alert-info" style="margin: 20px 0;">
        No results found.
    </div>

@endif




