<h1 class="title line-bottom-center">reSynct® Privacy Policy</h1>

<div class="text-center">Below Privacy Policy updated effective April 10, 2016</div>
<br><br><br>

<p>reSynct  ("reSynct", "we" or "us") owns and operates the reSynct RePoints Hotel Program® ("reSynct"). We are dedicated to protecting the privacy of our program members ("Members") and of visitors to our website located at www.reSynct.com (the "reSynct Site"). This privacy policy ("Policy") explains our policies and practices regarding the personal information we collect (1) on the reSynct Site, and (2) in connection with our Members' participation in the reSynct program, including the types of information collected and how it is used and shared.</p>



<p><img src="{{trans('image.truste_certified_privacy')}}">

<p>The TRUSTe program covers only information that is collected through this website, www.reSynct.com, and does not cover information that may be collected through  www.reSynct.com , and software downloaded from the website.</p>

<p>In order to view our relationship with TRUSTe please visit the validation page visible by clicking on the TRUSTe seal. If you have an unresolved privacy or data use concern that we have not addressed satisfactorily, please contact TRUSTe at <a href="https://feedback-form.truste.com/watchdog/request" target="_blank">https://feedback-form.truste.com/watchdog/request</a></p>

<p>RevPAR Collective complies with the U.S. – E.U. Safe Harbor framework and the U.S. – Swiss Safe Harbor framework as set forth by the U.S. Department of Commerce regarding the collection, use and retention of personal data from European Union member countries and Switzerland. RevPAR Collective has certified that it adheres to the Safe Harbor Privacy Principles of notice, choice, onward transfer, security, data integrity, access and enforcement. To learn more about the Safe Harbor program, and to view reSynct’s certification, please visit <a href="http://www.export.gov/safeharbor/" target="_blank">http://www.export.gov/safeharbor/</a></p>

<h1>Collection of Information</h1>

<h4>reSynct Site Visitors</h4>

<p>We collect information from visitors to the reSynct Site ("Visitors"), both Members and non-Members, when they voluntarily provide it to us on the reSynct Site. For example, we may request your name, street address, phone number and/or email address when you request information relating to RPC, a Participating Hotel, the reSynct Site or reSynct program, or when you respond to one of our online surveys.</p>

<p>If you send us an email from the reSynct Site, we may retain your email (including the email address from which it was sent), our response(s) and any follow up communications that you send. This information may be used to assess the extent to which your questions or concerns were addressed and to improve our reSynct program and reSynct Site.</p>

<p>From time to time we may offer an "invite friends" feature on the reSynct Site that allows you to send an electronic message to a friend. If you choose to use this feature, we will ask you for your email address, the recipient's name and email address, and the text of any message you choose to include. You can import contacts from your Outlook or other email account address book to invite them to become members of our website. We collect the username and password for the email account from which you wish to import your contacts, and will only use your personal information for that purpose.  By using the invite friends feature , you represent and warrant to us that you are entitled to provide us with the recipient's name and email address for this purpose. We will only use recipient names and email addresses to send the one time invitation emails. We will not send any further communications to recipients unless and until they enroll in the reSynct program and agree to the terms and conditions of membership, including this Policy. We store this information for the sole purpose of sending this one-time email and tracking the success of our referral program. We may receive personal information about you (e.g., your name and email address) from other Members as part of the invite friends feature. We may allow the referring Member to check the status of your invitation and will notify that Member when and if you join the program and when you have your first stay at a Participating Hotel.</p>

<p>Your friend may contact us at support to request that we remove this information from our database.</p>

<p>Technologies such as: cookies, beacons, tags and scripts are used by reSynct and our marketing partners, affiliates, and analytics providers. These technologies are used in analyzing trends, administering the site, tracking users’ movements around the site and to gather demographic information about our user base as a whole. We may receive reports based on the use of these technologies by these companies on an individual as well as aggregated basis.</p>

<p>We use cookies for authentication, to remember users’ settings, to keep track of user activity during a visit to our site, to conduct usability tests, to improve site performance, and to implement certain features of our site. Users can control the use of cookies at the individual browser level. If you reject cookies, you may still use our site, but your ability to use some features or areas of our site may be limited.</p>

<p>As is true of most web sites, we gather certain information automatically and store it in log files.  This information may include internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and/or clickstream data.</p>

<p>We may combine this automatically collected log information with other information we collect about you. We do this to improve services we offer you and to improve marketing, analytics, and site functionality. The collected information is not used for any other purpose.</p>

<p>We use Local Storage, such as HTML5, to store content information and preferences. Third Parties with whom we partner to provide certain features on our site use LSOs such as Flash and HTML5 to collect and store information.  To manage Flash LSOs please click here.</p>

<p>We partner with a third party to either display advertising on our website or to manage our advertising on other sites. Our third party partner may use technologies such as cookies to gather information about your activities on this website and other sites in order to provide you advertising based upon your browsing activities and interests. If you wish to not have this information used for the purpose of serving you interest-based ads, you may opt-out by clicking here (or if located in the European Union click here). Please note this does not opt you out of being served ads. You will continue to receive generic ads.</p>

<h4>reSynct Members</h4>

<p>We collect certain information from Members when they enroll in the reSynct program. Members are required to provide their name, address and email address and may also provide their phone number. The email address you provide at the time of enrollment will serve as your Member ID. In addition, you will create a password at the time of enrollment which you will need to access your Member account ("Account") information and preferences.</p>

<p>We maintain personal profiles for our reSynct Members containing hotel stay preference and other information that they voluntarily provide to us. Examples of the types of voluntary information we may seek in the Member profile include (i) high vs. low floor, smoking vs. non-smoking room, beverage and pillow type preferences, and (ii) whether the Member participates in other travel rewards programs. Creating and completing a member profile allows you to make reservations and redeem reSynct Points more easily and also allows us and our partner hotels to enhance your membership and hotel stay experiences. We may share a Member's preference profile with a Participating Hotel when the Member books a stay at that Participating Hotel through a reSynct Site and/or when we otherwise learn of a Member's upcoming stay at a Participating Hotel.</p>

<p>We may also collect non-personal information relating to Members, such as aggregated Participating Hotel stay data, responses to surveys and/or promotional offers, communication preferences, travel patterns (e.g., frequency of travel, frequent destinations, etc) or use of the reSynct Sites.</p>

<p>Following a Member's check-out from a Participating Hotel, the Participating Hotel will provide us with information relating to the stay, including the Member's email address, check in and check out date, reservation number, room type, rate code and folio amount. This stay information is used to credit the appropriate number of reSynct Points (if any) to the Member's Account and may also be used to identify and send targeted promotional offers to the Member. We may also extrapolate certain information from such stay data, such as a Member's frequency of travel and/or frequency of stays in partner hotels, and share such information with a Participating Hotel when we learn of a Member's upcoming stay at that Participating Hotel.</p>

<h4>Children</h4>

<p>The reSynct Site is intended for users over the age of majority. We do not target or direct the reSynct Site towards children under the age of 13, and children under 13 should not provide personal information to the reSynct Site.</p>

<h1>Use of Collected Information</h1>

<h4>reSynct Site Visitors</h4>

<p>We will use personal information provided by Visitors to the reSynct Site for the purpose it was provided and as otherwise disclosed to the Visitor at the time the information is provided.</p>

<p>We may share non-personal information, such as aggregate user statistics, demographic information, and reSynct Site usage information with third parties.  We may use our third-party service providers and contractors to perform certain services on our behalf, such as processing, storing, maintaining and transmitting data, processing and web analytics, and data analysis. With respect reSynct Site analysis, we may use Google Analytics, or another analytics provider, to record and process information as to how your browser navigates the reSynct Site, what keywords drove traffic to the reSynct Site, to help us count visitors and evaluate the reSynct Site’s technical capacity. For those analytics providers that use cookies or Web beacons, any opt-out functionality is controlled by them, and subject to their opt-out policies and practices. For information on how Google Analytics collects and processes data visit the link to the site “How Google uses data when you use our partners' sites or apps”, (located <a href="http://www.google.com/policies/privacy/partners" target="_blank">here</a>, or any other URL Google may provide from time to time).</p>

<h4>reSynct Members</h4>

<p>We may use Members' personal information: (a) to provide services, such as processing a redemption transaction for a stay at a Participating Hotel, (b) to improve our services and better understand your needs and requests, for example, by analyzing Member travel and hotel stay activities and patterns, (c) to send Members electronic communications from us, including monthly Account balance statements, other notices regarding your Account and Member surveys, (d) to send Members offers and other promotional communications, which may include promotions or offers from partner hotels, provided that the Member has not opted out of receiving such promotional communications, or (e) to establish and maintain Member accounts and other business records and comply with accounting requirements.</p><p>

<h4>Sharing of Personal Information</h4>

<p>We will share your personal information with third parties only in the ways that are described in this privacy policy. If you do not want us to share your personal information with these companies, you may delete your membership account via the Delete Account section in the My Account section of the website.</p>

<p>We may share Members' personal information with our partner hotels. For example, we may share a Member's personal preference profile and point balance with a Participating Hotel when we are aware that the Member has booked a stay at that hotel. We will also match a Member's Member ID against guest stay information provided to us by the Partner Hotels to ensure that reSynct Points are credited to the Member's account in connection with Eligible Stays. We will also provide Members' personal information to partner hotels in connection with hotel transactions they book through the reSynct Site. Information that Members directly provide to a Participating Hotel (for example, when booking a stay on their website or at check in or check out) is subject to that Participating Hotel's own privacy practices and policies.</p>

<p>We may share Members’ personal information with Bank of America our financial services partner, so that reSynct may offer Members the opportunity to apply for the co-brand reSynct Hotel Rewards® Visa® Card. Specifically, we may share Members’ name, mailing address, email address, and telephone number with Synchrony so that we can identify Members who may be interested in learning about and applying for the reSynct Hotel Rewards Visa credit card.  If you do not want this information shared you may delete your membership account via the Delete Account section in the My Account section of the website.</p>

<h4>Service Provider</h4>

<p>We may use third party service providers to provide specific business support services to us which may involve limited access to Member or Visitor personal information. We require these companies to use the information only to provide the contracted services and prohibit them from transferring the information to another party except as needed to provide the contracted services. Examples of such services include sending emails, conducting and administering promotions, executing surveys, providing customer service, and maintaining Member Account database.</p>

<p>We reserve the right to disclose your personal information with other parties (a) when the Member or Visitor consents to the disclosure, (b) when we believe that it is necessary to protect our rights, protect your safety or the security of our reSynct Site or Members, (c) as required by law, or (d) when we believe that it is necessary to comply with a judicial proceeding, court order, or legal process served on our reSynct Site.</p>

<p>We also may disclose Member or Visitor personal information to other third parties in conjunction with entering into an agreement for the sale of our stock or assets or if we are involved in bankruptcy proceedings. The recipient of personal information following such actions may have privacy policies that differ from those in this policy.  You will be notified via email and/or a prominent notice on our Web site of any change in ownership or uses of your personal information, as well as any choices you may have regarding your personal information.</p>

<h4>Security</h4>

<p>The security of your personal information is important to us. We will take reasonable steps to protect the personal information collected from Members and/or through the reSynct Site from loss, misuse and unauthorized access, disclosure, alteration and destruction. When you enter personal information on our registration forms or within your Account, we encrypt that information using secure socket layer technology (SSL). We, and the service providers maintaining or otherwise handling such personal information on our behalf, have put in place appropriate physical, electronic and managerial procedures to safeguard and secure the personal information from loss, misuse, unauthorized access or disclosure, alteration or destruction. Nevertheless, any information transmitted over the Internet may be subject to breaches of security and we cannot guarantee the security of information you send to or receive from us. Any electronic transmissions that you submit or accept are at your own risk. If you have any questions about security on our reSynct Site, you can email us at <a href="mailto:info@resynct.com">info@resynct.com</a></p>

<h4>Links</h4>

<p>For your convenience, we may provide links from the reSynct Site to other web sites, including those of our partner hotels. The privacy policies on these third party web sites may be different from this policy and we are not responsible for the information collection practices or the content of the third party sites to which we link, including any partner Hotel sites. You access such linked sites at your own risk and should read the privacy policy of any linked site before sharing your personal information on such site.</p>

<h4>Social Media Widgets</h4>

<p>Our Web site includes Social Media Features, such as the Facebook Like button and Widgets, such as the Share this button or interactive mini-programs that run on our site. These Features may collect your IP address, which page you are visiting on our site, and may set a cookie to enable the Feature to function properly. Social Media Features and Widgets are either hosted by a third party or hosted directly on our Site. Your interactions with these Features are governed by the privacy policy of the company providing it.</p>

<h4>Blog</h4>

<p>Our Web site offers publicly accessible blogs or community forums. You should be aware that any information you provide in these areas may be read, collected, and used by others who access them. To request removal of your personal information from our blog or community forum, contact us at support.  In some cases, we may not be able to remove your personal information, in which case we will let you know if we are unable to do so and why.  Alternatively, if you used a third party application to post such information, you can remove it, by either logging into the said application and removing the information or by contacting the appropriate third party application.</p>

<h4>Testimonial</h4>

<p>We display personal testimonials of satisfied customers on our site in addition to other endorsements.  With your consent we may post your testimonial along with your name.  If you wish to update or delete your testimonial, you can contact us at support.</p>

<h4>Choice</h4>

<p>Members have the choice not to complete and submit a personal profile to us and/or to opt out of receiving promotional electronic communications from us. Members can opt out of promotional electronic communications by following the directions contained in such communications or by changing their communications preferences at that My Account portion of the reSynct Site here. Members will need their reSynct Member ID and password to access this portion of the reSynct Site.</p>

<h4>Changes</h4>

<p>We may modify this Policy from time to time. If we decide to change our privacy policy we will notify you of any material changes by email or by means of a notice on the home page of the reSynct Site, prior to the changes becoming effective so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it.</p>

<h4>Contact Us</h4>

<p>If you have any questions about this Privacy Policy or our privacy practices, or if you want to review, correct, delete inaccuracies or change personal information about you, you may do so by making the change within the My Account section of the reSynct Site or by contacting us via support or at the following address: reSynct, Inc. d/b/a reSynct Hotel Rewards, 30 Church Street, NY NY 10007 We will respond to your request to access within 30 days.</p>

<p>Please note that in an effort to prevent the unauthorized disclosure of Member personal information, you may be required to provide proof of identity in order to access personal information. If, upon review, you wish to deactivate your Member profile or update your personal information, you may do so by making the change on your My Account page or by emailing our support here. We will respond to your request to access within 30 days. In some instances, however, information that you requested to be removed may be retained in certain files for a period of time in order to troubleshoot problems. In addition, some types of information may be stored indefinitely on back-up systems or within log files due to technical constraints or financial or legal requirements. Therefore, you should not always expect that all of your personal information will be completely removed from our databases in response to your request.</p>

<h4>Data Retention</h4>

<p>We will retain your information for as long as your account is active or as needed to provide you services. If you wish that we no longer use your information to provide you services, contact us as <a href="mailto:security@resynct.com">security@resynct.com</a>. We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements</p>.

<p>Effective April 11, 2016.</p>
