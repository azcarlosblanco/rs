@extends('front')


@section('content')
<div id="content" class="faq container">
<div class="row">
<div class="col-xs-12">


<h1 class="title line-bottom-center">Frequently asked questions</h1>


@if ($faqs && count($faqs))
@foreach ($faqs as $faq)
<div class="line">
    <h3>{{$faq->question}}
        <span class="glyphicon glyphicon-chevron-up"></span>
        <span class="glyphicon glyphicon-chevron-down"></span>
    </h3>

    <div class="content">
        <?=breaks_to_paragraphs($faq->answer)?>
    </div>
</div>
@endforeach
@endif



</div><!-- .col-xs-12 -->
</div><!-- .row -->
</div><!-- .container -->
@endsection

<?php unset($generate_closure, $get_id); ?>
