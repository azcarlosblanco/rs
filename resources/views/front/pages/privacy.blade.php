@extends('front')


@section('content')
<div id="content" class="container privacy">
<div class="row">
<div class="col-xs-12">
    <?php /* I move content to another file to have option to include it in register_popup */ ?>
    @include('front.pages.privacy_content');
</div><!-- .col-xs-12 -->
</div><!-- .row -->
</div><!-- .container -->
@endsection
