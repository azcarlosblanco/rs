@extends('back')

@section('content')

    <div id="datatable">
        @include($table_view_name)
    </div>

@endsection

@section('footer')
    <script type="text/javascript">
        $('#datatable').DataTable({!! json_encode([
            'total_results_count' => $total_count,
            'page_size' => $page_size,
        ]) !!});
    </script>
@endsection

