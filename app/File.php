<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{

    const SIZE_FULL = 'full';

    const SIZE_THUMBNAIL = 'thumbnail';

    protected $guarded = [];

    protected $file_dir = null;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->file_dir = env('FILE_DIR', 'files');
    }


    public function getDirectory()
    {
        if ( ! $this->exists) {
            throw new \Exception('getDirectory called on non-existing model');
        }

        return $this->file_dir . '/' . ((int)($this->id / 2000) + 1) . '/' . $this->id;
    }

    public function getFilename($resize_variant='full')
    {
        return $resize_variant . '.' . $this->ext;
    }

    public function getPathname($resize_variant='full')
    {
        return $this->getDirectory() . '/' . $this->getFilename($resize_variant);
    }

}
