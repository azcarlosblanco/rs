<?php

namespace App\Providers;

use App\Member;
use App\User;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);

        $gate->define('member.manage', function(Member $member, $member_id=null) {
            // Or a member managing his own profile
            // if member_id is empty then it always fallback to current member id
            if (empty($member_id) or ($member->id == $member_id)) {
                return true;
            }

            // All other cases
            return false;
        });

    }
}
