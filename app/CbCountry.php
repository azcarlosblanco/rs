<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CbCountry extends Model
{



    /*
     * Search for autocomplete by country_name (value)
     * In case when we have only 1 result and $pattern == $result.country_name
     * Than means we have predefined (default) value I return all results to make nice dropdown
     *
     * @param string $pattern
     * @return array
     */
    public function autocomplete ($pattern = '')
    {
        $result = $this->where('value', 'like', "%{$pattern}%")->lists('value', 'code')->all();
        // if ( (count($result) == 1) && (strtolower($pattern) == strtolower(reset($result))) ) {
        //     $result = $this->lists('value', 'code')->all();
        // }

        return $result;
    }



    public function member () { return $this->hasMany('member'); }
}
