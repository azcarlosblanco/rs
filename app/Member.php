<?php

namespace App;

use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Class Member
 * @package App
 * @property int $current_points_amount
 */
class
Member extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{

    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname', 'lastname', 'phone', 'city', 'state', 'country_id'];


    public function getFirstnameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getLastnameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Returns total amount of pending points - reservations waiting for a confirmation.
     * @return int
     */
    public function getOnHoldPoints()
    {
        $points = DB::table('reservations')
            ->select(DB::raw('SUM(points_earned) as on_hold_points'))
            ->where('status', '=', 'new')
            ->where('member_id', '=', $this->id)
            ->get();

        if (count($points)) {
            return (int)$points[0]->on_hold_points;
        }

        return 0;
    }

    /**
     * Returns total amount of earned points from the beginning of the membership.
     * @return int
     */
    public function getTotalEarnedPoints()
    {
        $points = DB::table('reservations')
            ->select(DB::raw('SUM(points_earned) as total_points'))
            ->where('status', '!=', 'new')
            ->where('is_valid', '=', '1')
            ->where('member_id', '=', $this->id)
            ->get();

        if (count($points)) {
            return (int)$points[0]->total_points;
        }

        return 0;
    }

    /**
     * Returns total amount of spent points from the beginning of the membership.
     * @return int
     */
    public function getTotalSpentPoints()
    {
        $points = DB::table('reservations')
            ->select(DB::raw('SUM(points_spent) as total_points'))
            ->where(function($query) {
                $query->whereNotIn('status', ['cancelled', 'no_show'])
                    ->orWhere('is_valid', '=', '0');
            })
            ->where('member_id', '=', $this->id)
            ->get();

        if (count($points)) {
            return (int)$points[0]->total_points;
        }

        return 0;
    }

    /**
     * Recalculates the member's point balance.
     */
    public function updatePointBalance()
    {
        $this->current_points_amount = $this->calculateCurrentPoints();
        $this->save();
    }

    /**
     * Calculates the current user balance.
     *
     * (!!!) - This is duplicated in the admin application in Model_Member
     *
     * @return int
     */
    public function calculateCurrentPoints()
    {
        // Earned points extra (registration etc)
        $extra_points = DB::table('point_transactions')
            ->select(DB::raw('SUM(points) as total'))
            ->where('member_id', '=', $this->id)
            ->get();
        // Earned points for resrvations
        $earned_points = DB::table('reservations')
            ->select(DB::raw('SUM(points_earned) as total'))
            ->where('is_valid', '=', '1')
            ->where('member_id', '=', $this->id)
            ->get();
        // Spent points
        $spent_points = DB::table('reservations')
            ->select(DB::raw('SUM(points_spent) as total'))
            ->where(function($query) {
                return $query->where('is_valid', '=', 1)
                    ->orWhere('status', '=', 'new');
            })
            ->where('member_id', '=', $this->id)
            ->get();
        // Allocated points
        $allocated_points = DB::table('allocations')
            ->select(DB::raw('SUM(points_amount) as total'))
            ->whereNotNull('allocated_at')
            ->where('allocated_at', '>', DB::raw('DATE_SUB(NOW(), INTERVAL '.Allocation::ALLOCATION_TTL.' SECOND)'))
            ->where('member_id', '=', $this->id)
            ->get();

        $extra_points = count($extra_points) ? $extra_points[0]->total : 0;
        $earned_points = count($earned_points) ? $earned_points[0]->total : 0;
        $spent_points = count($spent_points) ? $spent_points[0]->total : 0;
        $allocated_points = count($allocated_points) ? $allocated_points[0]->total : 0;

        return $earned_points + $extra_points - $spent_points - $allocated_points;
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    /**
     * For backward compatibility - in previous version there was multiple user types. Now all users are members (we are using member model for authentication)
     * @return bool
     */
    public function isMember()
    {
        return true;
    }

    public function scopeFulltext($query, $search)
    {
        return $query
            ->where('members.email', 'LIKE', "%$search%")
            ->orWhere(DB::raw("CONCAT(members.firstname, ' ', members.lastname)"), 'LIKE', "%$search%");
    }

    public function getNameForWidget()
    {
        if (strlen($this->firstname) > 10) {
            return 'You have';
        }
        return $this->firstname . ':';
    }

    public function getFullName()
    {
        if (empty($this->firstname) && empty($this->lastname)) {
            return $this->email;
        }
        return trim($this->firstname . ' ' . $this->lastname);
    }

    public function getLongPreviewAttribute()
    {
        return $this->getFullName() . '(' . $this->email . ')';
    }


    public function avatar()
    {
        return $this->hasOne('App\MemberAvatar');
    }


    public function country()
    {
        return $this->belongsTo('App\CbCountry', 'cb_country_id', 'id');
    }
}

