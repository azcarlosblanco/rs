<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use Gate;

class MemberUpdateProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('member.manage', $this->get('members'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required',
            'lastname' => 'required',
        ];
    }
}

