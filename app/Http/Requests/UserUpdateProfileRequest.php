<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use Gate;

class UserUpdateProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user() and Auth::user()->isAdmin());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'role' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users',
        ];

        if ($this->get('role') == 'hotel') {
            $rules['hotel_id'] = 'required';
        }
        if ($this->get('role') == 'member') {
            $rules['member_id'] = 'required';
        }

        if ($this->method() == 'POST') {
            $rules['password'] = 'required|confirmed|min:6';
        } else {
            // PUT or PATH
            $rules['password'] = 'confirmed|min:6';
        }

        return $rules;
    }
}

