<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use Gate;

class HotelUpdateProfileRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('hotel.manage', [$this->route()->parameter('hotels'), array_get($this->route()->getAction(), 'as')]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'real_name' => 'required',
            'website_name' => 'required',
            'email' => 'required|email',
            'cb_country_id' => 'required',
//            'state' => 'required',
            'city' => 'required',
        ];
    }
}

