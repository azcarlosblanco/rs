<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CbCountry;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Autocomplete extends Controller
{
    function __construct()
    {
        $this->middleware('member');
    }


    /**
     * Autocomplete for country field, can search by pattern in country_name
     * //Also return full list of countries in case when we found only 1 result -- for better user expirience
     * //(when you have predifined (default) value after click it still works as should)
     * @param $member_id
     * @return mixed
     */
    protected function getCountry(Request $request)
    {
        $CbCountry   = new CbCountry;

        $pattern     = $request->input('country');
        $countries   = $CbCountry->autocomplete($pattern);
        $suggestions = [];

        foreach ($countries as $country_code => $country_name) {
            $suggestions[] = ['value' => $country_name , 'code' => $country_code];
        }

        $response = ['suggestions' => $suggestions];
        return response()->json($response);
    }
}
