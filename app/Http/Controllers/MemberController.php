<?php

namespace App\Http\Controllers;

use App\Http\Requests\MemberUpdateProfileRequest;
use App\Member;
use App\MemberAvatar;
use App\Reservation;
use App\CbCountry;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Storage;
use Image;
use GeoIP;

class MemberController extends Controller
{

    function __construct()
    {
        $this->middleware('member');
    }

    /**
     * Returns a member model with given ID.
     * If ID is not given then current user's member_id is used.
     * All permission checks were taken care of in Middleware using Gate
     * @param $member_id
     * @return mixed
     */
    protected function getMember($member_id=null)
    {
        if (empty($member_id)) {
            return Auth::user();
        }

        return Member::findOrFail($member_id);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showProfile($id=null)
    {
        $member = $this->getMember($id);

        // If no cb_country_id stored in db prefill it with GeoLocation, but do not store
        if (is_null($member->cb_country_id) || ! $member->country) {
            $country_code = GeoIP::getLocation()['isoCode'];
            $CbCountry    = new CbCountry;
            if ($country  = $CbCountry->where('code', '=', $country_code)->first()) {
                $member->country()->associate($country);
            }
        }

        return view('member.profile', [
            'member' => $member,
        ]);
    }

    /**
     * @param MemberUpdateProfileRequest $request
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateProfile(MemberUpdateProfileRequest $request, $id=null)
    {
        $member = $this->getMember($id);
        $member->fill($request->all());

        // Fill cb_country_id field
        if ($request->input('country_code')) {
            $CbCountry   = new CbCountry;
            if ($country = $CbCountry->where('code', '=', $request->input('country_code'))->first()) {
                $member->country()->associate($country);
            }
        }

        $member->save();

        $this->successFlash();
        return redirect(route('MemberShowProfile', ['id' => $id]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reservationTable()
    {
        $member = $this->getMember();

        return view('member.reservation_table', [
            'upcoming_reservations' => $member->reservations()->upcoming()->orderBy('check_in_date', 'ASC')->get(),
            'other_reservations' => $member->reservations()->notUpcoming()->orderBy('check_out_date', 'DESC')->get(),
        ]);
    }
    /**
     * Display a reservation detail
     *
     * @return \Illuminate\Http\Response
     */
    public function reservationDetail($id)
    {
        $reservation = Reservation::findOrFail($id);

        return [
            'content' => view('partials.reservation_detail', [
                            'reservation' => $reservation,
                        ])->render(),
            'success' => 1,
        ];
    }


    public function balance()
    {
        return view('member.balance', [
            'member' => $this->getMember()
        ]);
    }


    /*
     * Return url to user avatar, used after updating avatar to change avatar on profile without page reloading
     */
    public function getAvatar()
    {
        $response = (object)['url' => false];
        if ($avatar = Auth::user()->avatar) {
            $response->url = $avatar->url();
        }

        return response()->json($response);
    }

    public function postAvatar()
    {
        // SLIM PLUGIN LOGIC (copied from async.php)

        try {
            $images = \App\Logic\Slim::getImages();
        }
        catch (Exception $e) {
            $response = \App\Logic\Slim::getOutputJSON(\App\Logic\SlimStatus::Failure);
            return response()->json($response);
        }

        // If no images found
        if (count($images) === 0) {
            $response = \App\Logic\Slim::getOutputJSON(\App\Logic\SlimStatus::Failure);
            return response()->json($response);
        }

        // Should always be one file (when posting async)
        $image = $images[0];
        $file = \App\Logic\Slim::saveFile($image['output']['data'], $image['input']['name']);


        // INTERNAL LARAVEL LOGIC


        $storage = Storage::disk('public_upload');

        // Okay, we have file, but have we currently avatar for current member?
        if ($avatar = Auth::user()->avatar) {
            // Oh, we have avatar, so we need to delete old
            if ($storage->has($avatar->getFullPath())) {
                // Remove file
                $storage->delete($avatar->getFullPath());
                // And also try to remove all empty directories
                \App\Helpers\Storage::deleteDirectoryIfEmpty($storage, $avatar->getFullPath());
            }
            $avatar->delete(); // Soft model delete
            unset($avatar);
        }

        // Resize avatar on server side & also get info about avatar
        $avatar_image = Image::make($file['path'])->resize(300, 300)->save();

        // Create new entry for new avatar
        $avatar = new MemberAvatar;
        $avatar->extension = pathinfo($file['name'], PATHINFO_EXTENSION);
        $avatar->width     = $avatar_image->width();
        $avatar->height    = $avatar_image->height();
        Auth::user()->avatar()->save($avatar);

        // And move file from /tmp to storage
        \App\Helpers\Storage::moveToStorage($storage, $file['path'], $avatar->getFullPath());

        $response = \App\Logic\Slim::getOutputJSON(\App\Logic\SlimStatus::Success, $file['name'], $file['path']);
        return response()->json($response);
    }
}
