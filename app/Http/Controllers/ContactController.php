<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Mail\Message;
use Mail;
use Session;
use Validator;

class ContactController extends Controller
{

    protected $report_email = 'info@resynct.com';


    function __construct()
    {
        $this->middleware('member');
    }

    public function getCategories()
    {
        return [
            'bug_report' => 'Report a bug',
            'reservation_issue' => 'Issue during reservation',
            'admin_issue' => 'Issue with points',
        ];
    }

    public function getContactForm()
    {
        return view('contact.form', [
            'categories' => $this->getCategories(),
        ]);
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'category' => 'required',
            'text' => 'required|min:20',
        ]);
    }

    public function postContactForm(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }


        $cats = $this->getCategories();
        $cat = array_get($cats, $request->get('category'));
        $text = $request->get('text');
        $member = Auth::user();

        // E-mail an account confirmation link to the new member
        Mail::send('emails.member_contact_form', ['member' => $member, 'text' => $text], function (Message $m) use ($member, $cat) {
            $m->to('info@resynct.com', 'reSynct');
            $m->from($member->email, $member->getFullName());
            $m->subject('Member Contact Form - '.$cat);
        });

        Session::flash('flash_msg', 'Thank you for your feedback. We will get back to you within 48 hours.');
        return redirect('/member/contact-us');
    }


}
