<?php

namespace App\Http\Controllers;

use App\Allocation;
use App\BookingEngine;
use App\Helpers\Currency;
use App\Helpers\Tracking;
use App\Reservation;
use App\Hotel;
use App\WhitelistedDomain;
use Auth;
use Carbon\Carbon;
use Cookie;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use View;

class APIController extends Controller
{

    protected $engine_id = null;
    protected $hotel_id = null;

    protected $wid = null; // widget ID (unique inside the page)

    protected $options = [];

    protected function detectOrigin(Request $request)
    {
        $referer = $request->server('HTTP_REFERER');
        $hostname = parse_url($referer, PHP_URL_HOST);
        // Detect engine/hotel separately - we can have the same domain for the hotel website and for the booking engine
        $engine_domain = WhitelistedDomain::where([
            'hostname' => $hostname,
        ])->where('booking_engine_id', '>', 0)->first();
        $hotel_domain = WhitelistedDomain::where([
            'hostname' => $hostname,
        ])->where('hotel_id', '>', 0)->first();
        if ($engine_domain === null && $hotel_domain === null) {
            return $this->invalidIframeContent('Permission denied.');
        }
        if ($engine_domain && $engine_domain->booking_engine_id) {
            $this->engine_id = $engine_domain->booking_engine_id;
        }
        if ($hotel_domain && $hotel_domain->hotel_id) {
            $this->hotel_id = $hotel_domain->hotel_id;
        }
        return true;
    }

    public function getWidget($type, Request $request)
    {
        \Debugbar::disable();

        if ( ! in_array($type, ['collect', 'redeem', 'login', 'confirm', 'logout'])) {
            return $this->invalidIframeContent('invalid action type');
        }

        // Process logout action
        if ($type === 'logout') {
            Auth::logout();
            return ['success' => 1];
        }

        $this->wid = $request->get('wid');
        if (empty($this->wid)) {
            return $this->invalidIframeContent('Missing widget id.');
        }
        if (($val = $this->detectOrigin($request)) !== true) {
            return $val;
        }
        View::share('user', Auth::user());
        $this->options = [
            'wid' => $this->wid,
            'engine_id' => $this->engine_id,
            'hotel_id' => $this->hotel_id,
            'login_url' => url('auth/login/popup'),
            'register_url' => url('auth/register/popup'),
            'logout_url' => url('api/widget/logout'),
        ];

        // Log the access to database
        Tracking::widgetDisplayed($type, $this->engine_id, $this->hotel_id, $request);

        switch ($type) {
            case 'collect':
                $res = $this->collectWidget($request); break;
            case 'redeem':
                $res = $this->redeemWidget($request); break;
            case 'login':
                $res = $this->loginWidget($request); break;
            case 'confirm':
                $res = $this->confirmWidget($request); break;
        }
        View::share('options', $this->options);

        return $res;
    }


    /**
     * Checks that all given parameters are present in the given array.
     * @param array $data
     * @param array $mandatory_params
     * @return string|bool - true if all params present, string containing first error message if not
     */
    protected function checkData(array $data, array $mandatory_params)
    {
        foreach ($mandatory_params as $param) {
            if ( ! isset($data[$param])) {
                return '"'.$param.'" is missing. These parameters are mandatory: '.implode(', ', $mandatory_params);
            }
        }
        return true;
    }


    protected function collectWidget(Request $request)
    {
        $currency = $request->get('currency');
        $amount = $request->get('amount');
        $hotel_id = $request->get('hotel_id');
        $session_id = $request->get('session_id');
        // Check all parameters were passed
        if (empty($hotel_id) || empty($currency) || empty($amount) || empty($session_id)) {
            return $this->invalidIframeContent('Missing parameter.');
        }
        // Check that given currency is supported
        if ( ! Currency::isCurrencyCodeSupported($currency)) {
            return $this->invalidIframeContent('Currency code is not supported: '.$currency.'.');
        }
        // Check the hotel exists
        $hotel = Hotel::where('engine_hotel_id', $hotel_id)->where('booking_engine_id', $this->engine_id)->first();
        if ($hotel === null) {
            return $this->invalidIframeContent('Hotel not found for engine_id: '.$this->engine_id.', hotel_id: '.$hotel_id);
        }

        // Create an "allocation" if not exists (start session - so that the booking engine can detect it is a resynct reservation
        // Based on this record the booking engine server loads the resynct user related information
        // session_id is known to the booking engine already
        if (Auth::user() && ($res = $this->initAllocationRecord($session_id)) !== true) {
            return $this->invalidIframeContent($res);
        }

        // Convert the currency to USD
        $usd_amount = Currency::getUsdAmount($amount, $currency);
        $points = $hotel->calculatePointsForReservationAmount($usd_amount);

        $width = 180;
        $height = 28;
        return view('iframes.widget_collect', compact('points', 'width', 'height'));
    }


    protected function redeemWidget(Request $request)
    {
        if ( ! Auth::user()) {
            return $this->invalidIframeContent('no user found');
        }
        if ( ! $this->engine_id) {
            return $this->invalidIframeContent('booking engine was not detected');
        }

        $data = $request->all();
        if (($res = $this->checkData($data, ['session_id', 'hotel_id', 'room_code', 'currency', 'amount', 'display_currency', 'display_amount', 'check_in', 'check_out'])) !== true) {
            return $this->invalidIframeContent($res);
        }

        // Check that given currency is supported
        if ( ! Currency::isCurrencyCodeSupported($data['currency'])) {
            return $this->invalidIframeContent('Currency code is not supported: "'.$data['currency'].'".');
        }

        // Check the hotel exists
        $hotel = Hotel::where('engine_hotel_id', $data['hotel_id'])->where('booking_engine_id', $this->engine_id)->first();
        if ($hotel === null) {
            return $this->invalidIframeContent('Hotel not found for engine_id: '.$this->engine_id.', hotel_id: '.$data['hotel_id']);
        }

        // Check that user has enough balance to redeem
        if (Auth::user()->current_points_amount < Hotel::MIN_REDEEMABLE_POINT_AMOUNT) {
            return $this->invalidIframeContent('Not enough points to redeem.');
        }

        // @todo - check that the user has confirmed his e-mail address

        // Create an "allocation" if not exists (start session - so that the booking engine can detect it is a resynct reservation
        // Based on this record the booking engine server loads the resynct user related information
        // session_id is known to the booking engine already
        if (($res = $this->initAllocationRecord($data['session_id'])) !== true) {
            return $this->invalidIframeContent($res);
        }

        // Convert the currency to USD
        $usd_amount = ceil(Currency::getUsdAmount($data['amount'], $data['currency']));
        $user_point_balance = Auth::user()->current_points_amount;
        // Floor to whole hundreds
        $user_point_balance = floor($user_point_balance / 100) * 100;
        $res_amount_in_points = ceil($usd_amount * Hotel::USD_TO_POINTS_MULTIPLIER);
        $max_points = min($user_point_balance, $res_amount_in_points);
        $min_points = Hotel::MIN_REDEEMABLE_POINT_AMOUNT;

        // Calculate the USD -> DisplayCurrency multiplier - based on the calculated usd_amount and given display_amount in given display currency
        // - we can not use our internal currency conversion rates because we need the values to precisely match the values in the booking engine
        $display_currency_multiplier = Currency::calculateMultiplier($usd_amount, $data['display_amount']);

        // Here we generate 10 different steps on how many points the user can redeem
        $select_options = [];
        $steps = 10;
        $round_to = Hotel::REDEEM_POINTS_MINIMUM_STEP;
        for ($i=1; $i<=$steps; $i++) {
            $points = $max_points / $steps * $i;
            if ($points < $min_points) {
                $points = $min_points;
            }
            // Ceil to have the minimum step
            $points = ceil($points / $round_to) * $round_to;
            // Make sure it's not more than a maximum
            if ($points > $max_points) {
                $points = $max_points;
            }
            if ( ! isset($select_options[$points])) {
                if ($i === $steps) {
                    if ($points === $res_amount_in_points) {

                    }
                }
                $dollars = $points / Hotel::USD_TO_POINTS_MULTIPLIER;
                $display_amount = floor($dollars * $display_currency_multiplier); // @todo - rounding - make sure it's in align with our policy
                $select_options[$points] = "$points pts ($display_amount $data[display_currency])";
            }
        }

        $usd_to_points_multiplier = $hotel->getAmountToPointsMultiplier();
        $flat_points = $hotel->getFlatPointsAmount();

        $options = $request->only([
            'session_id',
            'hotel_id',
            'room_code',
            'check_in',
            'check_out',
            'max_points',
            'min_points',
            'currency',
        ]);
        $options['rate_code'] = array_get($data, 'rate_code', ''); // optional parameter
        $options['save_point_selection_url'] = route('api.js_request', ['action' => 'save_point_selection']);
        $options['clear_point_selection_url'] = route('api.js_request', ['action' => 'clear_point_selection']);
        $options['engine_id'] = $this->engine_id;
        $options['amount'] = $data['amount']; // amount in HOTEL base currency
        $options['usd_amount'] = $usd_amount;
        $options['display_currency'] = $data['display_currency'];
        $options['display_currency_multiplier'] = $display_currency_multiplier;
        $options['usd_to_points_multiplier'] = $usd_to_points_multiplier;
        $options['flat_points'] = $flat_points;

        $this->options = array_merge($this->options, $options);

        // @todo - remove if not needed
        $width = 172;
        $height = 28;
        return view('iframes.widget_redeem', compact('width', 'height', 'select_options'));
    }

    /**
     * Basically starts the user session in the booking engine.
     * @param $session_id
     * @return array|bool
     */
    protected function initAllocationRecord($session_id)
    {
        $alloc = Allocation::where([
            'booking_engine_id' => $this->engine_id,
            'session_id' => $session_id,
            'member_id' => Auth::user()->id,
        ])->first();

        if ($alloc === null) {
            // Allocation does not exist yet - create the new empty one
            // This can be called multiple times in parallel so we need to make sure we don't duplicate the record
            // - it's impossible to create a locked allocation record
            DB::insert('INSERT INTO allocations (booking_engine_id, session_id, member_id) values (?, ?, ?) ON duplicate KEY UPDATE created_at=NOW()', [$this->engine_id, $session_id, Auth::user()->id]);

        } else {
            // Allocation exists already - make sure it is not allocated yet (not part of the reservation in progress at the moment)
            if ( ! $alloc->allocated_at) {
                // Not allocated - we can reuse the record (and the given session ID)
                // We just need to make sure there is no invalid selection saved in it
                // @todo - make sure this makes sense
                $alloc->clearSelection();
                $alloc->save();
            } else {
                return 'The locked allocation already exists for the given session_id. Please refresh your session_id - provide a new one.';
            }
        }
        return true;
    }

    protected function loginWidget(Request $request)
    {
        // Set a tracking cookie here to set members.source_hotel_id based on that after the registration
        Cookie::queue('rs_hid', $this->hotel_id, 60*24*Hotel::MEMBER_COOKIE_EXPIRATION_DAYS);

        $profile_url = route('MemberShowProfile');
        $width = 112;
        $height = 28;
        return view('iframes.widget_login', compact('width', 'height', 'profile_url'));
    }


    protected function confirmWidget(Request $request)
    {
        if ( ! Auth::user()) {
            return $this->invalidIframeContent('no user found');
        }
        $data = $request->all();
        if (($res = $this->checkData($data, ['display_currency', 'display_amount', 'display_taxes_amount'])) !== true) {
            return $this->invalidIframeContent($res);
        }

        $session_id = $request->get('session_id');
        $reservation_id = $request->get('reservation_id');
        if (empty($session_id) && empty($reservation_id)) {
            return $this->invalidIframeContent('no ID given');
        }
        // Check that given currency is supported
        if ( ! Currency::isCurrencyCodeSupported($data['display_currency'])) {
            return $this->invalidIframeContent('Currency code is not supported: "'.$data['display_currency'].'".');
        }

        if ($session_id) {
            $id = $session_id;
            $type = 'session';
        } else {
            $id = $reservation_id;
            $type = 'reservation';
        }
        $params = [
            'room_rate' => 0,
            'discount' => 0,
            'spent_points' => 0,
            'earned_points' => 0,
            'final_rate' => 0,
        ];

        if ($type == 'session') {
            $alloc = Allocation::where([
                'booking_engine_id' => $this->engine_id,
                'session_id' => $id,
                'member_id' => Auth::user()->id,
            ])->first();
            if ($alloc === null) {
                return $this->invalidIframeContent('allocation not found');
            }

            $usd_amount = $alloc->reservation_amount_usd;
            $usd_discount = $alloc->getDiscountInUSD();
            $params['spent_points'] = (int)$alloc->points_amount;
            $params['earned_points'] = $alloc->getPointsAmountToEarn();
        }
        if ($type == 'reservation') {
            $filters = [
                'booking_engine_id' => $this->engine_id,
                'code' => $id,
                'member_id' => Auth::user()->id,
            ];
            $res = Reservation::where($filters)->first();
            if ($res === null) {
                return $this->invalidIframeContent('reservation not found');
            }

            $usd_amount = $res->total_amount;
            $usd_discount = $res->discount_amount;
            $params['spent_points'] = (int)$res->points_spent;
            $params['earned_points'] = $res->points_earned;
        }

        $usd_to_display_currency = Currency::calculateMultiplier($usd_amount, $data['display_amount']);
        $params['currency'] = strtoupper($data['display_currency']);
        // Room rate in display currency - we received that as a parameter
        $params['room_rate'] = round($data['display_amount'], 2);
        // Discount in display_currency
        $params['discount'] = round($usd_discount * $usd_to_display_currency, 2);
        $params['taxes'] = round($data['display_taxes_amount'], 2);

        // @todo - if data[display_total_amount] is present then us it and skip the calculation

        if (isset($data['display_total_amount']) && $data['display_total_amount']) {
            $params['final_rate'] = $data['display_total_amount'];
        } else {
            $params['final_rate'] = round($data['display_amount'] - $params['discount'] + $data['display_taxes_amount'], 2);
        }

        $type = 'confirm';
        return view('iframes.widget_confirm', [
            'type' => $type,
            'id' => $id,
            'data' => $params,
        ]);
    }

    /**
     * Returns an empty iframe with the error message inside the html comment - for easy integration debugging.
     * @param $msg
     * @return View
     */
    protected function invalidIframeContent($msg)
    {
        return view('iframes.widget_invalid', ['msg' => $msg]);
    }



    // =========================================================================================================================================================================== //
    // ============================================================= AJAX CALLS from the inside of the iframes =================================================================== //
    // =========================================================================================================================================================================== //


    /**
     * All CORS AJAX calls gateway.
     * All code needed is here (and not in Middlewares etc.) to make extracting to a different (micro) framework easier.
     * @param Request $request
     */
    public function jsRequest($action, Request $request)
    {
        \DebugBar::disable();

        // no need for xDomain policy anymore - it was rebuilt using iframes
//        $origin = $request->headers->get('Origin');

        $res = $this->processClientRequest($action, $request);

        return response($res, 200, [
            // no need for xDomain policy anymore - it was rebuilt using iframes
//            'Access-Control-Allow-Origin' => $origin,
//            'Access-Control-Allow-Credentials' => 'true',
//            'Access-Control-Allow-Methods' =>  'GET, POST, PUT, DELETE, OPTIONS',
//            'Access-Control-Allow-Headers' => 'Content-Type, Authorization, X-Requested-With',
        ]);
    }


    protected function processClientRequest($action, Request $request)
    {
        $user = Auth::user();
        // Only member is a valid user here
        $user = ($user) ? $user : null;
        $user_id = $user ? $user->id : null;

        switch ($action) {

            // @todo - this should be called "startSession"
            case 'load_user':
                return $this->loadUser($user, $request->all());

            case 'load_hotel':
                return $this->getHotelInfo($request->input('hotel_ids'));

            case 'save_point_selection':
                return $this->savePointSelection($user, $request->all());

            case 'clear_point_selection':
                return $this->clearPointSelection($user, $request->all());

        }

        throw new HttpException(404, 'Invalid API method called: "'.$action.'".');
    }

    protected function loadUser($user, $data)
    {
        // If the user is authenticated we need to initialize the allocation record
        // - create a mapping between "session_id" and "member_id" - because in S2S API only session_id is used
        if ($user && isset($data['booking_engine_id'], $data['session_id'])) {
            // This is a call from booking engine (because session_id is provided)
            // let's "start a session" by creating an empty allocation record
            $alloc = Allocation::where([
                'booking_engine_id' => $data['booking_engine_id'],
                'session_id' => $data['session_id'],
                'member_id' => $user->id,
            ])->first();

            if ($alloc === null) {
                // Allocation does not exist yet - create the new empty one
                $alloc = Allocation::create([
                    'booking_engine_id' => $data['booking_engine_id'],
                    'session_id' => $data['session_id'],
                    'member_id' => $user->id,
                    'created_at' => Carbon::now(),
                ]);
            } else {
                // Allocation exists already - make sure it is not allocated yet (not part of the reservation in progress at the moment)
                if ( ! $alloc->allocated_at) {
                    // Not allocated - we can reuse the record (and the given session ID)
                    // We just need to make sure there is no invalid selection saved in it
                    // @todo - make sure this makes sense
                    $alloc->clearSelection();
                    $alloc->save();
                } else {
                    return ['error' => 'The locked allocation already exists for the given session_id. Please refresh your session_id - provide a new one.'];
                }
            }
        }
        return [
            'user_id' => $user ? $user->id : null,
            'user_name' => $user ? $user->getFullName() : '',
            'user_firstname' => $user ? $user->firstname : '',
            'user_lastname' => $user ? $user->lastname : '',
            'user_points' => $user ? $user->current_points_amount : null,
        ];
    }

    protected function savePointSelection($user, array $data)
    {
        if ( ! $user or ! $user->isMember()) {
            return ['error' => 'not_authenticated'];
        }

        // Validate the request parameters
        if (($res = $this->checkData($data, ['booking_engine_id', 'session_id', 'hotel_id', 'room_code', 'currency', 'amount', 'points_amount', 'check_in', 'check_out'])) !== true) {
            return ['error' => 'invalid_request', 'msg' => $res];
        }

        // Check that given currency is supported
        if ( ! Currency::isCurrencyCodeSupported($data['currency'])) {
            return ['error' => 'invalid_request', 'msg' => 'Currency code is not supported: '.$data['currency'].'.'];
        }

        // Load the hotel - we need commission info
        $hotel = Hotel::where([
            'engine_hotel_id' => $data['hotel_id'],
            'booking_engine_id' => $data['booking_engine_id'], // @todo - add this field to the hotel profile
        ])->first();
        if ($hotel === null) {
            return ['error' => 'Hotel with ID "'.$data['hotel_id'].'" not found.'];
        }

        // Check that user has enough points to make the selection
        if ($user->current_points_amount < $data['points_amount']) {
            return ['error' => 'not_enough_points', 'msg' => 'There is not enough points on the member\'s account to make this selection.'];
        }

        // Member can only have one selection per session in the booking_engine - try to find it
        $alloc = Allocation::firstOrNew([
            'session_id' => $data['session_id'],
            'member_id' => $user->id,
            'booking_engine_id' => $data['booking_engine_id'],
        ]);

        $alloc->room_code = $data['room_code'];
        $alloc->rate_code = array_get($data, 'rate_code', '');
        $alloc->engine_hotel_id = $data['hotel_id']; // Hotel ID inside the Booking Engine
        $alloc->booking_engine_id = $data['booking_engine_id']; // Hotel ID inside the Booking Engine
        // we need to have currency multiplier valid at the time of the selection (rates can change before the booking is made - but we need to use this rate
        $alloc->setHotel($hotel);
        // Check that the user account is confirmed - if not then the user can not spend points
        if ($user->confirmed) {
            $alloc->points_amount = $data['points_amount'];
        } else {
            $alloc->points_amount = 0;
        }
        $alloc->setReservationAmount($data['amount'], $data['currency']);
        $alloc->check_in_date = $data['check_in'];
        $alloc->check_out_date = $data['check_out'];
        $alloc->created_at = Carbon::now()->format('Y-m-d H:i:s'); // We need to always update this
        $alloc->save();

        return ['success' => 1];
    }

    protected function clearPointSelection($user, array $data)
    {
        if ( ! $user or ! $user->isMember()) {
            return response(['error' => 'not_authenticated']);
        }
        if ( ! isset($data['booking_engine_id'], $data['session_id'])) {
            return response(['error' => 'invalid_request', 'msg' => 'Expected parameters: session_id, booking_engine_id.']);
        }

        // Load the allocation
        $alloc = Allocation::where([
            'session_id' => $data['session_id'],
            'booking_engine_id' => $data['booking_engine_id'],
        ])->first();

        if ($alloc === null) {
            return response(['success' => '1', 'description' => 'No selection to delete.']);
        }

        $alloc->clearSelection();
        $alloc->save();

        return ['success' => 1];
    }

    protected function getHotelInfo($hotel_ids)
    {
        \DebugBar::disable();
        $hotels = Hotel::wherein('engine_hotel_id', $hotel_ids)->get();
        $res = [];
        foreach ($hotels as $hotel) {
            $res[$hotel->engine_hotel_id] = [
                'usd_to_points_multiplier' => $hotel->getAmountToPointsMultiplier(),
                'flat_points' => $this->getFlatPointsAmount(),
            ];
        }

        return $res;
    }



}

