<?php

namespace App\Http\Controllers;

use App\Allocation;
use App\BookingEngine;
use App\Helpers\Currency;
use App\Hotel;
use App\Member;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Mail\Message;
use Mail;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ServerAPIController extends Controller
{

    protected $allowed_actions = ['get_selection', 'allocate_points', 'confirm_reservation', 'rollback_allocation'];

    protected $engine_id = null;

    /**
     * Checks that all given parameters are present in the given array.
     * @param array $data
     * @param array $mandatory_params
     * @return array|bool - true if all params present
     */
    protected function checkData(array $data, array $mandatory_params)
    {
        foreach ($mandatory_params as $param) {
            if ( ! isset($data[$param])) {
                return ['result' => 'error', 'error' => 'invalid_request', 'description' => '"'.$param.'" is missing. These parameters are mandatory: '.implode(', ', $mandatory_params)];
            }
        }
        return true;
    }

    /**
     * All Server2Server calls are in this controller.
     * All code needed is here (and not in Middlewares etc.) to make extracting to a different (micro) framework easier.
     * @param Request $request
     */
    public function processRequest($action, Request $request)
    {
        \DebugBar::disable();

        if ( ! in_array($action, $this->allowed_actions)) {
            return response(['result' => 'error', 'error' => 'invalid_action', 'description' => 'Action "'.$action.'" not supported.']);
        }
        if (($res = $this->detectEngineId($request)) !== true) {
            return response($res, 403);
        }

        $res = $this->processServerRequest($action, $request);

        return response($res, 200);
    }

    /**
     * Detects booking engine ID based on IP address.
     */
    protected function detectEngineId(Request $request)
    {
//        $ip = request()->ip();
        $username = $request->get('username');
        $pass = $request->get('password');
        $engine = BookingEngine::where('username', '=', $username)
            ->first();
        if ($engine === null || ! Hash::check($pass, $engine->password)) {
            return ['result' => 'error', 'error' => 'invalid_credentials', 'Given credentials are invalid. Please check your username and password.'];
        }
        $this->engine_id = $engine->id;

        // everything OK
        return true;
    }


    /**
     * Just a crossroad deciding what action to call.
     * @param $action
     * @param Request $request
     * @return array
     */
    protected function processServerRequest($action, Request $request)
    {
        $params = $request->except('username', 'password');
        switch ($action) {
            case 'get_selection':
                return $this->getSelection($params);

            case 'allocate_points':
                return $this->allocatePoints($params);

            case 'confirm_reservation':
                return $this->confirmReservation($params);

            case 'rollback_allocation':
                return $this->rollbackAllocation($params);
        }

        return ['result' => 'error', 'error' => 'invalid_action', 'error' => 'Action "'.$action.'" is not implemented.'];
    }


    /**
     * Returns info about amount of points selected for given session_id and corresponding discount in given currency.
     * Also updated the reservation amount in the allocation record.
     * @param array $data
     * @return array
     */
    protected function getSelection(array $data)
    {
        // Check that all mandatory parameters are present
        if (($res = $this->checkData($data, ['session_id', 'hotel_id', 'room_code', 'amount', 'currency', 'display_amount', 'display_currency', 'check_in_date', 'check_out_date'])) !== true) {
            return $res;
        }
        $rate_code = array_get($data, 'rate_code', '');

        // Load the hotel - we need commission info
        $hotel = Hotel::where([
            'engine_hotel_id' => $data['hotel_id'],
            'booking_engine_id' => $this->engine_id,
        ])->first();
        if ($hotel === null) {
            return ['result' => 'error', 'error' => 'Hotel with ID "'.$data['hotel_id'].'" not found. Or a different booking engine is assigned to it.'];
        }

        // Booing engine can only have 1 selection per session
        $alloc = Allocation::where([
            // @todo - hotel_id could be here as well (not in case there is multiple hotel_ids on the search results
            'session_id' => $data['session_id'],
            'booking_engine_id' => $this->engine_id,
        ])->first();

        if ($alloc === NULL) {
            // Allocation not found - no authenticated user session was started for given session_id - there is nothing to do
            // - no further ReSynct processing should take place in Booking Engine
            return ['result' => 'no_allocation_found', 'description' => 'No authenticated user session was started. This is not a reSynct reservation.'];
        }

        // === Allocation/session does exist here

        // Shall we populate the allocation record with the request data?
        $load_data = true;
        if ($alloc->points_amount) {
            // We have some selection
            // If the room_code, hotel_id and reservation_amount matches given data then the selection is valid for this reservation
            if (
                $alloc->room_code == $data['room_code']
                && $alloc->rate_code == $rate_code

                // === @todo !!! I'm not sure currency and amount should be checked here - it requires a BE to know the hotel's base currency and send the exact amount to us
//                && $alloc->currency == $data['currency'] // hotel base currency
//                && $alloc->reservation_amount == $data['amount'] // amount in hotel base currency - it need to match

                && $alloc->engine_hotel_id == $data['hotel_id']
                && $alloc->check_in_date == $data['check_in_date']
                && $alloc->check_out_date == $data['check_out_date']
            ) {
                // We have valid allocation data - no need to populate
                $load_data = false;
            }
        }

        if ($load_data) {
            // The selection is not valid for given data - it was made on a different room item in the booking engine
            // - Set new data to the allocation record (there will be no selection = no rePoints redemption)
            $alloc->setHotel($hotel);
            $alloc->room_code = $data['room_code'];
            $alloc->rate_code = $rate_code;
            $alloc->check_in_date = $data['check_in_date'];
            $alloc->check_out_date = $data['check_out_date'];
            // There is never a selection (or it's invalid) if we are loading data from the request now
            $alloc->points_amount = 0;
        }

        // Always update the allocation with last received amount
        // It is possible that the USD amount will change and the redeemed discount in hotel currency will be different than what the user saw on the step2
        // (because of the conversion rate fluctuations)
        // - there is no simple way to avoid this. The user can always go back and apply points again.
        $alloc->setReservationAmount($data['amount'], $data['currency']);
        $alloc->save();

        $usd_to_display_currency = Currency::calculateMultiplier($alloc->reservation_amount_usd, $data['display_amount']);

        return [
            'result' => 'success',
            'allocation_id' => $alloc->id,
            // User data requested by Luca (on Skype)
            'user' => [
                'full_name' => $alloc->member->getFullName(),
                'email' => $alloc->member->email,
            ],
            'data' => [
                'points_to_spend' => $alloc->points_amount,
                'currency' => $data['display_currency'],
                'points_to_earn' => $alloc->getPointsAmountToEarn(),
                'total_discount' => $alloc->getDiscountInUSD() * $usd_to_display_currency,
            ],
        ];
    }

    /**
     * Allocates given points amount and removes them from the member's balance - so that he can not allocate them again.
     * @param array $data
     * @return array
     */
    protected function allocatePoints(array $data)
    {
        // Check that all mandatory parameters are present
        if (($res = $this->checkData($data, ['session_id', 'allocation_id'])) !== true) {
            return $res;
        }

        // Booking engine can only have 1 selection per session
        $alloc = Allocation::where([
            'id' => $data['allocation_id'],
            'session_id' => $data['session_id'],
            'booking_engine_id' => $this->engine_id,
        ])->first();

        // No allocation found
        if ($alloc === null) {
            return ['result' => 'error', 'error' => 'No allocation was found for given session_id and allocation_id.'];
        }

        // Allocate points
        if (($res = $alloc->allocatePoints()) === true) {
            return [
                'result' => 'success',
                'allocation_id' => $alloc->id,
            ];
        } else {
            return ['result' => 'error', 'error' => $res];
        }
    }

    /**
     * Allocates given points amount and removes them from the member's balance - so that he can not allocate them again.
     * @param array $data
     * @return array
     */
    protected function confirmReservation(array $data)
    {
        // Check that all mandatory parameters are present
        if (($res = $this->checkData($data, ['session_id', 'allocation_id', 'reservation_code'])) !== true) {
            return $res;
        }

        // Booing engine can only have 1 selection per session
        $alloc = Allocation::where([
            'id' => $data['allocation_id'],
            'session_id' => $data['session_id'],
            'booking_engine_id' => $this->engine_id,
        ])
            ->valid() // scope filter defined in model
            ->first();

        if ($alloc === null) {
            return ['result' => 'error', 'error' => 'no_allocation_found', 'description' => 'No allocation was found.'];
        }

        // Allocate point
        if (($res = $alloc->placeReservation($data['reservation_code'])) && $res->id) {

            // Confirmation e-mail to the user
            $head_info = $res->points_earned . ' ' .'Points Earned!';
            Mail::send('emails.member_reservation_confirmation', ['member' => $alloc->member, 'res' => $res, 'head_info' => $head_info], function (Message $m) use ($alloc) {
                $m->to($alloc->member->email, $alloc->member->getFullName());
                $m->from('status@resynct.com', 'reSynct');
                $m->subject('reSynct booking notification');
            });

            return ['result' => 'success'];
        } else {
            return ['result' => 'error', 'error' => $res];
        }
    }

    /**
     * Allocates given points amount and removes them from the member's balance - so that he can not allocate them again.
     * @param array $data
     * @return array
     */
    protected function rollbackAllocation(array $data)
    {
        // Check that all mandatory parameters are present
        if (($res = $this->checkData($data, ['session_id', 'allocation_id'])) !== true) {
            return $res;
        }

        // Find the allocation
        $alloc = Allocation::where([
            'id' => $data['allocation_id'],
            'session_id' => $data['session_id'],
            'booking_engine_id' => $this->engine_id,
        ])
            ->whereNotNull('allocated_at')
            ->first();

        if ($alloc === null) {
            return ['result' => 'error', 'error' => 'no_allocation_found', 'description' => 'No allocated selection found for allocation_id: "'.$data['allocation_id'].'"'];
        }

        // Allocate points
        if (($res = $alloc->cancelAllocation()) === true) {
            return ['result' => 'success'];
        } else {
            return ['result' => 'error', 'error' => $res];
        }
    }


}

