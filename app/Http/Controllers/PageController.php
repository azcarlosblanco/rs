<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Response;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PageController extends Controller
{


    public function faq_member ()
    {
        return Response::view('front.pages.faq', ['faqs' => $this->_get_faqs('member')]);
    }

    public function faq_hotel ()
    {
        return Response::view('front.pages.faq', ['faqs' => $this->_get_faqs('hotel')]);
    }

    protected function _get_faqs($mode)
    {
        $mode = in_array($mode, ['member', 'hotel']) ? $mode : 'member';
        return DB::table('faqs')->where('type', $mode)->orderBy('sequence')->orderBy('id')->get();
    }


    public function privacy ()
    {
        return Response::view('front.pages.privacy');
    }
}
