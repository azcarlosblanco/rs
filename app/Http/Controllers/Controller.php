<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Session;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        header('P3P: CP="Potato"');
    }


    protected function tableResponse(Request $request, \App\Repository\Repository $repository, $table_view_name, $view_params=array())
    {
        $repository->useRequest($request);
        $results = $repository->getResults();
        $total_count = $repository->getTotalResultsCount();
        $filter = $request->get('filter', array());


        if ($request->ajax()) {
            // Ajax response
            return array(
                'content' => view($table_view_name, [
                    'results' => $results,
                    'filter' => $filter,
                ] + $view_params)->render(),
                'total_count' => $total_count,
            );
        }

        // Standard response - with layout
        return view('datatable', [
            'results' => $results,
            'filter' => $filter,
            'table_view_name' => $table_view_name,
            'total_count' => $total_count,
            'page_size' => $repository->getPageSize(),
        ] + $view_params);
    }

    /**
     * Sets flash message after successful action.
     */
    public function successFlash()
    {
        Session::flash('flash_msg', trans('app.object.saved'));
    }
}
