<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Tracking;
use App\Member;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Mail;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use App\Http\Traits\CreatesUser;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
class MemberPopupAuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins, CreatesUser;

    protected $loginPath = 'auth/login/popup';


    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        \DebugBar::disable();
        // User is logged in but it's not a member - log him out
        if (Auth::check() and ! Auth::user()->isMember()) {
            Auth::logout();
            // And continue to whatever page was requested (login page probably)
        }
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        // Make sure only confirmed users can login
        return $request->only($this->loginUsername(), 'password') + array('active' => '1', 'role' => 'member');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister(Request $request)
    {
        Tracking::widgetDisplayed('register_popup', null, null, $request);
        return view('auth.register_popup');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'max:45',
            'lastname' => 'max:45',
            'full_name' => 'required|max:91',
            'email' => 'required|email|max:100|unique:members',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        Auth::login($this->create($request->all(), true));

        // Just close the popup window
        return $this->registered(null, null);
    }



    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin(Request $request)
    {
        Tracking::widgetDisplayed('login_popup', null, null, $request);
//        return view('auth.register_popup_confirm'); // for registration confirmation page testing only
        return view('auth.login_popup');
    }

    protected function authenticated($request, $user)
    {
        return view('auth.login_popup_after');
    }

    protected function registered()
    {
        return view('auth.register_popup_confirm', ['user' => Auth::user()]);
    }




}
