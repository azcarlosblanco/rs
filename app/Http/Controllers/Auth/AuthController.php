<?php

namespace App\Http\Controllers\Auth;

use App\Member;
use App\User;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Mail;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use App\Http\Traits\CreatesUser;
use App\Http\Traits\AjaxHandler;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins, CreatesUser, AjaxHandler;


    protected $redirectAfterLogout;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getLogout', 'getConfirmation']]);

        $this->redirectAfterLogout = route('home.getMember');
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout(Request $request)
    {
        Auth::logout();

        $ret_url = $request->get('ret_url');
        if ( ! empty($ret_url)) {
            return redirect($ret_url);
        }

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        // Make sure only confirmed users can login
        return $request->only($this->loginUsername(), 'password') + array('active' => '1');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'max:45',
            'lastname' => 'max:45',
            'full_name' => 'required|max:91',
            'email' => 'required|email|max:100|unique:members',
            'password' => 'required|confirmed|min:6',
        ]);
    }


    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegisterAjax(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }

        $user = $this->create($request->all());
        Session::set('registered_user_id', $user->id);
        Auth::login($user, true);

        $response = redirect(route('MemberReservationTable'));
        return $this->_ajaxHandler($request, $response);
    }


    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLoginAjax(\Illuminate\Http\Request $request)
    {
        $response = $this->postLogin($request);
        return $this->_ajaxHandler($request, $response);
    }


    /**
     * @param $user_id
     * @param $token
     */
    public function getConfirmation($user_id, $token)
    {
        $user = Member::findOrFail($user_id);

        // Check the confirmation token ttl
        $least_create_date = Carbon::now()->subDays(Config::get('auth.confirmation_token_ttl_days'));

        // Is confirmation token still valid?
        if ($user->created_at->gt($least_create_date) and $user->confirmation_token == $token) {
            $user->confirmed_at = Carbon::now();
            $user->confirmed = 1; // we need specific value to use in getCredentials method
            $user->active = 1; // we need specific value to use in getCredentials method
            $user->confirmation_token = ''; // clear the confirmation token so that the link can not be used multiple times (after the user is blocked by admin for example)
            $user->save();
            // Account confirmed - log user in and remember him (cookie)
            Auth::login($user, true);

            return redirect('/home');
        }

        // Invalid confirmation token
        return view('auth.member_confirmation_error');
    }



}

