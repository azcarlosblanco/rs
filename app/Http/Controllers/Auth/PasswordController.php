<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Session;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmail($popup=false)
    {
        if ($popup) {
            \Debugbar::disable();
            return view('auth.password_popup');
        }
        return view('auth.password');
    }


    /**
     * Handle a pasword reset link request
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmailAjax(\Illuminate\Http\Request $request)
    {
        $response = $this->postEmail($request);
        return $this->generateResponse($request, $response);
    }


    /*
     * Return repsonse as json for ajax or as normal response for normal request
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Illuminate\Http\RedirectResponse $response
     * @return \Illuminate\Http\JsonResponse or Illuminate\Http\RedirectResponse
     */
    protected function generateResponse(\Illuminate\Http\Request $request, \Illuminate\Http\RedirectResponse $response)
    {
        if ($request->ajax()) {
            // Prepare errors for ajax, format [{'email' => ['incorrect e-mail']}, ...]
            $errors = Session::pull('errors');
            $response_errors = null;
            if ( ! is_null($errors) && $errors->any()) {
                foreach ($errors->keys() as $key) {
                    $response_errors[$key] = $errors->get($key);
                }
            }

            // Generate json with data from response
            return response()->json([
                'target_url' => $response->getTargetUrl(),
                'errors'     => $response_errors,
                'status'     => (Session::get('status') ? Session::pull('status') : null),
            ]);
        }

        return $response;
    }

}
