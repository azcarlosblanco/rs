<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Gate;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class IsMemberMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get member ID (if any)
        $member_id = Auth::user() ? Auth::user()->id : null;

        if (Gate::allows('member.manage', $member_id)) {
            // We are good to continue
            return $next($request);
        }

        // We are not good - not found exception
        return redirect('auth/logout');
    }
}
