<?php
/**
 * Handles the creation of the new user (after the registration)
 */

namespace App\Http\Traits;

use Session;
use \Illuminate\Http\Request;
use \Illuminate\Http\RedirectResponse;

trait AjaxHandler {

    /*
     * Return repsonse as json for ajax or as normal response for normal request
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Illuminate\Http\RedirectResponse $response
     * @return \Illuminate\Http\JsonResponse or Illuminate\Http\RedirectResponse
     */
    protected function _ajaxHandler (\Illuminate\Http\Request $request, \Illuminate\Http\RedirectResponse $response)
    {
        if ( ! $request->ajax()) {
            return $response;
        }

        // @TODO
        // if ($validator->fails()) {
        //     $this->throwValidationException($request, $validator);
        // }

        // Prepare errors for ajax, format [{'email' => ['incorrect e-mail']}, ...]
        $errors = Session::pull('errors');
        $response_errors = null;
        if ( ! is_null($errors) && $errors->any()) {
            $errors_array = [];
            foreach ($errors->keys() as $key) {
                $response_errors[$key] = $errors->get($key);
            }
        }

        // Generate json with data from response
        return response()->json([
            'target_url' => $response->getTargetUrl(),
            'errors'     => $response_errors,
            'status'     => (Session::get('status') ? Session::pull('status') : null),
        ]);
    }
}
