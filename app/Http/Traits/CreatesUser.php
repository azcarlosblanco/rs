<?php
/**
 * Handles the creation of the new user (after the registration)
 */

namespace App\Http\Traits;
use App\Hotel;
use App\PointTransaction;
use Cookie;
use DB;
use Illuminate\Mail\Message;
use Intervention\Image\Point;
use Mail;
use App\Member;
use Request;

trait CreatesUser {


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data, $from_popup=false)
    {
        DB::beginTransaction();

        // Split the name info firstname/lastname
        $firstname = $lastname = '';
        if (array_key_exists('full_name', $data) && $data['full_name']) {
            $full_name = explode(' ', trim($data['full_name']));
            if (count($full_name) > 1) {
                $lastname  = array_pop($full_name);
                $firstname = implode(' ', $full_name);
            } else {
                $firstname = array_shift($full_name);
                $lastname  = '';
            }
            unset($full_name);
        }

        // Create a member record first
        $member = new Member();
        $member->fill([
            'firstname' => $firstname,
            'lastname' => $lastname,
        ]);
        $member->email = $data['email'];
        $member->password = bcrypt($data['password']);
        // Generate the confirmation token and create a user
        $member->confirmation_token = str_random(100);
        $member->role = 'member';
        $member->active = 1;

        // Assign the member to the hotel whose ID is in the cookie
        $hotel_id = Cookie::get('rs_hid');
        $hotel = Hotel::find($hotel_id);
        if ($hotel && $hotel->id) {
            $member->source_hotel_id = $hotel_id;
            $member->source_hotel_last_visit = date('Y-m-d H:i:s');
        }
        $member->source_ip = Request::ip();
        $member->save();


        // If it's registration via pop-up window and there is a registration reward at the hotel
        if ($from_popup && $hotel && $hotel->member_registration_reward > 0) {
            // Create a reward record
            $pt = new PointTransaction();
            $pt->member_id = $member->id;
            $pt->points = $hotel->member_registration_reward;
            $pt->type = 'registration_hotel_reward';
            $pt->save();
            $member->updatePointBalance();
        }
        DB::commit();

        // E-mail an account confirmation link to the new member
        Mail::send('emails.member_account_confirmation', ['member' => $member], function (Message $m) use ($member) {
            $m->to($member->email, $member->getFullName());
            $m->from('info@resynct.com', 'reSynct');
            $m->subject('Welcome to reSynct!');
        });

        return $member;
    }


}
