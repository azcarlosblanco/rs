<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// ========================================================================
// =================== General public website routes ======================

// Route::get('/', function () {
//     // @todo - return user public website
//     return redirect('auth/login');
// });
Route::get('/', 'Home@index');

// @todo - add "/member" route and controller for the member website
Route::get('/member', [
    'uses' => 'Home@getMember',
    'as' => 'home.getMember'
]);



Route::get('/home', function () {
    if (Auth::guest()) {
        return redirect('/');
    }
    return redirect('/member/reservations');
});


// ========================================================================
// ========================== API related routes ==========================

// Confirmation iframe
Route::get('/api/ifr/{engine_id}/{type}/{id}', [
    'uses' => 'APIController@getConfirmationIframe',
    'as' => 'api.getConfirmationIframe',
])->where([
    'type' => '(session|reservation)'
]);

// Widgets
Route::get('/api/widget/{type}', [
    'uses' => 'APIController@getWidget',
    'as' => 'api.getWidget',
])->where([
    'type' => '(collect|redeem|login|confirm|logout)'
]);



// Server 2 server API
Route::any('/api/s2s/{action}', [
    'uses' => 'ServerAPIController@processRequest',
    'as' => 'api.s2s_request',
]);

// load login widget library - widget.js
Route::get('/api/widget.js', [
    'uses' => 'APIController@getWidgetJS',
    'as' => 'api.widget_js'
]);
// load booking widgets library - booking.js
Route::get('/api/booking.js', [
    'uses' => 'APIController@getBookingJS',
    'as' => 'api.booking_js',
]);

// Set Hotel Cookie Iframe api
Route::get('/api/shc/{hotel_id}', [
    'uses' => 'APIController@setHotelCookie',
    'as' => 'api.set_hotel_cookie',
]);

// JS CORS AJAX request
Route::any('/api/{action}', [
    'uses' => 'APIController@jsRequest',
    'as' => 'api.js_request',
]);



// ========================================================================
// =================== Authentication and registration ====================

// PopUp authentication
Route::get('auth/login/popup', 'Auth\MemberPopupAuthController@getLogin');
Route::post('auth/login/popup', 'Auth\MemberPopupAuthController@postLogin');

Route::get('auth/register/popup', 'Auth\MemberPopupAuthController@getRegister');
Route::post('auth/register/popup', 'Auth\MemberPopupAuthController@postRegister');



// Authentication routes...
Route::get('auth/login', ['uses' => 'Auth\AuthController@getLogin', 'as' => 'WebsiteLogin']);
Route::post('auth/login', 'Auth\AuthController@postLoginAjax');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegisterAjax');


// On this URI the user account can be activated
// !!! - this route is DUPLICATED in Admin Panel
Route::get('auth/confirmation/{id}/{token}', ['as' => 'AccountConfirmation', 'uses' => 'Auth\AuthController@getConfirmation']);

// This page is displayed to the new MEMBER after his successful registration
Route::get('auth/member-after-registration', ['as' => 'AfterMemberRegistration', 'uses' => function() {
    $id = Session::get('registered_user_id');
    $user = \App\Member::findOrFail($id);
    return view('auth.member_after_registration', ['user' => $user]);
}]);

// Password reset link request routes...
Route::get('password/email/{popup?}', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmailAjax');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Do we even need a public hotel registration process?
//// This page is displayed to the new HOTEL after his successful registration
//Route::get('auth/hotel-after-registration', ['as' => 'AfterHotelRegistration', 'uses' => function() {
//    $id = Session::get('registered_user_id');
//    $user = \App\User::findOrFail($id);
//    return view('auth.member_after_registration', ['user' => $user]);
//}]);


// ========================================================================
// ======================= Member backend routes ==========================

    Route::get('/member/reservations', [
        'as' => 'MemberReservationTable',
        'uses' => 'MemberController@reservationTable',
    ]);
    Route::post('/member/reservations/{id}', [
        'as' => 'MemberReservationDetail',
        'uses' => 'MemberController@reservationDetail',
    ]);
    Route::get('/member/profile/{id?}', [
        'as' => 'MemberShowProfile',
        'uses' => 'MemberController@showProfile',
    ]);

    // Parametr se tu jmenuje members aby byl nazev kompatibilni s nazvem v resource() controlleru
    // a mohl se snadno pouzit stejny validacni request - MemberUpdateProfile
    Route::put('/member/profile/{members?}', [
        'as' => 'MemberUpdateProfile',
        'uses' => 'MemberController@updateProfile',
    ]);
    Route::get('/member/balance/{id?}', [
        'as' => 'MemberBalance',
        'uses' => 'MemberController@balance',
    ]);

    Route::get('/member/contact-us', [
        'as' => 'GetContactForm',
        'uses' => 'ContactController@getContactForm',
        'middleware' => 'auth',
    ]);
    Route::post('/member/contact-us', [
        'uses' => 'ContactController@postContactForm',
        'middleware' => 'auth', // This should be available for any logged in user role
    ]);



    Route::get('/member/avatar', [
        'as'         => 'GetMemberAvatar',
        'uses'       => 'MemberController@getAvatar',
        'middleware' => 'auth', // This should be available for any logged in user role
    ]);
    Route::post('/member/avatar', [
        'as'         => 'PostMemberAvatar',
        'uses'       => 'MemberController@postAvatar',
        'middleware' => 'auth', // This should be available for any logged in user role
    ]);


    // Autocompletes
    Route::get('autocomplete/country', ['uses' => 'Autocomplete@getCountry', 'as' => 'AutocompleteCountry', 'middleware' => 'auth']);

// ========================================================================
// ========================= STANDALONE PAGES =============================
    Route::get('/faq/member', ['as' => 'front.pages.faq.member', 'uses' => 'PageController@faq_member']);
    Route::get('/faq/hotel',  ['as' => 'front.pages.faq.hotel', 'uses' => 'PageController@faq_hotel']);
    Route::get('/privacy',    ['as' => 'front.pages.privacy',   'uses' => 'PageController@privacy']);

