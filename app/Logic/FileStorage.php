<?php
/**
 * Created by PhpStorm.
 * User: Jira
 * Date: 9.10.2015
 * Time: 18:30
 */

namespace App\Logic;

use App\File;
use File as FileSystem;
use Intervention\Image\Facades\Image;
use Log;
use \Symfony\Component\HttpFoundation\File\UploadedFile;

class FileStorage {

    /**
     * Removes file with given file_id. Other parameters may be past to check that file belongs to the specific object/category.
     * @param $file_id
     * @param null $object_name
     * @param null $object_id
     * @param null $category
     * @return bool - true on success, false on failure
     */
    public function removeFile($file_id, $object_name=null, $object_id=null, $category=null)
    {
        $file = File::where('id', $file_id);
        if ($object_name) {
            $file->where('object_name', $object_name);
        }
        if ($object_id) {
            $file->where('object_id', $object_id);
        }
        if ($category) {
            $file->where('category', $category);
        }
        $file = $file->first();

        if ($file !== null) {
            $dir = $file->getDirectory();
            // Delete model
            $file->delete();

            // And associated file(s) (whole directory)
            return FileSystem::deleteDirectory($dir);
        }

        // File not found
        return false;
    }

    public function getDropzoneFilesInfo($object_name, $object_id, $category='files')
    {
        $files = File::where('object_name', $object_name)
            ->where('object_id', $object_id)
            ->where('category', $category)
            ->orderBy('id', 'ASC')
            ->get();
        $result = array();
        foreach ($files as $file) {
            $result[] = array(
                'name' => $file->filename,
                'size' => $file->size,
                'url' => url($file->getPathName(File::SIZE_THUMBNAIL)),
                'id' => $file->id,
            );
        }

        return $result;
    }

    /**
     * Moves uploaded file to it's directory and creates corresponding DB record.
     * @param UploadedFile $file
     * @param $object_type
     * @param $object_id
     * @param bool $image
     */
    public function store(UploadedFile $file, $object_name, $object_id, $category='files', $image=false)
    {
        $params = [
            'filename' => $file->getClientOriginalName(),
            'ext' => $file->getClientOriginalExtension(),
            'size' => $file->getClientSize(),
            'object_id' => $object_id,
            'object_name' => $object_name,
            'category' => $category,
        ];
        if ($image) {
            $image_object = Image::make($file->getPathname());
            $params['width'] = $image_object->width();
            $params['height'] = $image_object->height();
        }
        $db_file = File::create($params);

        $dir = $db_file->getDirectory();
        $filename = $db_file->getFilename(File::SIZE_FULL);

        // Make dir if it does not exist
        if ( ! is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        Log::write('info', 'File MOVE: '.$dir.'  /   ' .$filename);

        // Copy uploaded file
        $target = $file->move($dir, $filename);

        Log::write('info', 'File moved: '.$target);

        // If it's an image then save a thumbnail
        if ($image) {
            $thumb_path = $db_file->getPathname(File::SIZE_THUMBNAIL);
            $image_object = Image::make($target);
            $image_object->widen(200, function($constraint) {
                // Do not resize up
                $constraint->upsize();
            })->save($thumb_path);
        }

        return $db_file;
    }

    // @todo - refactor using Laravel service container and Facades
    public static function instance()
    {
        return new static;
    }

}