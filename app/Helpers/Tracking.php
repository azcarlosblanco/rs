<?php
/**
 * Created by PhpStorm.
 * User: Jira
 * Date: 9.10.2015
 * Time: 9:53
 *
 * Static codebooks provider -
 */

namespace App\Helpers;

use App\Hotel;
use Auth;
use DB;
use Illuminate\Http\Request;

class Tracking {

    public static function widgetDisplayed($type, $engine_id, $hotel_id, Request $request)
    {
        if ($type === 'confirm') {
            $session_id = $request->get('session_id');
            $reservation_id = $request->get('reservation_id');
            if ($session_id) {
                $type = 'confirm_reservation';
            } else {
                $type = 'confirm_confirmation';
            }
        }

        $member = Auth::user();
        if ($member && $hotel_id && $member->source_hotel_id == $hotel_id) {
            // Member just visited his source hotel - store that directly in members table
            $member->source_hotel_last_visit = date('Y-m-d H:i:s');
            $member->save();
        }

        // IF the hotel was not detected based on referrer we check for it in params (coming from the booking engine)
        if (empty($hotel_id) && $engine_id) {
            $engine_hotel_id = $request->get('hotel_id', 0);
            $hotel = Hotel::where('engine_hotel_id', $engine_hotel_id)->where('booking_engine_id', $engine_id)->first();
            if ($hotel) {
                $hotel_id = $hotel->id;
            }
        }

        DB::insert("INSERT INTO access_logs (action_type, booking_engine_id, hotel_id, member_id, action_date, ip, user_agent, created, views_count) VALUES (?, ?, ?, ?, ?, ?, ?, NOW(), 1) ON DUPLICATE KEY UPDATE
            views_count=views_count+1,
            updated=NOW()
        ", [
            $type,
            $engine_id ?: 0,
            $hotel_id ?: 0,
            Auth::user() ? Auth::user()->id : 0,
            date('Y-m-d'),
            $request->ip(),
            $request->header('User-Agent')
        ]);
    }

}

