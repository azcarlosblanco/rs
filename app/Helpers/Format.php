<?php
/**
 * Created by PhpStorm.
 * User: Jira
 * Date: 9.10.2015
 * Time: 9:53
 *
 * Static codebooks provider -
 */

namespace App\Helpers;



class Format {


    public static function points($points, $is_valid=false)
    {
        if ($points == 0) {
            return '<span class="points-zero">'.$points.'</span>';
        }
        if ($points > 0) {
            $class = $is_valid ? 'points-positive' : 'points-zero';
            return '<span class="'.$class.'"><i class="glyphicon glyphicon-arrow-up"></i> '.$points.'</span>';
        }
        if ($points < 0) {
            $class = $is_valid ? 'points-negative' : 'points-zero';
            return '<span class="'.$class.'"><i class="glyphicon glyphicon-arrow-down"></i> '.$points.'</span>';
        }
    }

    /**
     * Formats the number as a currency.
     * @param <string> $price cena
     * @param <int> $cb_currency_type mena
     * @return <string>
     */
    static public function usMoneyValue($price, $currency='USD')
    {
        $currency = strtoupper($currency);
        if ((int)$price == $price) return number_format($price, 0, '.', ' ').' '.$currency;
        return number_format($price, 2, '.', ' ').' '.$currency;
    }

}
