<?php
/**
 * Created by PhpStorm.
 * User: Jira
 * Date: 9.10.2015
 * Time: 9:53
 *
 * Static codebooks provider -
 */

namespace App\Helpers;



use DB;

class Currency {

    public static $rates = [];

    /**
     * Calculates the multiplier for USD -> DisplayCurrency conversions
     * @param $usd_amount
     * @param $display_currency_amount
     * @return float|int
     */
    public static function calculateMultiplier($usd_amount, $display_currency_amount)
    {
        if ( ! $usd_amount) {
            return 0;
        }
        return $display_currency_amount / $usd_amount;
    }

    public static function loadRates()
    {
        if ( ! empty(static::$rates)) {
            return;
        }
        $rates = [];
        $res = DB::table("currency_rates")->get();
        foreach ($res as $row) {
            $rates[$row->code] = $row->value;
        }
        static::$rates = $rates;
    }

    public static function isCurrencyCodeSupported($currency_code)
    {
        $currency_code = strtoupper($currency_code);
        if ($currency_code === 'USD') {
            return true;
        }
        static::loadRates();
        return isset(static::$rates[$currency_code]);
    }

    public static function getUsdAmount($currency_amount, $currency_code)
    {
        $currency_code = strtoupper($currency_code);
        if ($currency_code === 'USD') {
            return $currency_amount;
        }

        static::loadRates();
        $rate = array_get(static::$rates, $currency_code);
        if ( ! $rate) {
            return 0;
        }

        return ($currency_amount / $rate);
    }

    public static function getCurrencyAmount($usd_amount, $currency_code)
    {
        $currency_code = strtoupper($currency_code);
        if ($currency_code === 'USD') {
            return $usd_amount;
        }

        static::loadRates();
        $rate = array_get(static::$rates, $currency_code);
        if ( ! $rate) {
            return 0;
        }

        return round($usd_amount * $rate, 2);
    }

    public static function getMultiplier($currency_code)
    {
        $currency_code = strtoupper($currency_code);
        if ($currency_code === 'USD') {
            return 1;
        }
        static::loadRates();
        return array_get(static::$rates, $currency_code);
    }

}

