<?php
/**
 * Helpers for operations with files and storages
 */

namespace App\Helpers;

use \Exception;
use Illuminate\Filesystem\FilesystemAdapter; // Not really use, only for shorcats in class definition

class Storage {

    /*
     * This function is coping file from external_disk ( /tmp ) to local storage using file descriptor (fopen())
     * @param FilesystemAdapter $storage
     * @param string $old_path
     * @param string $new_path (relative to / of $storage)
     *
     * @return bool
     * @throws Exception
     */
    public static function moveToStorage(FilesystemAdapter $storage, $old_path, $new_path)
    {
        if ( ! file_exists($old_path) || ! is_readable($old_path)) {
            return false;
        }

        $dir_new = pathinfo($new_path, PATHINFO_DIRNAME);
        if ( ! $storage->has($dir_new)) {
            if ( ! $storage->makeDirectory($dir_new)) {
                throw new Exception("Couldn't create directory: {$dir_new}");
            }
        }
        unset($dir_new);

        $descriptor = fopen($old_path, 'r');
        $result     = $storage->put($new_path, $descriptor);
        fclose($descriptor);

        if ( ! $result) {
            throw new Exception("Couldn't move file from: '{$old_path}' to '{$new_path}'");
        }

        return true;
    }


    /*
     * This function trying to remove directory recursively back from storage if it empty
     * For each part (recursive order) in /one/two/three/four/ check if empty -- remove and got one level upper
     *      if (empty($dir)) remove /one/two/three/four
     *      if (empty($dir)) remove /one/two/three
     *      if (empty($dir)) remove /one/two
     *      if (empty($dir)) remove /one
     *
     * @param FilesystemAdapter $storage
     * @param string $path
     *
     * @return bool
     */
    public static function deleteDirectoryIfEmpty(FilesystemAdapter $storage, $path)
    {
        $to_delete = pathinfo($path, PATHINFO_DIRNAME);

        if ( ! $storage->has($to_delete)) {
            return false;
        }

        $to_delete = explode('/', $to_delete);

        while ($to_delete) {
            $directory = implode('/', $to_delete);
            if ( ! $storage->has($directory)) {
                return false;
            }

            // Not empty? Stop removing!
            if ($storage->files($directory) || $storage->directories($directory)) {
                return false;
            }

            // Remove and go to one level upper
            $storage->deleteDirectory($directory);
            array_pop($to_delete);
        }

        return true;
    }
}
