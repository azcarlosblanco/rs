<?php
/**
 * Created by PhpStorm.
 * User: Jira
 * Date: 24.9.2015
 * Time: 14:08
 */


if ( ! function_exists('elixir_uri')) {
    /**
     * Get the path to a versioned Elixir file.
     *
     * @param  string  $file
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    function elixir_uri($file)
    {
        static $manifest = null;

        if (is_null($manifest)) {
            $manifest = json_decode(file_get_contents(public_path('build/rev-manifest.json')), true);
        }

        if (isset($manifest[$file])) {
            // Call to asset() added (it's missing in original laravel elixir() function (!)
            return asset('/build/'.$manifest[$file]);
        }

        throw new InvalidArgumentException("File {$file} not defined in asset manifest.");
    }
}



if ( ! function_exists('breaks_to_paragraphs')) {
    /**
     * Convert all [<p>,</p>,<br>,<br/>,<br />,\r,\n\,\r\n,\n\r] to <p></p>
     *
     * @param  string  $in
     * @return string  $out
     *
     * @throws \InvalidArgumentException
     */
    function breaks_to_paragraphs ($in)
    {
        $temp = str_replace(['<p>', '</p>', '<br>', '<br />', '<br/>', "\r\n", "\n\r", "\r", "\n"], "\n", $in);
        while (strpos($temp, "\n\n") !== false) {
            $temp = str_replace("\n\n", "\n", $temp);
        }

        $temp = explode("\n", $temp);
        $temp = array_map('trim', $temp);

        $out = '<p>' . implode("</p>\n\n<p>", $temp) . "</p>\n";
        return $out;
    }
}
