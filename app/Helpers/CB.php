<?php
/**
 * Created by PhpStorm.
 * User: Jira
 * Date: 9.10.2015
 * Time: 9:53
 *
 * Static codebooks provider -
 */

namespace App\Helpers;


use App\CbCountry;

class CB {

    public static function roles($prepend=false)
    {
        $res = [
            'admin' => 'Admin',
            'hotel' => 'Hotel',
            'member' => 'Member',
        ];

        return static::getList($res, $prepend);
    }

    public static function countries($prepend=false)
    {
        $res = CbCountry::lists('value', 'id')->all();
        return static::getList($res, $prepend);
    }


    public static function getList($list, $prepend=false)
    {
        if ($prepend) {
            $list = ['' => ''] + $list;
        }

        return $list;
    }
}
