<?php

namespace App;

use App\Helpers\Currency;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Member;
use App\Hotel;
use App\Reservation;
use DB;

class Allocation extends Model
{

    // (!!!) - duplicated in admin panel Model_Allocation
    const ALLOCATION_TTL = 1800; // how many [seconds] the allocation is considered valid

    public $timestamps = false;

    protected $dates = ['created_at', 'allocated_at'];

    protected $guarded = [];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }

    /**
     * Returns a total $ discount corresponding to the spend points amount.
     * @return float
     */
    public function getDiscountInUSD()
    {
        return $this->points_amount / Hotel::USD_TO_POINTS_MULTIPLIER;
    }

    /**
     * Returns a total discount IN USER/BookingEngine CURRENCY corresponding to the spend points amount.
     * @return float
     */
    public function getDiscountInUserCurrency()
    {
        return round($this->getDiscountInUSD() * $this->currency_multiplier, 2);
    }

    /**
     * @return float total amount to be paid by the client (reservation_amount - discount) [USD]
     */
    public function getAmountToBePaidInUSD()
    {
        return $this->reservation_amount_usd - $this->getDiscountInUSD();
    }

    /**
     * @return float amount of commission ReSynct is going to get from the hotel.
     */
    public function getCommissionAmountUSD()
    {
        return round($this->getAmountToBePaidInUSD() * $this->commission_relative / 100 + $this->commission_flat, 2);
    }

    /**
     * @return float total amount of points a customer is going to collect for the reservation
     */
    public function getPointsAmountToEarn()
    {
        return floor($this->getCommissionAmountUSD() * Hotel::USD_TO_POINTS_MULTIPLIER * $this->member_split_rate);
    }

    /**
     * Sets the hotel and loads relevant information to the allocation record.
     * @param Hotel $hotel
     */
    public function setHotel(Hotel $hotel)
    {
        $this->hotel_id = $hotel->id; // Our internal hotel ID
        $this->commission_relative = $hotel->commission_relative;
        $this->commission_flat = $hotel->commission_flat;
        $this->member_split_rate = $hotel->member_split_rate;
    }

    /**
     * Sets the currency in which the reservation amount is given.
     * This must be called BEFORE setReservationAmount
     * @param $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        $this->currency_multiplier = Currency::getMultiplier($currency);
    }

    /**
     * Sets the reservation amount and the currency.
     * @param Hotel $hotel
     */
    public function setReservationAmount($reservation_amount, $currency)
    {
        // Store the hotel base currency always
        $this->setCurrency($this->hotel->currency);

        // Always save the USD amount - calculate based on the user's currency
        $this->reservation_amount_usd = Currency::getUsdAmount($reservation_amount, $currency);
        // Also calculate the reservation amount in Hotel's currency
        $this->reservation_amount = $this->reservation_amount_usd * $this->currency_multiplier;

        // Make sure the points amount (to redeem) is not higher than the amount of the reservation
        $this->updatePointsAmount();
    }

    /**
     * Make sure that in case the reservation_amount is changed the points_spent amount is updated accordingly if needed
     */
    protected function updatePointsAmount()
    {
        if ($this->points_amount > $this->reservation_amount_usd * Hotel::USD_TO_POINTS_MULTIPLIER) {
            $this->points_amount = $this->reservation_amount_usd * Hotel::USD_TO_POINTS_MULTIPLIER;
        }
    }

    /**
     * Clears the point selection and hotel/reservation related data - so that the allocation record can be
     * used for a new selection/reservation process
     */
    public function clearSelection()
    {
        $this->points_amount = 0;
        $this->currency_multiplier = 1;
        $this->hotel_id = null;
        $this->reservation_amount = null;
        $this->reservation_amount_usd = null;
        $this->commission_relative = null;
        $this->commission_flat = null;
        $this->currency = null;
        $this->check_in_date = null;
        $this->check_out_date = null;
        $this->engine_hotel_id = null;
        $this->created_at = Carbon::now();
    }

    /**
     * Allocates a points selected previously by subtracting them from the member's balance
     * @return bool|string - true on success, string on error
     */
    public function allocatePoints()
    {
        $member = $this->member;

        // Already allocated - only refresh the date
        if ($this->allocated_at !== NULL) {
            // Already allocated
            // Just refresh the allocation time
            $this->allocated_at = Carbon::now();
            $this->save();
            return true;
        }

        // We are going to allocate now - check the user's balance
        if ($member->current_points_amount < $this->points_amount) {
            return 'not_enough_balance';
        }

        if ($this->points_amount == 0) {
            // Nothing to allocate
            // But we need to set the allocation time so that this record is no longer editable
            $this->allocated_at = Carbon::now();
            $this->save();
            return true;
        }
        DB::beginTransaction();
            // Mark this as allocated
            $this->allocated_at = Carbon::now();
            $this->save();

            // Update members point balance
            $this->member->updatePointBalance();
        DB::commit();

        return true;
    }

    /**
     * Allocates a points selected previously by subtracting them from the member's balance
     * @return bool|string - true on success, string on error
     */
    public function cancelAllocation()
    {
        $member = $this->member;

        if ($this->allocated_at === NULL) {
            return 'not_allocated_yet';
        }
        DB::beginTransaction();

            // Mark this NON-allocated
            $this->allocated_at = NULL;
            $this->save();

            // Update members point balance
            $this->member->updatePointBalance();

        DB::commit();

        return true;
    }

    /**
     * Creates a reservation record based on the data stored in the allocation record.
     */
    public function placeReservation($code)
    {
        if ($this->allocated_at === NULL) {
            return 'points_not_allocated';
        }

        DB::beginTransaction();
        // Create the reservation
        $reservation = new Reservation();
        $reservation->hotel_id = $this->hotel_id;
        $reservation->code = $code;
        $reservation->booking_engine_id = $this->booking_engine_id;
        $reservation->member_id = $this->member_id;
        $reservation->room_code = $this->room_code;
        $reservation->check_in_date = $this->check_in_date;
        $reservation->check_out_date = $this->check_out_date;
        $reservation->total_amount = $reservation->orig_total_amount = $this->reservation_amount_usd;
        $reservation->total_amount_in_currency = $this->reservation_amount;
        $reservation->currency = $this->currency;
        $reservation->currency_multiplier = $this->currency_multiplier;
        $reservation->paid_amount = $this->getAmountToBePaidInUSD();
        $reservation->discount_amount = $this->getDiscountInUSD();
        $reservation->commission_relative = $this->commission_relative;
        $reservation->commission_flat = $this->commission_flat;
        $reservation->commission_amount = $this->getCommissionAmountUSD();
        $reservation->points_spent = $reservation->orig_points_spent = $this->points_amount;
        $reservation->points_earned = $this->getPointsAmountToEarn();
        $reservation->member_split_rate = $this->member_split_rate;
        $reservation->created_at = Carbon::now();
        $reservation->save();

        // Delete the allocation
        $this->delete();

        // Update members point balance
        $reservation->member->updatePointBalance();

        // Commit the transaction
        DB::commit();

        return $reservation;
    }

    /**
     * Get only valid allocations.
     * @param Builder $query
     * @return $this
     */
    public function scopeValid(Builder $query)
    {
        return $query->whereNotNull('allocated_at')
            ->where('allocated_at', '>', DB::raw('DATE_SUB(NOW(), INTERVAL '.Allocation::ALLOCATION_TTL.' SECOND)'));
    }

}

