<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder as Builder;

class Reservation extends Model
{

    protected $dates = ['check_in_date', 'check_out_date'];

    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }

    public function getRealEarnedPoints()
    {
        if (in_array($this->status, ['cancelled', 'no_show']) && ! $this->was_paid) {
            return 0;
        }
        return $this->points_earned;
    }

    public function getRealSpentPoints()
    {
        if (in_array($this->status, ['cancelled', 'no_show']) && ! $this->was_paid) {
            return 0;
        }
        return $this->points_spent;
    }

    public function getEarnedPointsTooltip()
    {
        if ($this->status === 'new') {
            return 'On hold rePoints. Reservation waiting for confirmation.';
        }
        if (in_array($this->status, ['cancelled', 'no_show']) && ! $this->was_paid) {
            return 'No rePoints earned because the reservation was not paid.';
        }
        return 'rePoints earned and added to your balance.';
    }

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function getNightsCount()
    {
        return $this->check_out_date->diff($this->check_in_date)->days;
    }

    /**
     * Get only valid - confirmed reservations.
     * @param Builder $query
     * @return $this
     */
    public function scopeValid(Builder $query)
    {
        return $query->where('status', '=', 'confirmed');
    }

    /**
     * Get all reservations in "final" status (all except of 'new');
     * @param Builder $query
     * @return $this
     */
    public function scopeNotValid(Builder $query)
    {
        return $query->where('status', '!=', 'confirmed');
    }

    public function scopeUpcoming(Builder $query)
    {
        return $query->where('check_in_date', '>=', Carbon::now());
    }

    public function scopeNotUpcoming(Builder $query)
    {
        return $query->where('check_in_date', '<', Carbon::now());
    }


    public function getStatusPreview()
    {
        switch ($this->status) {
            case 'new':
                return '<div class="label label-warning">On Hold</div>';
            case 'no_show':
                return '<div class="label label-danger">No-show</div>';
            case 'cancelled':
                return '<div class="label label-danger">Cancelled</div>';
            case 'confirmed':
                return '<div class="label label-success">Confirmed</div>';
        }
    }


}
