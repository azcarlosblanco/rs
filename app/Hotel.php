<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{

    // Number of days fo which the member cookie is valid - see https://docs.google.com/document/d/1fsk1ypwsvt1qN38r8Hw8U8nj63dEz5LiOdUGrrcOTKo/edit
    const MEMBER_COOKIE_EXPIRATION_DAYS = 7;

    // There is 100 points for each $ of commission earned by the USER
    const USD_TO_POINTS_MULTIPLIER = 100;

    // What part of commission goes to the user - global setting, currently it's 50%
//    const COMMISSION_TO_USER_MULTIPLIER = 0.5; // - moved to hotel.member_split_rate

    // This is the minimum the user can redeem
    const MIN_REDEEMABLE_POINT_AMOUNT = 500;

    // Minimum step in point selection 100 points equals 1 dollar
    const REDEEM_POINTS_MINIMUM_STEP = 100;



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['commission_flat', 'commission_relative', 'current_points_amount', 'user_id'];


    /**
     * @param $reservation_amount
     * @return int - amount of points user will get for booking given amount in this hotel
     */
    public function calculatePointsForReservationAmount($reservation_amount)
    {
        $relative = $reservation_amount * $this->getAmountToPointsMultiplier();
        $flat = $this->getFlatPointsAmount();
        return (int)($relative + $flat);
    }

    public function getAmountToPointsMultiplier()
    {
        return $this->commission_relative / 100 * Hotel::USD_TO_POINTS_MULTIPLIER * $this->member_split_rate;
    }

    public function getFlatPointsAmount()
    {
        return $this->commission_flat * Hotel::USD_TO_POINTS_MULTIPLIER * $this->member_split_rate;
    }


    public function country()
    {
        return $this->belongsTo('App\CbCountry', 'cb_country_id');
    }

    public function reservations()
    {
        return $this->hasMany('App/Reservation');
    }

    public function scopeFulltext($query, $search)
    {
        return $query
            ->where('email', 'LIKE', "%$search%")
            ->orWhere('real_name', 'LIKE', "%$search%");
    }

    public function getLongPreviewAttribute()
    {
        return $this->real_name . '(' . $this->email . ')';
    }

    public function getWebsiteLink()
    {
        if ($this->website) {
            return '<a href="'.$this->website.'" target="_blank">' . $this->website_name .'</a>';
        }
        return $this->website_name;
    }

}
