<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberAvatar extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['filename', 'extension', 'width', 'height'];


    public function member()
    {
        return $this->belongsTo('App\Member');
    }


    /*
     * Generate and return full path (directory + filename) for local storage
     * Convert md5($this->id) from 'abcdefgzzzzzzz..' to 'images/avatars/abc/def/14.jpg'
     */
    public function getFullPath()
    {
        $hash          = md5($this->id);
        $hash_splitted = str_split($hash, 3);
        $path          = "images/avatars/{$hash_splitted[0]}/{$hash_splitted[1]}/";
        $path_full     = "{$path}{$this->id}.{$this->extension}";

        return $path_full;
    }



    public function url()
    {
        $path = $this->getFullPath();
        return url("upload/{$path}");
    }
}
