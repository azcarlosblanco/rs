<?php
/**
 * Created by PhpStorm.
 * User: Jiri
 * Date: 3.10.15
 * Time: 14:21
 */

namespace App\Repository;


class HotelRepository extends Repository {

    protected $model_name = '\App\Hotel';

    public function applyFilters($filters)
    {
        if ($val = array_get($filters, 'fulltext', false)) {

            $this->query->where(function($query) use($val) {
                $query
                    ->where('hotels.real_name', 'LIKE', '%'.$val.'%')
                    ->orWhere('hotels.phone', 'LIKE', '%'.$val.'%')
                    ->orWhere('hotels.email', 'LIKE', '%'.$val.'%')
                    ->orWhere('hotels.city', 'LIKE', '%'.$val.'%')
                    ->orWhere('hotels.state', 'LIKE', '%'.$val.'%')
                ;
            });

            unset($filters['fulltext']);
        }

        return parent::applyFilters($filters);
    }


} 