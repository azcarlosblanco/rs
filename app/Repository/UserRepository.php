<?php
/**
 * Created by PhpStorm.
 * User: Jiri
 * Date: 3.10.15
 * Time: 14:21
 */

namespace App\Repository;


use DB;

class UserRepository extends Repository {

    protected $model_name = '\App\User';

    public function applyFilters($filters)
    {
        if ($val = array_get($filters, 'fulltext', false)) {

            $this->query->where(function($query) use($val) {
                $query
                    ->where(DB::raw("CONCAT(users.firstname, ' ', users.lastname)"), 'LIKE', '%'.$val.'%')
                    ->orWhere('users.email', 'LIKE', '%'.$val.'%')
                    ->orWhere('users.phone', 'LIKE', '%'.$val.'%')
                ;
            });

            unset($filters['fulltext']);
        }

        return parent::applyFilters($filters);
    }


} 