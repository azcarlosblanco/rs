<?php

namespace App\Repository;


use ArrayAccess;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Schema;

class Repository {

    const DEFAULT_PAGE_SIZE = 10;

    protected $model_name = null;

    protected $request = array();

    protected $pre_filters = array();

    /**
     * @var Model
     */
    protected $model = null;

    /**
     * @var QueryBuilder
     */
    protected $query = null;

    function __construct()
    {
        $this->model = new $this->model_name;
    }

    public function newQuery()
    {
        $this->query = $this->model->newQuery();
        return $this;
    }

    protected function columnExists($column)
    {
        return Schema::hasColumn($this->model->getTable(), $column);
    }

    /**
     * @param ArrayAccess $filters expected format:
     * 'filters' => array(
     *     'key' => value
     * ),
     * 'sorting' => array(
     *     'column' => direction
     * ),
     * 'pagination' => array(
     *     'page_size' => 20,
     *     'page_number' => 0,
     * ),
     *
     * @return $this
     */
    public function useRequest($request)
    {
        $this->request = $request;
    }

    /**
     * Filters defined usually on server-side.
     * With this method we make sure they will  be used for all queries run by this instance.
     * @param $pre_filters
     */
    public function usePreFilters($pre_filters)
    {
        $this->pre_filters = $pre_filters;

        return $this;
    }

    /**
     * Apply pre-filters on current query.
     * @param null $pre_filters
     */
    public function applyPreFilters($pre_filters=null)
    {
        if ($pre_filters === null) {
            $pre_filters = $this->pre_filters;
        }

        return $this->applyFilters($pre_filters);
    }

    /**
     * Apply filters on current query.
     * @param null $pre_filters
     */
    public function applyFilters($filters)
    {
        foreach ($filters as $key => $value)
        {
            if ($value === '' or $value === null) {
                continue;
            }
            if ($this->columnExists($key)) {
                $this->query->where($key, '=', $value);
            }
        }

        return $this;
    }

    public function applySorting($sorting)
    {
        foreach ($sorting as $attr => $dir)
        {
            $dir = strtoupper($dir);
            if ($this->columnExists($attr) and in_array($dir, ['ASC', 'DESC'])) {
                $this->query->orderBy($attr, $dir);
            }
        }
    }

    public function getPage()
    {
        $this->newQuery() // Create new query instance
            ->applyPreFilters()
            ->applyFilters(array_get($this->request, 'filter', array()))
            ->applySorting(array_get($this->request, 'sorting', array()))
            ;
        $pagination = array_get($this->request, 'pagination', array());
        $page_size = array_get($pagination, 'page_size', static::DEFAULT_PAGE_SIZE);
        $page_number = array_get($pagination, 'page_number', 1);
        $skip = ($page_number-1) * $page_size;

        return $this->query->skip($skip)->take($page_size)->get();
    }

    public function getTotalResultsCount()
    {
        $this->newQuery()
            ->applyPreFilters()
            ->applyFilters(array_get($this->request, 'filter', array()))
        ;

        return $this->query->count();
    }

    public function getPageSize()
    {
        return static::DEFAULT_PAGE_SIZE;
    }

    /**
     * Applies a pagination and gets the result
     * @return mixed
     */
    public function getResults()
    {
        return $this->getPage();
    }

}
