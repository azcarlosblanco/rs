<?php
/**
 * Created by PhpStorm.
 * User: Jiri
 * Date: 3.10.15
 * Time: 14:21
 */

namespace App\Repository;


use DB;

class MemberRepository extends Repository {

    protected $model_name = '\App\Member';

    public function applyFilters($filters)
    {
        if ($val = array_get($filters, 'fulltext', false)) {

            $this->query->where(function($query) use($val) {
                $query
                    ->where(DB::raw("CONCAT(members.firstname, ' ', members.lastname)"), 'LIKE', '%'.$val.'%')
                    ->orWhere('members.email', 'LIKE', '%'.$val.'%')
                ;
            });

            unset($filters['fulltext']);
        }

        return parent::applyFilters($filters);
    }


} 