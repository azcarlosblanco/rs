<?php
/**
 * Created by PhpStorm.
 * User: Jiri
 * Date: 3.10.15
 * Time: 14:21
 */

namespace App\Repository;


use DB;

class ReservationRepository extends Repository {

    protected $model_name = '\App\Reservation';

    public function applyFilters($filters)
    {
        if ($val = array_get($filters, 'fulltext', false)) {

            $this->query
                ->join('members', 'members.id', '=', 'reservations.member_id')
                ->join('hotels', 'hotels.id', '=', 'reservations.hotel_id');
            $this->query->where(function($query) use($val) {
                $query
                    ->where('hotels.real_name', 'LIKE', '%'.$val.'%')
                    ->orWhere(DB::raw("CONCAT(members.firstname, ' ', members.lastname)"), 'LIKE', '%'.$val.'%')
                    ->orWhere('reservations.code', 'LIKE', '%'.$val.'%')
                ;
            });

            unset($filters['fulltext']);
        }

        return parent::applyFilters($filters);
    }

}
