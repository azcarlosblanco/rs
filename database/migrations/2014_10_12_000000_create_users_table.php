<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('role', 20)->default('member');
            $table->integer('cb_country_id');
            $table->integer('current_points_amount')->default(0);
            $table->string('firstname', 45);
            $table->string('lastname', 45);
            $table->string('email')->unique();
            $table->string('phone', 15);
            $table->string('place_state', 45);
            $table->string('place_city', 45);
            $table->string('place_street', 45);
            $table->string('place_number', 15);
            $table->string('password', 60);
            $table->boolean('confirmed')->default(false);
            $table->boolean('active')->default(false);
            $table->timestamp('confirmed_at')->nullable()->default(null);
            $table->string('confirmation_token', 255);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('members');
    }
}
