<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE IF NOT EXISTS `hotels` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `engine_hotel_id` VARCHAR(45) NULL,
              `engine_ip` VARCHAR(45) NULL,
              `booking_engine_id` VARCHAR(15) NULL,
              `active` TINYINT NOT NULL DEFAULT 0,
              `real_name` VARCHAR(100) NULL,
              `website_name` VARCHAR(255) NULL,
              `star_rating` TINYINT NULL,
              `email` VARCHAR(65) NULL,
              `phone` VARCHAR(15) NULL,
              `website` VARCHAR(255) NULL,
              `commission_flat` DECIMAL(4,2) NULL,
              `commission_relative` DECIMAL(5,2) NULL,
              `cb_country_id` INT NULL,
              `state` VARCHAR(45) NULL,
              `city` VARCHAR(45) NULL,
              `zip` VARCHAR(10) NULL,
              `address` VARCHAR(100) NULL,
              `neighborhood` VARCHAR(45) NULL,
              `latitude` DECIMAL(11,8) NULL,
              `longitude` DECIMAL(11,8) NULL,
              `current_points_amount` INT NOT NULL DEFAULT 0,
              `user_id` INT NULL,
              `created_at` TIMESTAMP NULL,
              `updated_at` TIMESTAMP NULL,
              PRIMARY KEY (`id`))
            ENGINE = InnoDB;
        ");

        DB::statement("

            CREATE TABLE IF NOT EXISTS `invoices` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `hotel_id` INT NOT NULL,
              `period_start` DATE NOT NULL,
              `period_end` DATE NOT NULL,
              `commission_amount` DECIMAL(8,2) NOT NULL,
              `redeemed_amount` DECIMAL(8,2) NOT NULL DEFAULT 0,
              `redeemed_points` INT NOT NULL DEFAULT 0,
              `due_date` DATE NOT NULL,
              `paid_at` TIMESTAMP NULL DEFAULT NULL,
              `user_id` INT NULL DEFAULT NULL,
              `created_at` TIMESTAMP NULL,
              `updated_at` TIMESTAMP NULL,
              PRIMARY KEY (`id`),
              INDEX `inv_hotel_idx` (`hotel_id` ASC),
              CONSTRAINT `inv_hotel_fk`
                FOREIGN KEY (`hotel_id`)
                REFERENCES `hotels` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
            ");

        DB::statement("
            CREATE TABLE IF NOT EXISTS `reservations` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `status` ENUM('new', 'confirmed', 'cancelled', 'no_show') NOT NULL DEFAULT 'new',
              `hotel_id` INT NOT NULL,
              `booking_engine_id` INT NOT NULL,
              `member_id` INT NOT NULL,
              `invoice_id` INT NULL,
              `code` VARCHAR(20) NOT NULL,
              `check_in_date` DATE NOT NULL,
              `check_out_date` DATE NOT NULL,
              `room_code` VARCHAR(10) NOT NULL,
              `price_details_json` TEXT NOT NULL,
              `total_amount` DECIMAL(8,2) NOT NULL,
              `paid_amount` DECIMAL(8,2) NOT NULL,
              `discount_details_json` TEXT NULL DEFAULT NULL,
              `discount_amount` DECIMAL(8,2) NOT NULL DEFAULT 0,
              `commission_amount` DECIMAL(8,2) NOT NULL,
              `commission_relative` DECIMAL(5,2) NOT NULL,
              `points_spent` INT NOT NULL,
              `points_earned` INT NOT NULL,
              `confirmed_by` INT NULL,
              `confirmed_at` TIMESTAMP NULL,
              `created_at` TIMESTAMP NULL,
              `updated_at` TIMESTAMP NULL,
              PRIMARY KEY (`id`),
              INDEX `res_hotel_idx` (`hotel_id` ASC),
              INDEX `res_member_idx` (`member_id` ASC),
              INDEX `res_invoice_idx` (`invoice_id` ASC),
              CONSTRAINT `res_hotel_fk`
                FOREIGN KEY (`hotel_id`)
                REFERENCES `hotels` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `res_member_fk`
                FOREIGN KEY (`member_id`)
                REFERENCES `members` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `res_invoice_fk`
                FOREIGN KEY (`invoice_id`)
                REFERENCES `invoices` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
            ");

        DB::statement("
            CREATE TABLE IF NOT EXISTS `discounts` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `hotel_id` INT NOT NULL,
              `date` DATE NOT NULL,
              `room_code` VARCHAR(20) NOT NULL,
              `percentage_amount` INT NOT NULL,
              `created_at` TIMESTAMP NULL,
              `updated_at` TIMESTAMP NULL,
              `user_id` INT NULL,
              PRIMARY KEY (`id`),
              INDEX `dis_hotel_idx` (`hotel_id` ASC),
              CONSTRAINT `dis_hotel_fk`
                FOREIGN KEY (`hotel_id`)
                REFERENCES `hotels` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
            ");

        DB::statement("
            CREATE TABLE IF NOT EXISTS `allocations` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `session_id` VARCHAR(100) NOT NULL,
              `member_id` INT NOT NULL,
              `hotel_id` INT NULL,
              `engine_hotel_id` VARCHAR(45) NULL,
              `booking_engine_id` VARCHAR(15) NULL,
              `room_code` VARCHAR(45) NULL,
              `check_in_date` DATE NULL DEFAULT NULL,
              `check_out_date` DATE NULL DEFAULT NULL,
              `points_amount` INT NOT NULL DEFAULT 0,
              `reservation_amount` DECIMAL(8,2) NULL,
              `reservation_amount_usd` DECIMAL(8,2) NULL,
              `commission_relative` DECIMAL(5,2) NULL,
              `currency` VARCHAR(5) NULL DEFAULT 'USD',
              `currency_multiplier` DECIMAL(8,6) NULL DEFAULT 1,
              `hotel_commission` DECIMAL(8,6) NULL,
              `price_details_json` TEXT NULL DEFAULT NULL,
              `allocated_at` TIMESTAMP NULL DEFAULT NULL,
              `created_at` TIMESTAMP NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `alloc_uniqueid_idx` (`session_id` ASC),
              INDEX `alloc_hotel_idx` (`hotel_id` ASC),
              INDEX `alloc_member_idx` (`member_id` ASC),
              CONSTRAINT `alloc_hotel_fk`
                FOREIGN KEY (`hotel_id`)
                REFERENCES `hotels` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `alloc_member_fk`
                FOREIGN KEY (`member_id`)
                REFERENCES `members` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB

        ");

        DB::statement("
            CREATE TABLE IF NOT EXISTS `whitelisted_domains` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `domain_name` VARCHAR(255) NOT NULL,
              `hotel_id` INT NULL,
              `booking_engine_id` INT NULL DEFAULT NULL,
              `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`))
            ENGINE = InnoDB
        ");

        DB::statement("
        CREATE TABLE IF NOT EXISTS `booking_engines` (
          `id` INT NOT NULL AUTO_INCREMENT,
          `name` VARCHAR(45) NULL,
          `username` VARCHAR(45) NULL,
          `password` VARCHAR(60) NULL,
          `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`))
        ENGINE = InnoDB
        ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP TABLE IF EXISTS reservations, allocations, discounts, invoices, hotels, whitelisted_domains, booking_engines;
        ");
    }
}

