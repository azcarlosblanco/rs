<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyFromMembersToCbCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            // $table->integer('cb_country_id')->nullable()->change();
            $table->integer('cb_country_id')->nullable()->default(null)->change();
        });
        Schema::getConnection()->statement('update members set cb_country_id = null where cb_country_id = 0');

        Schema::table('members', function ($table) {
            $table->foreign('cb_country_id')->references('id')->on('cb_countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function ($table) {
            $table->dropForeign(['cb_country_id']);
        });

        Schema::getConnection()->statement('update members set cb_country_id = 0 where cb_country_id is null');

        Schema::table('members', function (Blueprint $table) {
            $table->integer('cb_country_id')->nullable(false)->default(0)->change();
        });
    }
}
