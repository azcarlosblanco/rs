<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMemberAvatars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // id[autoincrement], filename, extension, width, height, created
        Schema::create('member_avatars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_id');
            $table->string('filename', 255);
            $table->string('extension', 50);
            $table->string('external_url', 500);
            $table->integer('width');
            $table->integer('height');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('member_avatars', function ($table) {
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_avatars', function ($table) {
            $table->dropForeign(['member_id']);
        });
        Schema::drop('member_avatars');
    }
}
