<?php

use Illuminate\Database\Seeder;

class HotelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i<20; $i++) {
            DB::table('hotels')->insert([
                'real_name' => 'Pegas',
                'engine_hotel_id' => 1,
                'booking_engine_id' => '1',
                'website_name' => 'Hotel Pegas',
                'star_rating' => 4,
                'email' => str_random(10).'@gmail.com',
                'phone' => '444333222',
                'commission_relative' => 10,
                'cb_country_id' => '57',
                'state' => '',
                'city' => 'Brno',
                'zip' => '54423',
                'current_points_amount' => rand(100, 12000),
            ]);
            DB::table('hotels')->insert([
                'real_name' => 'Grand Hotel',
                'engine_hotel_id' => 2,
                'booking_engine_id' => '2',
                'website_name' => 'Grand Hotel',
                'star_rating' => 5,
                'email' => str_random(10).'@gmail.com',
                'phone' => '605624007',
                'commission_relative' => 10,
                'cb_country_id' => '57',
                'state' => '',
                'city' => 'Brno',
                'zip' => '54423',
                'current_points_amount' => rand(100, 12000),
            ]);
        }
    }

}
