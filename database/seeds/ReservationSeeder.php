<?php

use Illuminate\Database\Seeder;

class ReservationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a user
        DB::table('members')->insert([
            'id' => 1,
            'firstname' => 'Paul',
            'lastname' => 'Walker',
            'email' => 'paul@gmail.com',
            'current_points_amount' => '37450',
            'password' => bcrypt('secret'),
            'role' => 'member',
            'confirmed' => '1',
            'active' => '1',
            'confirmed_at' => '2015-08-12 06:43:36',
        ]);
        // Create a user
        DB::table('members')->insert([
            'id' => 2,
            'firstname' => 'John',
            'lastname' => 'Doe',
            'email' => 'john@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 'member',
        ]);

        DB::table('reservations')->insert([
            'status' => 'new',
            'hotel_id' => '1',
            'member_id' => '1',
            'code' => str_random(10),
            'check_in_date' => '2015-10-11',
            'check_out_date' => '2015-10-14',
            'room_code' => 'ABC',
            'total_amount' => 356,
            'paid_amount' => 356,
            'commission_amount' => 35.6,
            'commission_relative' => 10,
            'points_spent' => 0,
            'points_earned' => 1780,
        ]);
        DB::table('reservations')->insert([
            'status' => 'confirmed',
            'hotel_id' => '2',
            'member_id' => '1',
            'code' => str_random(10),
            'check_in_date' => '2015-10-11',
            'check_out_date' => '2015-10-14',
            'room_code' => 'ABC',
            'total_amount' => 356,
            'paid_amount' => 356,
            'commission_amount' => 35.6,
            'commission_relative' => 10,
            'points_spent' => 0,
            'points_earned' => 1780,
        ]);
        DB::table('reservations')->insert([
            'status' => 'no_show',
            'hotel_id' => '2',
            'member_id' => '1',
            'code' => str_random(10),
            'check_in_date' => '2015-10-11',
            'check_out_date' => '2015-10-14',
            'room_code' => 'ABC',
            'total_amount' => 150,
            'paid_amount' => 150,
            'commission_amount' => 15,
            'commission_relative' => 10,
            'points_spent' => 0,
            'points_earned' => 750,
        ]);
        DB::table('reservations')->insert([
            'status' => 'cancelled',
            'hotel_id' => '2',
            'member_id' => '1',
            'code' => str_random(10),
            'check_in_date' => '2015-10-11',
            'check_out_date' => '2015-10-14',
            'room_code' => 'ABC',
            'total_amount' => 200,
            'paid_amount' => 200,
            'commission_amount' => 20,
            'commission_relative' => 10,
            'points_spent' => 0,
            'points_earned' => 1000,
        ]);

        DB::table('booking_engines')->insert([
            'name' => 'Booking Force',
            'username' => 'booking_force',
            'password' => bcrypt('123SeCrEt321')
        ]);
        DB::table('booking_engines')->insert([
            'name' => 'Localhost',
            'username' => 'localhost',
            'password' => bcrypt('123SeCrEt321')
        ]);

        DB::table('whitelisted_domains')->insert([
            'domain_name' => 'https://resynct-dot-social-booking-engine.appspot.com',
            'booking_engine_id' => '1',
        ]);
        DB::table('whitelisted_domains')->insert([
            'domain_name' => 'http://localhost:8080',
            'booking_engine_id' => '2',
        ]);
    }
}
