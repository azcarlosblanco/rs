process.env.DISABLE_NOTIFIER = true;
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// Back
elixir(function(mix) {
    // BACK
    mix.sass('back.scss')
        .styles([
            'public/css/back.css', // Output of sass('back.scss')
            'resources/assets/css/selectize.bootstrap3.css',
            'resources/assets/css/slim.css', // http://slim.pqina.nl/
        ], 'public/css/back_merged.css', './')
        .styles([
            'popup.css',
        ], 'public/css/popup.css')
        .styles([
            'dropzone.css',
        ], 'public/css/dropzone.css')
        .styles([
            'simple-slider.css',
        ], 'public/css/simple-slider.css')
        .styles([
            'widget.css',
        ], 'public/css/widget_login.css')
        .styles([
            'widget.css',
            'simple-slider.css',
        ], 'public/css/widget_booking.css')
        .styles([
            'iframe_widget.css',
            'simple-slider.css',
        ], 'public/css/widgets_new.css')
        .scripts([
            'jquery-1.11.3.min.js',
            'selectize.min.js',
            'unserialize.jquery.latest.js',
            'datatable.js',
            'app.js',
            'resynct/jquery.jumpTop.js',
            'jquery.tablesorter.min.js',  // https://github.com/christianbach/tablesorter
            'jquery.autocomplete.min.js', // https://github.com/devbridge/jQuery-Autocomplete
            'slim.jquery.js',                                        // http://slim.pqina.nl/
            'resynct/member/jquery.MemberProfileAvatarUpdate.js',    // Local configuration for slim image crop plugin
            'resynct/member/jquery.MemberProfile.js',
        ])

        .scripts([
            'simple-slider.js',
            'resynct/core.js',
            'resynct/login.js',
            'resynct/redeem.js',
            'resynct/confirm.js',
            'resynct/collect.js'
        ], 'public/js/widgets.js')

        // @todo - merge this with booking_js.php
        .scripts([
            'simple-slider.js',
        ], 'public/js/simple-slider.js')
        .scripts([
            'dropzone.js',
        ], 'public/js/dropzone.js')
        // BACK


        // FRONT
        .sass('front.scss')
        .styles([
            'public/css/front.css', // Output of sass('front.scss')
            'resources/assets/css/selectize.bootstrap3.css',
        ], 'public/css/front_merged.css', './')
        .scripts([
            'resynct/home/main.js',
            'resynct/home/jquery.RegistrationFormPlugin.js',
            'resynct/home/jquery.LoginFormPlugin.js',
            'resynct/home/jquery.PasswordFormPlugin.js',
            'resynct/home/jquery.ResetFormPlugin.js',
            'resynct/home/get_member.js',
            'resynct/home/faq.js'
        ], 'public/js/front.js')
        // FRONT

        .version([
             'css/back_merged.css',
             'css/back.css',
             'js/all.js',
             'js/widgets.js',
             'css/popup.css',

             'css/front_merged.css',
             'css/front.css',
             'js/all.js',
             'js/front.js'
        ])
    ;
});
